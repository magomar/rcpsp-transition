package rcpsp.converter.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.DirectoryChooser;
import rcpsp.Folders;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author Mario Gómez Martínez <magomar@gmail.com>
 */
public class ConverterController implements Initializable {
    @FXML
    private ListView dataSetsFound;
    private ObservableList<File> datasetsModel = FXCollections.observableArrayList();
    @FXML
    private TextField libraryDirectoryTextField;
    @FXML
    private ComboBox<DatasetType> datasetTypeChoiceBox;
    private ObservableList<DatasetType> datasetTypesModel = FXCollections.observableArrayList();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        datasetTypesModel.addAll(DatasetType.values());
        datasetTypeChoiceBox.setItems(datasetTypesModel);
//        datasetTypeChoiceBox.setValue(DatasetType.SM);
//        datasetTypeChoiceBox.getSelectionModel().select(DatasetType.SM);
        dataSetsFound.setItems(datasetsModel);
    }

    @FXML
         private void refreshDatasets() {
        String directoryName = libraryDirectoryTextField.getText();
        DatasetType datasetType = datasetTypeChoiceBox.getValue();
        if (datasetType == null) datasetType = DatasetType.SM;
        if (!directoryName.isEmpty() && datasetType != null) {
            File mainDirectory = new File(directoryName);
            String[] directories = mainDirectory.list(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String name) {
                    DatasetType selectDatasetType = (DatasetType) datasetTypeChoiceBox.getValue();
                    return  name.endsWith(selectDatasetType.getFileExtension()) && new File(dir, name).isDirectory();
                }
            });
            datasetsModel.clear();
            for (int i = 0; i < directories.length; i++) {
                String directory = directories[i];
                datasetsModel.add(new File(directory));
            }
        }
    }

    @FXML
    private void selectLibraryDirectoryAction(ActionEvent event) {
        DirectoryChooser directoryChooser = new DirectoryChooser();

        directoryChooser.setTitle("Chose a directory containing datasets");
        File defaultDirectory = new File(System.getProperty("user.dir") + File.separator + Folders.DATASETS);
        directoryChooser.setInitialDirectory(defaultDirectory);
        //Show open file dialog
        File file = directoryChooser.showDialog(null);

        if (file != null) {
            libraryDirectoryTextField.setText(file.getPath());
            refreshDatasets();
        }

    }


    public void handleImportDatasetButtonAction(ActionEvent actionEvent) {
    }

    public enum DatasetType {
        RCP,
        SM,
        MM;

        public String getFileExtension() {
            return name().toLowerCase();
        }
    }
}
