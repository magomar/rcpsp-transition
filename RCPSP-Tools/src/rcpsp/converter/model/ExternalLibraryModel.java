package rcpsp.converter.model;

import javafx.beans.property.StringProperty;

/**
 * @author Mario Gómez Martínez <magomar@gmail.com>
 */
public class ExternalLibraryModel {
    private StringProperty path;

    public final String getPath() {
        return path.get();
    }

    public StringProperty pathProperty() {
        return path;
    }

    public final void setPath(String path) {
        this.path.set(path);
    }
}
