/**
 * 
 */
package rcpsp.experiment;

/**
 * @author Mario Gomez
 *
 */
public interface Experiment {

	public void runExperiment();
	public void endExperiment();
}
