/**
 * 
 */
package rcpsp.experiment;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.BenchmarkSolution;
import rcpsp.data.jaxb.Project;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Mario Gomez
 *
 */
public final class CompareSingleVsMultiThread extends AbstractExperiment {
	private final NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
	private final Scheduler scheduler;
	private final DatasetWrapper wrapper;

	/**
	 * @param name
	 * @param scheduler
	 * @param wrapper
	 */
	public CompareSingleVsMultiThread(String name, Scheduler scheduler, DatasetWrapper wrapper) {
		super(name);
		this.scheduler = scheduler;
		this.wrapper = wrapper;
	}

	/* (non-Javadoc)
	 * @see cbrcps.AbstractExperiment#runExperiment()
	 */
	@Override
	public void runExperiment() {
		super.runExperiment();
		
		long singleThreadTimeCost;
		long multiThreadTimeCost;

		String[] datasetName = wrapper.getDatasetName();		
		int numDatasets = datasetName.length;

		Project[][] project = new Project[numDatasets][];

		for (int d = 0; d < numDatasets; d++) {
			BenchmarkResults benchmark  = wrapper.parseBenchmarkFile(datasetName[d]);
			int numProblems = benchmark.getBenchmarkSolution().size();
			project[d] = new Project[numProblems];
			int problemIndex = 0;
			for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolution()) {
				String problem = benchmarkSolution.getProblem();
				project[d][problemIndex] = wrapper.parseProblem(datasetName[d], problem);
				System.out.print(".");
				problemIndex++;
			}
			System.out.println();
		}

		// MULTI-THREAD
		System.out.println("RUNNING MULTI-THREAD ");
		stopwatch.start();
		for (int d = 0; d < numDatasets; d++) 
			TaskScheduler.solveMultipleProblemsConcurrently(scheduler, project[d]);
        stopwatch.stop();
		multiThreadTimeCost = stopwatch.getTotalTime();
		System.out.println("FINISHED MULTI THREAD " + multiThreadTimeCost);
		System.out.println();
		
		// SINGLE THREAD
		System.out.println("RUNNING SINGLE THREAD ");
		stopwatch.start();
		for (int d = 0; d < numDatasets; d++) 
			TaskScheduler.solveMultipleProblems(scheduler, project[d]);
        stopwatch.stop();
		singleThreadTimeCost = stopwatch.getTotalTime();
		System.out.println("FINISHED SINGLE THREAD " + singleThreadTimeCost);
		System.out.println();

		double multiThreadImprovement = (double) multiThreadTimeCost / singleThreadTimeCost;
		System.out.println("Multi-Thread's cost is " + nf.format(multiThreadImprovement) + " times the cost of Single-Thread");
		
	}

}
