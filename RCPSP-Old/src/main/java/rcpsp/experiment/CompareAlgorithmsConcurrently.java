/**
 *
 */
package rcpsp.experiment;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.Solution;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.BenchmarkSolution;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.ProjectSchedule;
import rcpsp.tools.util.Report;

import java.util.Arrays;


/**
 * @author Mario Gomez
 */
public final class CompareAlgorithmsConcurrently extends AbstractExperiment {

    private final Scheduler[] scheduler;
    private final DatasetWrapper wrapper;

    /**
     * @param name
     * @param scheduler
     * @param wrapper
     */
    public CompareAlgorithmsConcurrently(String name, Scheduler[] scheduler, DatasetWrapper wrapper) {
        super(name);
        this.scheduler = scheduler;
        this.wrapper = wrapper;
    }

    private static double computeDeviation(BenchmarkSolution oldSolution, int makespan) {
        return (double) (makespan - oldSolution.getLowerBound()) / (double) oldSolution.getLowerBound();
    }

    @Override
    public void runExperiment() {
        super.runExperiment();

        String[] dataset = wrapper.getDatasetName();
        int numDatasets = dataset.length;
        int numAlgorithms = scheduler.length;

        double[][][] data = new double[dataset.length][numAlgorithms][];
        BenchmarkResults[] newBenchmarkResults = new BenchmarkResults[numDatasets];
        BenchmarkSolution[][] newBenchmarkSolution = new BenchmarkSolution[numDatasets][];
        String[] algorithm = new String[numAlgorithms];

        for (int a = 0; a < numAlgorithms; a++) {
            algorithm[a] = scheduler[a].getAlgorithm();
        }

        for (int d = 0; d < numDatasets; d++) {
            BenchmarkResults benchmarkResults = wrapper.parseBenchmarkFile(dataset[d]);
            int numProblems = benchmarkResults.getBenchmarkSolution().size();
            BenchmarkSolution[] datasetBenchmarkSolutions = new BenchmarkSolution[numProblems];
            for (int a = 0; a < numAlgorithms; a++) {
                data[d][a] = new double[numProblems];
            }
            Project[] project = wrapper.loadDatasetProjects(benchmarkResults);

            // iterate over all problems and set default, worst-case solutions
            int problemIndex = 0;
            for (BenchmarkSolution benchmarkSolution : benchmarkResults.getBenchmarkSolution()) {
                BenchmarkSolution singleBenchmarkSolution = new BenchmarkSolution();
                singleBenchmarkSolution.setProblem(benchmarkSolution.getProblem());
                singleBenchmarkSolution.setLowerBound(benchmarkSolution.getLowerBound());
                singleBenchmarkSolution.setUpperBound(benchmarkSolution.getUpperBound());
                ProjectSchedule worstCaseSolution = new ProjectSchedule();
                worstCaseSolution.setMakespan(project[problemIndex].getTimeHorizon() + 1);
                singleBenchmarkSolution.setOptimalSolution(worstCaseSolution);
                datasetBenchmarkSolutions[problemIndex] = singleBenchmarkSolution;
                problemIndex++;
            }

            // EXECUTE SCHEDULING ALGORITHMS
            Solution[][] newSolution = new Solution[numAlgorithms][];
            for (int a = 0; a < numAlgorithms; a++) {
                newSolution[a] = TaskScheduler.solveMultipleProblemsConcurrently(scheduler[a], project);
            }

            // determine which is the best solution among the solutions obtained by all algorithms
            problemIndex = 0;
            for (BenchmarkSolution benchmarkSolution : benchmarkResults.getBenchmarkSolution()) {
                for (int a = 0; a < numAlgorithms; a++) {
                    int makespan = newSolution[a][problemIndex].getMakespan();
                    data[d][a][problemIndex] = computeDeviation(benchmarkSolution, makespan);
                    BenchmarkSolution singleBenchmarkSolution1 = datasetBenchmarkSolutions[problemIndex];
                    if (makespan < singleBenchmarkSolution1.getOptimalSolution().getMakespan()) {
                        ProjectSchedule projectSchedule = new ProjectSchedule();
                        projectSchedule.setMakespan(makespan);
                        projectSchedule.setAlgorithm(algorithm[a]);
                        projectSchedule.getJobSchedule().addAll(newSolution[a][problemIndex].getJobSchedules());
                        singleBenchmarkSolution1.setOptimalSolution(projectSchedule);
                    }
                }
                problemIndex++;
            }
            newBenchmarkSolution[d] = datasetBenchmarkSolutions;
            BenchmarkResults datasetBenchmarkResults = new BenchmarkResults();
            datasetBenchmarkResults.setDataset(benchmarkResults.getDataset());
            datasetBenchmarkResults.setType(benchmarkResults.getType());
            datasetBenchmarkResults.getBenchmarkSolution().addAll(Arrays.asList(datasetBenchmarkSolutions));
            newBenchmarkResults[d] = datasetBenchmarkResults;
        }

        // print results
        Report.reportStatistics(algorithm, dataset, data, this.toString());
        Report.reportSolutions(newBenchmarkResults, dataset, this.toString());
    }
}
