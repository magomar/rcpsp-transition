/**
 * 
 */
package rcpsp.experiment;

import rcpsp.util.Stopwatch;


/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractExperiment implements Experiment {
	protected final String name;
	protected final Stopwatch stopwatch = new Stopwatch();

	public AbstractExperiment(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return (name);
	}

	@Override
	public void runExperiment() {
		System.out.println("PERFORMING EXPERIMENT " + this.toString() + " * * *");
		stopwatch.start();
	}
	
	@Override
	public void endExperiment() {
		stopwatch.stop();
		System.out.println();
		System.out.println("FINISHED EXPERIMENT " + this.toString() + " * * *");
		System.out.println("Total time: " + stopwatch.getTotalTime());		
	}
	
}
