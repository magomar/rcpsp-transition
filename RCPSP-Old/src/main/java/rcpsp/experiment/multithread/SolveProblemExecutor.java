package rcpsp.experiment.multithread;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.Solution;
import rcpsp.data.jaxb.Project;

import java.util.concurrent.Callable;

/**
 * @author Mario Gomez
 */
public final class SolveProblemExecutor implements Callable<Solution> {
	final Scheduler scheduler;
	final Project project;

	/**
	 * @param scheduler
	 * @param project
	 */
	public SolveProblemExecutor(Scheduler scheduler, Project project) {
		this.scheduler = scheduler;
		this.project = project;
	}

	/* (non-Javadoc)
	 * @see java.util.concurrent.Callable#call()
	 */
	@Override
	public Solution call() throws Exception {
		Solution solution = scheduler.solve(project);
		scheduler.close();
		return solution;
	}

}
