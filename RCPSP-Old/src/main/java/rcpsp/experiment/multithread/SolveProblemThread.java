/**
 * 
 */
package rcpsp.experiment.multithread;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 *
 */
public final class SolveProblemThread extends Thread {
	final Scheduler scheduler;
	final Project project;

	public SolveProblemThread(Scheduler scheduler, Project project) {
		this.scheduler = scheduler;
		this.project = project;
	}

	@Override
	public void run() {
		scheduler.solve(project);
		scheduler.close();
	}	
	
}
