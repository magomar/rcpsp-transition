/**
 * 
 */
package rcpsp.jgrapht;

/**
 * @author Mario Gómez
 *
 */
public final class Node {
	private int id;

	/**
	 * @param id
	 */
	public Node(int id) {
		this.id = id;
	}

	public String toString() { 
		return "V"+id;        
	}
	public int getId() {
		return id;
	}
}
