/**
 *
 */
package rcpsp.tools.util;

import rcpsp.util.FileUtil;
import rcpsp.util.ProjectUtil;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gomez
 */
public final class FixDatasets {

    static final DatasetWrapper sourceWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson);
    static final DatasetWrapper targetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_fix);

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("");
        System.out.println("================== FIXING DATASETS ========================");
        System.out.println("");

        String[] sourceDatasetName = sourceWrapper.getDatasetName();
        String[] targetDatasetName = targetWrapper.getDatasetName();
        int projectCounter = 0;
        int incorrectProjects = 0;
        int fixedProjects = 0;
        // creates a new folder; if it existed, first deletes it and all its content
        FileUtil.createDir(Constant.datasetsPath + targetWrapper.getDatasetDefinition().getPath());
        for (int d = 0; d < sourceDatasetName.length; d++) {
            System.out.println("Fixing dataset " + sourceDatasetName[d]);
            System.out.println();
            List<Project> newProjects = new ArrayList<Project>();
//			 create file folder for the new dataset
            String datasetFolderName = Constant.datasetsPath + targetWrapper.getDatasetDefinition().getPath() + "/" + targetDatasetName[d] + targetWrapper.getDatasetDefinition().getProblemFormat();
            FileUtil.createDir(datasetFolderName);
//			load the source dataset
            BenchmarkResults benchmark = sourceWrapper.parseBenchmarkFile(sourceDatasetName[d]);
            Project[] project = sourceWrapper.loadDatasetProjects(benchmark);
            for (int p = 0; p < project.length; p++) {
                String newProblem = targetDatasetName[d] + (p + 1);
                System.out.print(".");
                if (!ProjectUtil.checkCorrectness(project[p])) {
                    incorrectProjects++;
                    if (ProjectUtil.canBeFixed(project[p])) {
                        fixedProjects++;
                    }
                    Project newProject = ProjectUtil.fix(project[p]);
                    newProjects.add(newProject);
                    targetWrapper.exportProject(newProject, targetDatasetName[d], newProblem);
                } else {
                    newProjects.add(project[p]);
                    targetWrapper.exportProject(project[p], targetDatasetName[d], newProblem);
                }
            }
            System.out.println();
            // generate new benchmark file with the new projects, but no solutions (they are still unknown)
            targetWrapper.generateBenchmark(newProjects, targetDatasetName[d]);
            projectCounter += project.length;
        }
        System.out.println();
        System.out.println(" * * * FINISHED: FixDatasets * * *");
        System.out.println("Total number of datasets created: " + targetDatasetName.length);
        System.out.println("Total number of problems generated: " + projectCounter);
        System.out.println("Correct: " + (projectCounter - incorrectProjects) + " / Incorrect: " + incorrectProjects);
        System.out.println("Fixed: " + fixedProjects + " / Non fixed: " + (incorrectProjects - fixedProjects));
    }
}
