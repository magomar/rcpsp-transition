/**
 *
 */
package rcpsp.tools.util;

import rcpsp.util.FileUtil;
import rcpsp.util.ProjectGenerator;
import rcpsp.util.ProjectUtil;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.Project;

import java.util.ArrayList;

/**
 * @author Mario Gomez This class is used to generate new datasets by randomly modifiying an existing dataset Note that
 * the execution of this class destroys the target dataset to create a new one
 */
public final class GenerateIndependentDatasets {

    static final DatasetWrapper sourceWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_fix);
    static final DatasetWrapper targetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_i);
    static final int numNewProjectsPerProject = 4;
    static final int minProjectSize = 13;
    static final int maxProjectSize = 25;
    static final int maxSuccessors = 5;
    static final int[] maxJobsToRemove = {5, 10, 5, 10};
    /**
     * If true, resources' capacities are reduced in source datasets before generating the new datasets
     */
    static final boolean reduceResourceCapacities = false;
    /**
     * If true, some precedence relations are removed in the source datasets before generating the new datasets
     */
    static final boolean removePrecedenceRelations = false;

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("");
        System.out.println("=================== GENERATING NEW DATASETS ==========================");
        System.out.println("");

        String sourceDatasetName = sourceWrapper.getDatasetName()[0];
        String[] targetDatasetName = targetWrapper.getDatasetName();
        // load the source dataset
        BenchmarkResults benchmark = sourceWrapper.parseBenchmarkFile(sourceDatasetName);
        Project[] project = sourceWrapper.loadDatasetProjects(benchmark);

        // prepare source projects
        ArrayList<Project> baseProjects = new ArrayList<Project>();
        for (int p = 0; p < project.length; p++) {
            if (project[p].getNumJobs() < maxProjectSize && project[p].getNumJobs() > minProjectSize) {
                for (int i = 0; i < numNewProjectsPerProject; i++) {
                    Project newProject = ProjectUtil.clone(project[p]);

                    if (reduceResourceCapacities) {
                        newProject = ProjectGenerator.reduceResources(project[p]);
                    }
                    baseProjects.add(newProject);
                }
            }
        }
        ArrayList<String> incorrectProjects = new ArrayList<String>();
        int projectCounter = 0;
//		creates a new folder, if it existed, first deletes it and all its content
        FileUtil.createDir(Constant.datasetsPath + targetWrapper.getDatasetDefinition().getPath());
        for (int d = 0; d < targetDatasetName.length; d++) {
            System.out.println("Generating dataset " + targetDatasetName[d]);
            ArrayList<Project> newProjects = new ArrayList<Project>();
//			create file folder for the new dataset
            String datasetFolderName = Constant.datasetsPath + targetWrapper.getDatasetDefinition().getPath() + "/" + targetDatasetName[d] + targetWrapper.getDatasetDefinition().getProblemFormat();
            FileUtil.createDir(datasetFolderName);
            int newProblemIndex = 0;
            for (Project p : baseProjects) {
                // remove some random jobs
                Project newProject = ProjectGenerator.removeJobs(p, maxJobsToRemove[d]);
                if (removePrecedenceRelations) {
                    newProject = ProjectGenerator.removePrecedenceRelations(p, maxSuccessors);
                }
                String newProblem = targetDatasetName[d] + ++newProblemIndex;
                targetWrapper.exportProject(newProject, targetDatasetName[d], newProblem);
                newProjects.add(newProject);
                System.out.print(".");
                if (!ProjectUtil.checkCorrectness(newProject)) {
                    incorrectProjects.add(newProblem);
                }
            }
            // generate new benchmark file with the new projects, but no solutions (they are still unknown)
            targetWrapper.generateBenchmark(newProjects, targetDatasetName[d]);
            projectCounter += newProblemIndex;
            System.out.println("");
        }
        System.out.println();
        System.out.println(" * * * FINISHED: GenerateDependentDatasets * * *");
        System.out.println("Source dataset: " + sourceDatasetName);
        System.out.println("Projects used as source: " + baseProjects.size() / numNewProjectsPerProject
                + " out of " + project.length);
        System.out.println("Size of new datasets: " + baseProjects.size() + "("
                + baseProjects.size() / numNewProjectsPerProject + "x" + numNewProjectsPerProject + ")");
        System.out.println("New datasets created: " + targetDatasetName.length);
        System.out.println("Total number of problems generated: " + projectCounter);
        if (incorrectProjects.size() > 0) {
            System.out.println("There are " + incorrectProjects.size() + " incorrect projects !");
            System.out.println("Incorrect projects are " + incorrectProjects.toString());
        }
        System.exit(0);
    }
}
