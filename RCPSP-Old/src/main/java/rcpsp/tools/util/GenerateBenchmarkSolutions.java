package rcpsp.tools.util;


import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.algorithms.scheduler.branchandbound.SprecherDrexlHeuristic;
import rcpsp.algorithms.scheduler.multiheuristic.MultiHeuristicSSGS;
import rcpsp.data.Solution;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.BenchmarkSolution;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.ProjectSchedule;
import rcpsp.experiment.TaskScheduler;

import java.util.Arrays;

/**
 * @author Mario Gomez
 */
public final class GenerateBenchmarkSolutions {
	static final long timeLimit = 1800000; // ms, half an hour = 1800000
//	static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_b);
        static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.example_casebase);
//	static final Scheduler optimalScheduler = new SprecherDrexlHeuristicTimeLimited(timeLimit);
	static final Scheduler optimalScheduler = new SprecherDrexlHeuristic();
	static final Scheduler multiHeuristicScheduler = new MultiHeuristicSSGS(Constant.singleRule);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String[] dataset = wrapper.getDatasetName();
        int numDatasets = dataset.length;
		BenchmarkResults[] newBenchmarkResults = new BenchmarkResults[numDatasets];
		BenchmarkSolution[][] newBenchmarkSolution = new BenchmarkSolution[numDatasets][];
		for (int d = 0; d < dataset.length; d++) {
			BenchmarkResults benchmarkResults  = wrapper.parseBenchmarkFile(dataset[d]);
			Project[] project = wrapper.loadDatasetProjects(benchmarkResults);
			Solution[] optimalSolution = TaskScheduler.solveMultipleProblemsConcurrently(optimalScheduler, project);
			Solution[] bestHeuristicSolution = TaskScheduler.solveMultipleProblemsConcurrently(multiHeuristicScheduler, project);
            int numProblems = benchmarkResults.getBenchmarkSolution().size();
			newBenchmarkSolution[d] = new BenchmarkSolution[numProblems];
			int problemIndex = 0;
			for (BenchmarkSolution benchmarkSolution : benchmarkResults.getBenchmarkSolution()) {
                BenchmarkSolution singleBenchmarkSolution = new BenchmarkSolution();
                singleBenchmarkSolution.setProblem(benchmarkSolution.getProblem());
                singleBenchmarkSolution.setLowerBound(benchmarkSolution.getLowerBound());
                singleBenchmarkSolution.setUpperBound(benchmarkSolution.getUpperBound());
                ProjectSchedule optimalSchedule = new ProjectSchedule();
                optimalSchedule.setMakespan(optimalSolution[problemIndex].getMakespan());
                singleBenchmarkSolution.setOptimalSolution(optimalSchedule);
                ProjectSchedule heuristicSchedule = new ProjectSchedule();
                heuristicSchedule.setMakespan(bestHeuristicSolution[problemIndex].getMakespan());
                singleBenchmarkSolution.setHeuristicSolution(heuristicSchedule);
                newBenchmarkSolution[d][problemIndex] = singleBenchmarkSolution;
                problemIndex++;
			}

            BenchmarkResults datasetBenchmarkResults = new BenchmarkResults();
            datasetBenchmarkResults.setDataset(benchmarkResults.getDataset());
            datasetBenchmarkResults.setType(benchmarkResults.getType());
            datasetBenchmarkResults.getBenchmarkSolution().addAll(Arrays.asList(newBenchmarkSolution[d]));
			newBenchmarkResults[d] = datasetBenchmarkResults;
			wrapper.generateBenchmarkWithSolutions(datasetBenchmarkResults);
		}
		System.out.println(" * * * FINISHED: GenerateBenchmarkResults * * *");

		// print results
		Report.reportSolutions(newBenchmarkResults, dataset, "GenerateBenchmarkResults");
		System.exit(0);
	}

}
