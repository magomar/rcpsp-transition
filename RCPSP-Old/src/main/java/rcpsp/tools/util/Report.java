/**
 *
 */
package rcpsp.tools.util;

import rcpsp.data.jaxb.BenchmarkResults;

import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;

/**
 * @author Mario Gomez
 *
 */
public final class Report {
	final static NumberFormat nf = NumberFormat.getInstance();

	/**
	 * @param algorithm
	 * @param dataset
	 * @param data
	 * @param experiment
	 */
	public static void reportStatistics(String[] algorithm, String[] dataset, 
			double[][][] data, String experiment) {

//		Compute statistics
		Statistics[][] stat = new Statistics[dataset.length][algorithm.length];
		Statistics[] statAlg = new Statistics[algorithm.length];
		for (int a = 0; a < algorithm.length; a++) {
			int numData = 0;
			for (int d = 0; d < dataset.length; d++) {
				stat[d][a] = new Statistics(data[d][a]);
				numData += data[d][a].length;
			}
			double[] algData = new double[numData];
			int i = 0;
			for (int d = 0; d < dataset.length; d++) 
				for (double x : data[d][a]) {
					algData[i] = x;
					i++;
				}
			statAlg[a] = new Statistics(algData);
		}

		StringBuilder s = new StringBuilder();
		s.append("Average deviation of the makespan from the best known solutions\n");
		s.append(String.format("%25s", "Algorithm"));
		for (int d = 0; d < dataset.length; d++) {
			s.append("\t");
			s.append(dataset[d]);
		}
		s.append("\tMean\tSt.Dev.\tMax\tMin\n");

		for (int a = 0; a < algorithm.length; a++) {
			s.append(String.format("%25s", algorithm[a]));
			for (int d = 0; d < dataset.length; d++) {
				s.append("\t");
				s.append(nf.format(stat[d][a].getMean()));
			}
			s.append("\t");
			s.append(nf.format(statAlg[a].getMean()));
			s.append("\t");
			s.append(nf.format(statAlg[a].getStDev()));
			s.append("\t");
			s.append(nf.format(statAlg[a].getMax()));
			s.append("\t");
			s.append(nf.format(statAlg[a].getMin()));
			s.append("\n");
		}

		// write results to text file
		final String outputFile = Constant.resultsPath + "Results" +experiment + Arrays.asList(dataset).toString() + ".txt";
		FileWriter fw = null;
		try {
			fw = new FileWriter(outputFile);
			fw.write(s.toString());
			fw.flush();
			fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		write results to default output (console)
		System.out.println(s.toString());
		System.out.println("Results written to: "+ outputFile);
		System.out.println();
	}

	/**
	 * Prints information about solutions found
	 * @param benchmarkResults
	 * @param dataset
	 * @param experiment
	 */
	public static void reportSolutions(BenchmarkResults[] benchmarkResults, String[] dataset, String experiment) {
		StringBuilder s = null;
		for (int d = 0; d < benchmarkResults.length; d++) {
			s = new StringBuilder();
			s.append(benchmarkResults[d]);
			// write results to text file
			final String outputFile = Constant.resultsPath + "Solutions" + experiment + dataset[d] + ".txt";
			FileWriter fw = null;
			try {
				fw = new FileWriter(outputFile);
				fw.write(s.toString());
				fw.flush();
				fw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

//			write results to default output (console)
			System.out.println("Solutions written to: "+ outputFile);
			System.out.println();
		}
	}

//	Print probabilities of finding the best solution (relative to all the algorithms compared)

//	public static void reportWiningProbabilities(String[] algorithm, String[] dataset, Statistics[][] stat,
//	String experiment) {
//	int[][] wins = new int[dataset.length][algorithm.length];
//	for (int d = 0; d < dataset.length; d++)
//	for (int i = 0; i < wins[d].length; i++)
//	wins[d][i] = 0;

//	// count the algorithms that found the best solution
//	for (int d = 0; d < dataset.length; d++)
//	for (int i = 0; i < algorithm.length; i++)
//	if (newSolution[i].getBenchmarkMakespan() <= bestSolution.getBenchmarkMakespan()) wins[d][i]++;

//	StringBuilder StringBuilder = new StringBuilder();
//	StringBuilder.append("\nProbability of finding the best solution");
//	StringBuilder.append(String.format("%25s", "Algorithm"));
//	for (int d = 0; d < dataset.length; d++) {
//	StringBuilder.append("\t");
//	StringBuilder.append(dataset[d]);
//	}
//	StringBuilder.append("\n");

//	for (int i = 0; i < algorithm.length; i++) {
//	StringBuilder.append(String.format("%25s", algorithm[i]));
//	for (int d = 0; d < dataset.length; d++) {

//	StringBuilder.append("\t");
//	StringBuilder.append(nf.format((double) wins[d][i]/(double) wins[d][0]));
//	}
//	System.out.println();
//	}
//	}

//	Print correlations

//	for (int i = 0; i < algorithm.length; i++) {
//	globalStats[i] = new Statistics(totalNumProblems);
//	for (int d = 0; d < dataset.length; d++) {
//	globalStats[i].addData(statistics[d][i].getData());
//	}
//	globalStats[i].update();
//	}
//	StringBuilder.append("Person's correlations");
//	for (int i = 0; i < algorithm.length; i++) {
//	StringBuilder.append(String.format("%25s", algorithm[i]));
//	for (int j = 0; j < algorithm.length; j++) {
//	double pearson =
//	(i==j ? 1 : Statistics.getPearsonCorrelation(globalStats[i], globalStats[j]));

//	StringBuilder.append("\t" + nf.format(pearson));
//	}
//	StringBuilder.append("\n");
//	}
//	StringBuilder.append("\n");


}
