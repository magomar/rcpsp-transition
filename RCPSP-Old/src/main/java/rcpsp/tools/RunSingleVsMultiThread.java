/**
 *
 */
package rcpsp.tools;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.algorithms.scheduler.branchandbound.SprecherDrexl;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.experiment.CompareSingleVsMultiThread;
import rcpsp.experiment.Experiment;

/**
 * @author Mario Gomez
 */
public final class RunSingleVsMultiThread {

    /**
     * @param args
     */
    public static void main(String[] args) {
//		Scheduler scheduler = ExperimentFactory.getAlgorithmsToCompareCBR()[4];
//		Scheduler scheduler = new SSGS();
        Scheduler scheduler = new SprecherDrexl();
//		Scheduler scheduler = new SprecherDrexlHeuristic();
        DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
        Experiment experiment = new CompareSingleVsMultiThread("Compare Single vs Multi-Thread", scheduler, datasetWrapper);
        experiment.runExperiment();
        experiment.endExperiment();
        System.exit(0);
    }

}
