/**
 * 
 */
package rcpsp.tools;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.experiment.CompareAlgorithmsConcurrently;
import rcpsp.experiment.Experiment;
import rcpsp.experiment.ExperimentFactory;

/**
 * @author Mario Gomez
 *
 */
public final class RunCompareAlgorithmsConcurrently {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareCBR(Constant.singleRule[0]);
		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareHeuristics();
		DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_b);
		Experiment experiment =  new CompareAlgorithmsConcurrently("CompareAlgorithmsConcurrently", scheduler, datasetWrapper);
		experiment.runExperiment();
		experiment.endExperiment();
		System.exit(0);
	}

}
