package rcpsp.tools;

import java.util.logging.Logger;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class DatasetConverter {

    private static final Logger LOG = Logger.getLogger(DatasetConverter.class.getName());
//    static final DatasetWrapper sourceWrapper = WrapperFactory.obtainWrapper(WrapperFactory.psplib_sm);

    public static void main(String[] args) {
//        System.out.println("");
//        System.out.println("================== CONVERTING DATASETS ========================");
//        System.out.println("");
//
//        String[] sourceDatasetName = sourceWrapper.getDatasetName();
//        int projectCounter = 0;
//
//        String folder = Constant.datasetsPath + sourceWrapper.getDatasetDefinition().getPath();
//
//        Marshaller marshaller;
//        JAXBContext jaxbContext;
//        try {
//            jaxbContext = JAXBContext.newInstance("rcpsp");
//            marshaller = jaxbContext.createMarshaller();
//            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        } catch (JAXBException ex) {
//            Logger.getLogger(DatasetConverter.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            JsonSchema jsonSchema = mapper.generateJsonSchema(Dataset.class);
//            String s = jsonSchema.toString();
//            LOG.log(Level.INFO, jsonSchema.toString());
//        } catch (JsonMappingException ex) {
//            LOG.log(Level.SEVERE, null, ex);
//        }
//
//        for (String datasetName : sourceDatasetName) {
//
//            System.out.println("converting dataset " + datasetName);
//            System.out.println();
//            Dataset dataset = new Dataset();
//            dataset.setName(datasetName);
//
//            String xmlFile = folder + datasetName + ".xml";
//            String jsonfile = folder + datasetName + ".json";
//
//            BenchmarkResults benchmark = sourceWrapper.parseBenchmarkFile(datasetName);
//            Project[] projects = sourceWrapper.loadDatasetProjects(benchmark);
//            List<Project> projectList = dataset.getProject();
//            projectList.addAll(Arrays.asList(projects));
//
//            projectCounter += projects.length;
//
//            //            try (FileOutputStream fos = new FileOutputStream(xmlFile)) {
//            //                try {
//            //                    marshaller.marshal(dataset, fos);
//            //                } catch (JAXBException ex) {
//            //                    LOG.log(Level.SEVERE, null, ex);
//            //                }
//            //            } catch (IOException ex) {
//            //                LOG.log(Level.SEVERE, null, ex);
//            //            }
//
//            File jFile = new File(jsonfile);
//            ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
//            try {
//                writer.writeValue(new File(jsonfile), dataset);
//            } catch (IOException ex) {
//                LOG.log(Level.SEVERE, null, ex);
//            }
//
////            try {
////                mapper.writeValue(jFile, dataset);
////            } catch (IOException ex) {
////                LOG.log(Level.SEVERE, null, ex);
////            }             
//            try {
//                Dataset newDataset = mapper.readValue(jFile, Dataset.class);
//            } catch (IOException ex) {
//                LOG.log(Level.SEVERE, null, ex);
//            }
//
//        }
//
//
//        System.out.println();
//        System.out.println(" * * * FINISHED: Convert Datasets * * *");
////        System.out.println("Total number of datasets created: " + sourceDatasetName.length);
////        System.out.println("Total number of projects in datasets: " + projectCounter);

    }
}
