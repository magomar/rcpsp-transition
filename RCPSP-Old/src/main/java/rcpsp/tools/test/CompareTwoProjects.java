package rcpsp.tools.test;

import rcpsp.algorithms.ProjectToGraphConverter;
import rcpsp.algorithms.matcher.BasicMatcher;
import rcpsp.algorithms.matcher.ProjectMatcher;
import rcpsp.data.ProjectMatching;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.data.jaxb.Project;
import simpack.api.IGraphAccessor;
import simpack.measure.graph.GraphIsomorphism;
import simpack.measure.graph.SubgraphIsomorphism;

import java.util.TreeSet;

public final class CompareTwoProjects {
//	 Change this to select a different collection of datasets (eg. patterson, psplib_sm, psplib_mm)
	static final DatasetWrapper wrapper1 = WrapperFactory.obtainWrapper(WrapperFactory.casebase);
	static final DatasetWrapper wrapper2 = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
	static final ProjectMatcher matcher = new BasicMatcher();
	
	static final Project project1 = wrapper1.parseProblem("casebase", "casebase1");
	static final Project project2 = wrapper2.parseProblem("pat_test", "pat_test1");
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		display using graph stream
//		ProjectToGraphConverter.convertToGraphStream(project1);
//		ProjectToGraphConverter.convertToGraphStream(project2);

		// Compare projects
		System.out.println();
		System.out.println("* * * Compare projects using SimPack * * *");
		ProjectMatching projectComparison = matcher.matchProjects(project1, project2);
		System.out.println(projectComparison);
		
		// Compare projects using SimPack
		System.out.println();
		System.out.println("* * * Compare projects using SimPack * * *");
		IGraphAccessor g1 = ProjectToGraphConverter.convertToSimPack(project1);
		IGraphAccessor g2 = ProjectToGraphConverter.convertToSimPack(project2);
		GraphIsomorphism gi = new GraphIsomorphism(g1, g2);
		gi.calculate();
		System.out.println("GraphIsomorphism: " + gi.getGraphIsomorphism());
		SubgraphIsomorphism sgi = new SubgraphIsomorphism(g1,g2);
		sgi.calculate();
		System.out.println("SubGraphIsomorphism: " + sgi.getSimilarity());
		TreeSet<String> cliqueList = sgi.getCliqueList();
		System.out.println("Maximal Cliques: " + cliqueList);
		
	// Compare projects using Absurdist
		/*	
		System.out.println();
		System.out.println("* * * Compare projects using ABSURDIST * * *");
		System.out.println();
		ConceptSystem systemO = ProjectToGraphConverter.convertToAbsurdist(project1);
		ConceptSystem systemN = ProjectToGraphConverter.convertToAbsurdist(project2);
		
		// Ensure that Absurdist will be created without 
		// expensive history arrays
		Absurdist.turnOffHistory();

		// learning rate (use default if none is supplied)
		String flag =  "L" ;
		if (System.getProperty(flag)!=null) {
			Absurdist.lRate =Double.parseDouble(System.getProperty(flag));
		}
		System.out.println( "Learning rate: -DL=" +Absurdist.lRate);

		// step count (use default if none is supplied)
		flag =  "maxLoop" ;
		if (System.getProperty(flag)!=null) {
			Absurdist.maxLoop =Integer.parseInt(System.getProperty(flag));
		}
		System.out.println( "maxLoop=" +Absurdist.maxLoop);
		
		Absurdist absurdist = new Absurdist( systemO, systemN );

		// resetChi: Default is true
		final String flag2 =  "coefMode" ;
		System.out.println( "-D"+flag2+"=" +absurdist.coefMode);

		// exp: Default is false
		absurdist.useExponential = Boolean.getBoolean( "exp" );
		System.out.println( "-Dexp=" + absurdist.useExponential);

		absurdist.mapSystem( );
		try {
			absurdist.print( );
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}

}
