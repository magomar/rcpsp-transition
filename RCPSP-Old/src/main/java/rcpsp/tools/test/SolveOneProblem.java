/**
 *
 */
package rcpsp.tools.test;


import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.jobselection.LST;
import rcpsp.algorithms.matcher.BasicMatcher;
import rcpsp.algorithms.matcher.ProjectMatcher;
import rcpsp.algorithms.sampler.ModifiedRBRS;
import rcpsp.algorithms.sampler.Sampler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.algorithms.scheduler.branchandbound.SprecherDrexlV2;
import rcpsp.algorithms.scheduler.branchandboundv2.SprecherAndDrexl;
import rcpsp.algorithms.scheduler.serialgenerationscheme.SSGS;
import rcpsp.data.Solution;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.data.jaxb.Project;
import rcpsp.util.ProjectUtil;

import java.text.NumberFormat;

/**
 * @author Mario Gomez
 *
 */
public final class SolveOneProblem {

    static final NumberFormat nf = NumberFormat.getInstance();
    /**
     * CBR wrapper: accesses the problems referred by the cases in the case_base
     */
//    static final DatasetWrapper cbWrapper = WrapperFactory.obtainWrapper(WrapperFactory.casebase);
    static final DatasetWrapper cbWrapper = WrapperFactory.obtainWrapper(WrapperFactory.example_casebase);
    /**
     * target wrapper: accesses the new problems to be solved
     */
//	Change this to select a different collection of datasets (eg. patterson, psplib_sm, psplib_mm)
    static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.psplib_sm);
//	static final DatasetWrapper wrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
    /**
     * Single dataset to select a problem from
     */
    static final String dataset = "j30";
    /**
     * Single problem to be soved (from the selected dataset)
     */
//    static final String problem = "j302_3";
//    static final String problem = "j301_7";
    static final String problem = "j301_1";
    /**
     * Sampling parameter: number of iterations
     */
    static final int iterations = 1000;
    /**
     * Sampling parameter: epsilon
     */
    static final double epsilon = 1;
    /**
     * Sampling parameter: alpha
     */
    static final double alpha = 1;
    /**
     * Sampling parameter: lambda
     */
    static final double lambda = 10;
    /**
     * Heuristic Priority Rule
     */
    static final JobPriorityRule jobPriorityRule = new LST();
//    static final JobPriorityRule jobPriorityRule = new LFT();
    /**
     * Sampler
     */
    static final Sampler sampler = new ModifiedRBRS(alpha, lambda);
    /**
     * Project Matcher Algorithm, used to compare projects in the case base, which aims at finding simmilar projects.
     * The algorithms.matcher is used by the retrieval cbrcps.algorithms when applying Case Based Reasoning to schedule projects.
     */
    static final ProjectMatcher matcher = new BasicMatcher();
    /**
     * k parameter for the K-NN case retrieval algorithm
     */
    static final int k = 1;

    /**
     * @param args
     */
    public static void main(String[] args) {
//        CBRCPS cbrcps = new CBRCPS(k);
//        try {
//            cbrcps.configure();
//            cbrcps.preCycle();
//        } catch (ExecutionException e) {
//        }

        // Parse project from file
        Project project = wrapper.parseProblem(dataset, problem);

        if (!ProjectUtil.checkCorrectness(project)) {
            System.out.println("The project is WRONG ! ");
        }

        System.out.println();
        System.out.println(project.toString() + "\n");

//		DirectedGraph<Node,Link> graph = ProjectToGraphConverter.convertToJGraphT(project);
//		System.out.println(graph.toString());

//        Graph g = ProjectToGraphConverter.convertToGraphStream(project);
////        GraphViewerRemote viewerRemote = g.display();
////        viewerRemote.setQuality(4);	// 4 is the max quality (range is 0-4).
//        g.addAttribute("ui.stylesheet", getStyleSheet(project));
//        Viewer v = g.display();
        Solution newSolution;
        Scheduler scheduler;

        scheduler = new SSGS(jobPriorityRule);
        newSolution = scheduler.solve(project);
        scheduler.close();
//        System.out.println(newSolution.toGanttDiagram() + "\n");
//        System.out.println(newSolution.toResourceAvailability() + "\n");

        scheduler = new SprecherAndDrexl();
        newSolution = scheduler.solve(project);
        scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");
                
        scheduler = new SprecherDrexlV2();
        newSolution = scheduler.solve(project);
        scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//        scheduler = new SprecherDrexlHeuristic(jobPriorityRule);
//        newSolution = scheduler.solve(project);
//        scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//                scheduler = new SSGS_CBR(new CBPR_AR(jobPriorityRule), jobPriorityRule, cbrcps, cbWrapper, algorithms.matcher);
//		newSolution = scheduler.solve(project);
//		scheduler.close();
//		System.out.println(newSolution.toGanttDiagram() + "\n");
//		System.out.println(newSolution.toResourceAvailability() + "\n");

//		Execute the postcycle, once the CBRCPS application is not needed anymore
//		try {
//			cbrcps.postCycle();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		}

//		System.exit(0);
    }

    /**
     * @param project
     * @return
     */
    private static String getStyleSheet(Project project) {
        String s =
                "graph {"
                + "    color:yellow;"
                + "}"
                + "node {"
                + "	 width:20;"
                + "    text-color: black;"
                + "    fill-color: yellow;"
                + "}"
                + "edge {"
                + "    width:1;"
                + "    color:black;"
                + "}"
                + "node#J0 {"
                + "    fill-color:green;"
                + "}"
                + "node#J" + (project.getNumJobs() - 1) + " {"
                + "    fill-color:red;"
                + "}";
        return s;
    }
}
