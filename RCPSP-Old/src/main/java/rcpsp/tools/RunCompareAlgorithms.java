package rcpsp.tools;

import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.io.WrapperFactory;
import rcpsp.experiment.CompareAlgorithms;
import rcpsp.experiment.Experiment;
import rcpsp.experiment.ExperimentFactory;

/**
 * @author Mario Gomez
 *
 */
public final class RunCompareAlgorithms {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareCBR(Constant.singleRule[0]);
		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareHeuristics();
//		Scheduler[] scheduler = ExperimentFactory.getAlgorithmsToCompareSampling();
		DatasetWrapper datasetWrapper = WrapperFactory.obtainWrapper(WrapperFactory.patterson_test_a);
		Experiment experiment =  new CompareAlgorithms("CompareAlgorithms", scheduler, datasetWrapper);
		experiment.runExperiment();
		experiment.endExperiment();
		System.exit(0);
	}

}
