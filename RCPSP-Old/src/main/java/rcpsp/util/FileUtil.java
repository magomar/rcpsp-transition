/**
 * 
 */
package rcpsp.util;

import java.io.File;

/**
 * @author Mario Gomez
 */
public final class FileUtil {

    /**
     * Deletes all files and subdirectories under dir. Returns true if all deletions were successful.
     * If a deletion fails, the method stops attempting to delete and returns false.
     * @param dir
     * @return true if the operation is successful, false otherwhise
     */
    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
    
        // The directory is now empty so delete it
        return dir.delete();
    } 
	
    /**
     * @param datasetFolderName
     * @return true if the operation is successful, false otherwhise
     */
    public static void createDir(String datasetFolderName) {
		File datasetFolder = new File(datasetFolderName);
		deleteDir(datasetFolder);
		datasetFolder.mkdir();
    }
}
