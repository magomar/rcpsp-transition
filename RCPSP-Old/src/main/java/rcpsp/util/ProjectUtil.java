package rcpsp.util;

import rcpsp.data.ProjectDescription;
import rcpsp.data.jaxb.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Mario Gomez <margomez at dsic.upv.es>
 */
public class ProjectUtil {

    public static Project clone(Project project) {
        throw new UnsupportedOperationException("Method not supported yet");
//           Project clone = (Project) project.clone();
        //        clone.name = name;
        //        clone.projectNumber = projectNumber;
        //        clone.numJobs = numJobs;
        //        clone.numRenewableResources = numRenewableResources;
        //        clone.numNonRenewableResources = numNonRenewableResources;
        //        clone.numDoublyConstrainedResources = numDoublyConstrainedResources;
        //        clone.job = new ArrayList<>();
        //        for (Job j : job) {
        //            clone.job.add((Job) j.clone());
        //        }
        //        clone.resource = new ArrayList<>();
        //        for (Resource r : resource) {
        //            clone.resource.add((Resource) r.clone());
        //        }
        //        clone.relDate = relDate;
        //        clone.dueDate = dueDate;
        //        clone.tardCost = tardCost;
        //        clone.MPMTime = MPMTime;
        //return clone
    }

    public static Job clone(Job job) {
        throw new UnsupportedOperationException("Method not supported yet");
//        Job clone = (Job) super.clone();
//        clone.id = id;
//        clone.numSuccessors = numSuccessors;
//        clone.numModes = numModes;
//        clone.successor = new ArrayList<>();
//        for (Integer s : this.successor) {
//            clone.successor.add(s);
//        }
//        clone.mode = new ArrayList<>();
//        for (Mode m : this.mode) {
//            clone.mode.add((Mode) m.clone());
//        }
//        return clone;
    }

    public static Mode clone(Mode mode) {
        throw new UnsupportedOperationException("Method not supported yet");
//        Mode clone = (Mode) super.clone();
//        clone.id = id;
//        clone.duration = duration;
//        clone.resourceRequest = new ArrayList<Integer>();
//        for (Integer request : resourceRequest) {
//            clone.resourceRequest.add(request); // Primitive types and their wrappers are inmutable so they don't need to be cloned
//        }
//        return clone;
    }

    public static Resource clone(Resource resource) {
        throw new UnsupportedOperationException("Method not supported yet");
//        Resource clone = (Resource) super.clone();
//        clone.id = id;
//        clone.name = name;
//        clone.capacity = capacity;
//        clone.isRenewable = isRenewable;
//        return clone;
    }

    /**
     * Checks whether a project is correctly specified
     *
     * @param project
     * @return
     */
    public static boolean checkCorrectness(Project project) {
        //TODO check other properties...
        boolean isOK = true;
        List<String> errors = new ArrayList<>();
        ProjectDescription pd = new ProjectDescription(project);
        List<Integer>[] predecessors = pd.getPredecessors();
//        Job startJob = project.getJob().get(0);
        for (Integer successor : project.getJob().get(0).getSuccessor()) {
            if (predecessors[successor.intValue()].size() > 1) {
                isOK = false;
                errors.add("Error ! job " + successor + " incorrectly has start job as predecessor");
            }
        }
        for (Integer predecessor : predecessors[project.getNumJobs() - 1]) {
            Job job = project.getJob().get(predecessor.intValue());
            if (job.getNumSuccessors() > 1) {
                isOK = false;
                errors.add("Error ! job " + predecessor + " incorrectly has end job as successor");
            }
        }
        for (Job job : project.getJob()) {
            int j = job.getId();
            if (job.getNumSuccessors() == 0 && j != project.getNumJobs() - 1) {
                isOK = false;
                errors.add("Error ! job " + j + " has no successors");
            }
            if (predecessors[j].isEmpty() && j != 0) {
                isOK = false;
                errors.add("Error ! job " + j + " has no predecessors");
            }
            for (Integer successor : job.getSuccessor()) {
                int k = successor.intValue();
                if (k <= j) {
                    isOK = false;
                    errors.add("Error ! job " + j + "can't have " + k + " as successor");
                }
            }
        }
        int nRR = project.getNumRenewableResources();
        int nNRR = project.getNumNonRenewableResources();


        // check whether resource requirements are satisfiable
        int numResources = project.getResource().size();
        int[] maxRequest = new int[numResources];
        int[] resourceCapacity = new int[numResources];
        int r = 0;
        for (Resource resource : project.getResource()) {
            resourceCapacity[r] = resource.getCapacity();
            maxRequest[r] = 0;
            r++;
        }

        for (Job j : project.getJob()) {
            int[] maxPartialRequest = new int[numResources];
            for (r = 0; r < numResources; r++) {
                maxPartialRequest[r] = 0;
            }
            for (Mode m : j.getMode()) {
                for (r = 0; r < numResources; r++) {
                    maxPartialRequest[r] = Math.max(maxPartialRequest[r], m.getResourceRequest().get(r));
                }
            }
            for (r = 0; r < project.getNumRenewableResources(); r++) {
                maxRequest[r] = Math.max(maxRequest[r], maxPartialRequest[r]);
            }
            for (r = project.getNumRenewableResources(); r < numResources; r++) {
                maxRequest[r] = maxRequest[r] + maxPartialRequest[r];
            }
        }

        for (r = 0; r < numResources; r++) {
            if (maxRequest[r] > resourceCapacity[r]) {
                isOK = false;
                errors.add("Error ! capacity of resource " + project.getResource().get(r).getName() + " exceeded");
            }
        }
        if (!isOK) {
            System.out.println();
            System.out.println("Project " + project.getName() + " has problems !");
            System.out.println(errors);
        }
        return isOK;
    }

    /**
     * @param project
     * @return true if project can be fixed calling the {@link cbrcps.model.Project#fix(Project project) fix} method
     */
    public static boolean canBeFixed(Project project) {
        boolean canBeFixed = true;
        for (Job job : project.getJob()) {
            int j = job.getId();
            // checks whether a successor's ID is lower than j
            for (Integer successor : job.getSuccessor()) {
                int k = successor.intValue();
                if (k <= j) {
                    canBeFixed = false;
                }
            }
        }
        return canBeFixed;
    }

    /**
     * This method fixes a number of problems that may appear in a project definition
     *
     * @param project
     * @return a new instance of the input project
     */
    public static Project fix(Project project) {
        Project newProject = ProjectUtil.clone(project);


        boolean isOK = true;
        List<String> errors = new ArrayList<>();

        // remove the successors of the start job that have more than one predecessor 
        ProjectDescription pd = new ProjectDescription(newProject);
        List<Integer>[] predecessors = pd.getPredecessors();
        List<Integer> newStartSuccessors = new ArrayList<>();
        Job startJob = newProject.getJob().get(0);
        for (Integer successor : startJob.getSuccessor()) {
            if (predecessors[successor.intValue()].size() == 1) {
                newStartSuccessors.add(successor);
            }
        }
        startJob.getSuccessor().clear();
        startJob.getSuccessor().addAll(newStartSuccessors);
        startJob.setNumSuccessors(newStartSuccessors.size());

        // remove the end job from successors of jobs having more than 1 successor
        pd = new ProjectDescription(newProject);
        predecessors = pd.getPredecessors();
        Job endJob = newProject.getJob().get(newProject.getNumJobs() - 1);
        for (Integer predecessor : predecessors[endJob.getId()]) {
            Job job = newProject.getJob().get(predecessor.intValue());
            if (job.getNumSuccessors() > 1) {
                job.getSuccessor().remove(endJob.getId());
                job.setNumSuccessors(job.getNumSuccessors() - 1);
            }
        }

        for (Job job : newProject.getJob()) {
            int j = job.getId();
            // for projects without successors add the end job
            if (job.getNumSuccessors() == 0 && j != newProject.getNumJobs() - 1) {
                job.getSuccessor().add(newProject.getNumJobs() - 1);
                job.setNumSuccessors(1);
            }
            // for jobs withouth predecessors set them as successors of the start job
            if (predecessors[j].isEmpty() && j != 0) {
                startJob.getSuccessor().add(j);
                startJob.setNumSuccessors(startJob.getNumSuccessors() + 1);
            }
            for (Integer successor : job.getSuccessor()) {
                int k = successor.intValue();
                if (k <= j) {
                    isOK = false;
                    errors.add("Error ! job " + j + "can't have " + k + " as successor");
                }
            }
        }

        // check whether resource requirements are satisfiable
        int numResources = project.getNumRenewableResources() + project.getNumNonRenewableResources();
        int[] maxRequest = new int[numResources];
        int[] resourceCapacity = new int[numResources];
        int r = 0;
        for (Resource resource : project.getResource()) {
            resourceCapacity[r] = resource.getCapacity();
            maxRequest[r] = 0;
            r++;
        }

        for (Job j : project.getJob()) {
            int[] maxPartialRequest = new int[numResources];
            for (r = 0; r < numResources; r++) {
                maxPartialRequest[r] = 0;
            }
            for (Mode m : j.getMode()) {
                for (r = 0; r < numResources; r++) {
                    maxPartialRequest[r] = Math.max(maxPartialRequest[r], m.getResourceRequest().get(r));
                }
            }
            for (r = 0; r < project.getNumRenewableResources(); r++) {
                maxRequest[r] = Math.max(maxRequest[r], maxPartialRequest[r]);
            }
            for (r = project.getNumRenewableResources(); r < numResources; r++) {
                maxRequest[r] = maxRequest[r] + maxPartialRequest[r];
            }
        }
        // if resource capacity is exceeded set it to the maxRequest
        for (r = 0; r < numResources; r++) {
            if (maxRequest[r] > resourceCapacity[r]) {
                newProject.getResource().get(r).setCapacity(maxRequest[r]);
            }
        }
        if (!isOK) {
            System.out.println();
            System.out.println("Project " + newProject.getName() + " still has non solved problems !");
            System.out.println(errors);
        }

        return newProject;
    }

    public static String toString(Project p) {
        StringBuilder s = new StringBuilder();
        s.append("PROJECT INFORMATION\n===================\nProject Name: ").append(p.getName());
        s.append("\nActivities: ").append(p.getJob().size());
        s.append("\nRenewable Resources: ").append(p.getNumRenewableResources());
        s.append("\nNon Renewable Resources: ").append(p.getNumNonRenewableResources());
//        s.append("\nDoubly Constrained Resources: ").append(p.getNumDoublyConstrainedResources());
        s.append("\nRel. Date: ").append(p.getRelDate());
        s.append("\nDue Date: ").append(p.getDueDate());
        s.append("\nTardiness Cost: ").append(p.getTardCost());
        s.append("\nMPM Time: ").append(p.getMPMTime());
        s.append("\nTime Horizon: ").append(p.getTimeHorizon());
        s.append("\n************************************************************\nPRECEDENCE RELATIONS\njobnr.\t#modes\t#successors\tsuccessors\n");
        for (Job j : p.getJob()) {
            s.append(toString(j));
        }
        s.append("************************************************************\nREQUESTS/DURATIONS\njobnr\tmode\tduration\t");
        for (Resource r : p.getResource()) {
            s.append(r.getName());
            s.append("\t");
        }
        s.append("\n");
        for (Job j : p.getJob()) {
            for (Mode m : j.getMode()) {
                s.append(j.getId());
                s.append("\t");
                s.append(toString(m));
            }
        }
        s.append("************************************************************\nRESOURCE AVAILABILITIES\n");
        for (Resource r : p.getResource()) {
            s.append(r.getName());
            s.append("\t");
        }
        s.append("\n");
        for (Resource r : p.getResource()) {
            s.append(r.getCapacity());
            s.append("\t");
        }
        return s.toString();
    }

    public static String toString(Job j) {
        StringBuilder s = new StringBuilder();
        s.append(j.getId()).append("\t").append(j.getMode().size());
        s.append("\t").append(j.getSuccessor().size());
        s.append("\t\t");
        for (Integer i : j.getSuccessor()) {
            s.append(i).append('\t');
        }
        s.append('\n');
        return s.toString();
    }

    public static String toString(Mode m) {
        StringBuilder s = new StringBuilder();
        	s.append(m.getId()).append("\t");
		s.append(m.getDuration()).append("\t\t");
		for (Integer request : m.getResourceRequest()) {
			s.append(request).append('\t');
		}
		s.append('\n');

        return s.toString();
    }

    public static String toString(List<JobSchedule> schedule) {
        StringBuilder s = new StringBuilder();
        for (JobSchedule jobSchedule : schedule) {
            s.append(jobSchedule.getId());
            s.append(" ");
        }
        return s.toString();
    }

    public static String toString(int[] jobsArray) {
        StringBuilder s = new StringBuilder();
        for (int i = 1; i < jobsArray.length; i++) {
            s.append(jobsArray[i]);
            s.append(" ");
        }
        return s.toString();
    }

}
