/**
 * 
 */
package rcpsp.data;

import java.util.Map;

/**
 * @author Mario Gomez
 *
 */
public final class ProjectMatching {
	private double similarity;
	private Map<Integer,Integer> map;

	/**
	 * @param simmilarity
	 * @param map
	 */
	public ProjectMatching(double simmilarity, Map<Integer, Integer> map) {
		this.similarity = simmilarity;
		this.map = map;
	}

	/**
	 * @return
	 */
	public double getSimilarity() {
		return similarity;
	}
	/**
	 * @return
	 */
	public Map<Integer, Integer> getMap() {
		return map;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = similarity + "\n" + map;
		return s;
	}
}
