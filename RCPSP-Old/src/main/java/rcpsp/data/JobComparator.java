/**
 * 
 */
package rcpsp.data;

import rcpsp.data.jaxb.Job;

import java.util.Comparator;

/**
 * @author Mario Gomez
 */
public final class JobComparator implements Comparator<Job> {

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(Job job1, Job job2) {
		return (job1.getId() - job2.getId());
	}

}
