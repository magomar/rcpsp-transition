package rcpsp.data;

import rcpsp.data.jaxb.Resource;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class ResourcesArray implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;
    private int[] leftoverCapacity;
    private int[] initialCapacity;

    /**
     * @param resources
     */
    public ResourcesArray(List<Resource> resources) {
        leftoverCapacity = new int[resources.size()];
        initialCapacity = new int[resources.size()];
        for (Resource resource : resources) {
            leftoverCapacity[resource.getId()] = resource.getCapacity();
            initialCapacity[resource.getId()] = resource.getCapacity();
        }
    }

    /**
     * @param resources
     */
    public ResourcesArray(Resource[] resources) {
        leftoverCapacity = new int[resources.length];
        initialCapacity = new int[resources.length];
        for (Resource resource : resources) {
            leftoverCapacity[resource.getId()] = resource.getCapacity();
            initialCapacity[resource.getId()] = resource.getCapacity();
        }
    }

    /**
     * @param leftoverCapacity
     * @param initialCapacity
     */
    public ResourcesArray(int[] leftoverCapacity, int[] initialCapacity) {
        this.leftoverCapacity = leftoverCapacity;
        this.initialCapacity = initialCapacity;
    }

    /**
     * Reduces the leftover capacity of a resource by using/consuming a certain amount
     *
     * @param resourceID
     * @param amount
     */
    public void useResource(int resourceID, int amount) {
        leftoverCapacity[resourceID] -= amount;
    }

    /**
     * Partially restores the capacity of a resource by adding certain amount
     *
     * @param resourceID
     * @param amount
     */
    public void liberateResource(int resourceID, int amount) {
        leftoverCapacity[resourceID] += amount;
    }

    /**
     * Checks whether a list of resource requests can be served given the leftover capacities
     *
     * @param requests
     * @return
     */
    public boolean isResourceFeasible(List<Integer> requests) {
        boolean result = true;
        int resourceID = 0;
        for (Integer request : requests) {
            if (leftoverCapacity[resourceID] < request.intValue()) {
                result = false;
                break;
            }
            resourceID++;
        }
        return result;
    }

    /**
     * Restores resources: sets all the resource capacities to its initial capacities
     */
    public void reset() {
        System.arraycopy(initialCapacity, 0, leftoverCapacity, 0, leftoverCapacity.length);
    }

    /**
     * @return the leftoverCapacity
     */
    public int[] getLeftoverCapacity() {
        return leftoverCapacity;
    }

    /**
     * @return the initialCapacity
     */
    public int[] getInitialCapacity() {
        return initialCapacity;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
//		final DecimalFormat df = new DecimalFormat("0.0");
        final NumberFormat nf = NumberFormat.getInstance();
        List<String> usages = new ArrayList<>();
        for (int i = 0; i < leftoverCapacity.length; i++) {
            double usage = (double) leftoverCapacity[i] / initialCapacity[i];
            usages.add(nf.format(usage));
        }
        return usages.toString();
    }

    /**
     * Returns a deep copy instance of the ResourcesArray object passed as parameter
     *
     * @param resourcesArray
     * @return the duplicate of provided object
     */
    public static ResourcesArray newInstance(ResourcesArray resourcesArray) {
        return new ResourcesArray(resourcesArray.getLeftoverCapacity().clone(),
                resourcesArray.getInitialCapacity().clone());
    }
}
