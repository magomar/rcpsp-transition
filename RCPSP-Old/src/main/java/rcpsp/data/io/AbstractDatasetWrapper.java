package rcpsp.data.io;

import rcpsp.algorithms.Constant;
import rcpsp.data.DatasetDefinition;
import rcpsp.data.jaxb.BenchmarkResults;
import rcpsp.data.jaxb.BenchmarkSolution;
import rcpsp.data.jaxb.BenchmarkType;
import rcpsp.data.jaxb.Project;
import rcpsp.util.ProjectUtil;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractDatasetWrapper implements DatasetWrapper {

    private static final Logger LOG = Logger.getLogger(AbstractDatasetWrapper.class.getName());
    protected final String path;
    protected final String[] datasetName;
    protected final BenchmarkType[] benchmarkType;
    protected final String benchmarkFormat;
    protected final String problemFormat;
    private final DatasetDefinition datasetDefinition;

    /**
     * @param datasetDefinition
     */
    public AbstractDatasetWrapper(DatasetDefinition datasetDefinition) {

//        Properties prop = new Properties();
//        try {
//            prop.load(new FileInputStream("configuration.properties"));
//        } catch (IOException ex) {
//            LOG.log(Level.SEVERE, null, ex);
//        }

        this.path = Constant.datasetsPath + datasetDefinition.getPath() + "/";
        this.datasetName = datasetDefinition.getDataset();
        this.benchmarkType = datasetDefinition.getBenchmarkType();
        this.benchmarkFormat = datasetDefinition.getBenchmarkFormat();
        this.problemFormat = datasetDefinition.getProblemFormat();
        this.datasetDefinition = datasetDefinition;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#getDatasetNames()
     */
    @Override
    public String[] getDatasetName() {
        return datasetName;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#getPath()
     */
    @Override
    public DatasetDefinition getDatasetDefinition() {
        return datasetDefinition;
    }

    /**
     * @param datasetName
     * @return
     */
    protected int getDatasetIndex(String datasetName) {
        int datasetIndex = -1;
        for (int i = 0; i < this.datasetName.length; i++) {
            if (this.datasetName[i].equals(datasetName)) {
                datasetIndex = i;
                break;
            }
        }
        return datasetIndex;
    }

    /**
     * @param benchmark
     * @return
     */
    @Override
    public Project[] loadDatasetProjects(BenchmarkResults benchmark) {
        print("Loading " + benchmark.getDataset() + " projects");
        Project[] project = new Project[benchmark.getBenchmarkSolution().size()];
        ArrayList<String> incorrectProjects = new ArrayList<>();
        int problemIndex = 0;
        for (BenchmarkSolution benchmarkSolution : benchmark.getBenchmarkSolution()) {
            project[problemIndex] = parseProblem(benchmark.getDataset(), benchmarkSolution.getProblem());
            System.out.print(".");
            if (!ProjectUtil.checkCorrectness(project[problemIndex])) {
                incorrectProjects.add(project[problemIndex].getName());
            }
            problemIndex++;
        }
        System.out.println();
        print("Number of projects loaded: " + problemIndex);
        if (incorrectProjects.size() > 0) {
            print("There are " + incorrectProjects.size() + " incorrect projects !");
            print("Incorrect projects are " + incorrectProjects.toString());
        }

        return project;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#generateBenchmark(java.util.ArrayList, java.lang.String)
     */
    @Override
    public void generateBenchmark(List<Project> newProjects, String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
        StringBuilder s = new StringBuilder();
        s.append("=============================\n");
        s.append("Instance Makespan\n");
        s.append("------------------------------\n");
        int index = 1;
        for (Project project : newProjects) {
            s.append(index);
            s.append("\t");
            //s.append(0); // set makespan to 0, meaning the optimal makespan is "unknown"
            s.append(project.getTimeHorizon()); // set makespan to time-horizon (the worst possible case)
            s.append("\n");
            index++;
        }
        // write results to text file
        try (FileWriter fw = new FileWriter(fileName)) {
            fw.write(s.toString());
        } catch (IOException ex) {
            Logger.getLogger(AbstractDatasetWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }

        print("Benchmark " + datasetName + " has been exported to " + fileName + " without solutions");
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#generateBenchmarkWithSolutions(cbrcps.cbrcps.rcpsp.core.data.BenchmarkResults, java.lang.String)
     */
    @Override
    public void generateBenchmarkWithSolutions(BenchmarkResults benchmarkResults) {
        String datasetName = benchmarkResults.getDataset();
        int datasetIndex = getDatasetIndex(datasetName);
        // First generate the standard benchmark file containing the problem instances and their makespan
        String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
        StringBuilder s = new StringBuilder();
        s.append("=============================\n");
        s.append("Instance Makespan\n");
        s.append("------------------------------\n");
        int index = 1;
        for (BenchmarkSolution solution : benchmarkResults.getBenchmarkSolution()) {
            s.append(index);
            s.append("\t");
            s.append(solution.getOptimalSolution().getMakespan());
            s.append("\n");
            index++;
        }
        // write results to text file
        try (FileWriter fw = new FileWriter(fileName)) {
            fw.write(s.toString());
        } catch (IOException ex) {
            Logger.getLogger(AbstractDatasetWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        print("Benchmark " + datasetName + "has been exported to " + fileName + " with solutions");
        // Now, serialize the benchmark with all the solutions
        saveBenchmarkResults(benchmarkResults, datasetName);
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#loadBenchmarkResults(java.lang.String)
     */
    @Override
    public BenchmarkResults loadBenchmarkResults(String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        // Now, serialize the benchmark with all the solution
        String fileName = path + datasetName + benchmarkType[datasetIndex] + ".ser";
        BenchmarkResults benchmark = null;
        try (FileInputStream fis = new FileInputStream(fileName)) {
            ObjectInputStream in = new ObjectInputStream(fis);
            benchmark = (BenchmarkResults) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(AbstractDatasetWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        print(datasetName + " solutions have been loaded from " + fileName);
        return benchmark;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#saveBenchmarkResults(cbrcps.cbrcps.rcpsp.core.data.BenchmarkResults, java.lang.String)
     */
    @Override
    public void saveBenchmarkResults(BenchmarkResults benchmark, String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);
        // Now, serialize the benchmark with all the solution
        String fileName = path + datasetName + benchmarkType[datasetIndex] + ".ser";
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(benchmark);
        } catch (IOException ex) {
            Logger.getLogger(AbstractDatasetWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }
        print(datasetName + " solutions have been saved to " + fileName);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    protected void print(String msg) {
        System.out.println(toString() + ": " + msg);
    }
}
