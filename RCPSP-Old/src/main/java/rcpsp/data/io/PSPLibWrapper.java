/**
 *
 */
package rcpsp.data.io;

import rcpsp.data.DatasetDefinition;
import rcpsp.data.jaxb.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 */
public final class PSPLibWrapper extends AbstractDatasetWrapper {

    public PSPLibWrapper(DatasetDefinition wrapperDetails) {
        super(wrapperDetails);
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#parseBenchmarkFile()
     */
    @Override
    public BenchmarkResults parseBenchmarkFile(String datasetName) {
        int datasetIndex = getDatasetIndex(datasetName);

        String fileName = path + datasetName + benchmarkType[datasetIndex] + benchmarkFormat;
        File file = new File(fileName);

        Scanner fileScanner = null;
        try {
            fileScanner = new Scanner(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PSPLibWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }


        Scanner lineScanner = new Scanner(fileScanner.nextLine());

        // Skips non useful lines
        while (!lineScanner.hasNext() && !lineScanner.next().startsWith("Par")) {
            lineScanner = new Scanner(fileScanner.nextLine());
        }
        lineScanner = new Scanner(fileScanner.nextLine());

        //parse benchmark results for each individual instance
        ArrayList<BenchmarkSolution> solutions = new ArrayList<>();
        while (fileScanner.hasNextLine()) {
            lineScanner = new Scanner(fileScanner.nextLine());
            if (lineScanner.hasNextInt()) {
                String problem = datasetName + lineScanner.nextInt() + "_" + lineScanner.nextInt();
                int upperBound = lineScanner.nextInt();
                int lowerBound = (benchmarkType[datasetIndex].equals(BenchmarkType.LB) ? lineScanner.nextInt() : upperBound);
                if (upperBound < 16384) // if solution exists
                {
                    BenchmarkSolution bs = new BenchmarkSolution();
                    bs.setProblem(problem);
                    bs.setLowerBound(lowerBound);
                    bs.setUpperBound(upperBound);
                    solutions.add(bs);
                }
            }
        }
        lineScanner.close();
        fileScanner.close();
        System.out.println("Dataset benchmark successfully parsed from file: " + file.getName());
        BenchmarkResults br = new BenchmarkResults();
        br.setDataset(datasetName);
        br.setType(benchmarkType[datasetIndex]);
        br.getBenchmarkSolution().addAll(solutions);
        return br;
    }

    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#parseProblem(java.lang.String, java.lang.String)
     */
    @Override
    public Project parseProblem(String datasetName, String problemName) {
        final String fileName = path + datasetName + problemFormat + "/" + problemName + problemFormat;
        final File file = new File(fileName);
        Project project = new Project();

        project.setName(problemName);
        Scanner fileScanner = null;
        try {
            fileScanner = new Scanner(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PSPLibWrapper.class.getName()).log(Level.SEVERE, null, ex);
        }

        Scanner lineScanner;
        // Skips non useful lines
        for (int i = 0; i < 6; i++) {
            fileScanner.nextLine();
        }
        // Parse time horizon
        lineScanner = new Scanner(fileScanner.nextLine());
        lineScanner.next();
        lineScanner.next();
        project.setTimeHorizon(lineScanner.nextInt());
        fileScanner.nextLine();

        // Parse renewable resources
        lineScanner = new Scanner(fileScanner.nextLine());
        for (int i = 0; i < 3; i++) {
            lineScanner.next();
        }
        project.setNumRenewableResources(lineScanner.nextInt());

        // Parse Non renewable resources
        lineScanner = new Scanner(fileScanner.nextLine());
        for (int i = 0; i < 3; i++) {
            lineScanner.next();
        }
        project.setNumNonRenewableResources(lineScanner.nextInt());

        // Parse Doubly constrained resources
        lineScanner = new Scanner(fileScanner.nextLine());
        for (int i = 0; i < 4; i++) {
            lineScanner.next();
        }
        project.setNumDoublyConstrainedResources(lineScanner.nextInt());

        // Skips non useful lines
        for (int i = 0; i < 3; i++) {
            fileScanner.nextLine();
        }

        // Parse project information
        lineScanner = new Scanner(fileScanner.nextLine());
        project.setId(lineScanner.nextInt());
        project.setNumJobs(lineScanner.nextInt() + 2);
        project.setRelDate(lineScanner.nextInt());
        project.setDueDate(lineScanner.nextInt());
        project.setTardCost(lineScanner.nextInt());
        project.setMPMTime(lineScanner.nextInt());

        // Skips non useful lines
        for (int i = 0; i < 3; i++) {
            fileScanner.nextLine();
        }

        // Parse precedence relations
        List<Job> jobs = project.getJob();
        for (int jobID = 0; jobID < project.getNumJobs(); jobID++) {
            Job job = new Job();
            lineScanner = new Scanner(fileScanner.nextLine());
            job.setId(lineScanner.nextInt() - 1);// job number
            job.setNumModes(lineScanner.nextInt());
            job.setNumSuccessors(lineScanner.nextInt());
            List<Integer> successors = job.getSuccessor();
            for (int s = 0; s < job.getNumSuccessors(); s++) {
                successors.add(Integer.valueOf(lineScanner.nextInt() - 1));
            }

            jobs.add(job);
        }


        // Skips non useful lines
        for (int i = 0; i < 4; i++) {
            fileScanner.nextLine();
        }

        // Parse modes (duration and resource requests)
        int numResources = project.getNumRenewableResources()
                + project.getNumNonRenewableResources()
                + project.getNumDoublyConstrainedResources();
        int numModes;

        for (Job aJob : jobs) {
            numModes = aJob.getNumModes();
            List<Mode> modes = aJob.getMode();
            for (int m = 0; m < numModes; m++) {
                Mode mode = new Mode();
                mode.setId(m);
                lineScanner = new Scanner(fileScanner.nextLine());
                if (m == 0) {
                    lineScanner.nextInt(); // skips job number
                }
                lineScanner.nextInt(); // skips mode number
                mode.setDuration(lineScanner.nextInt());
                List<Integer> resourceRequests = mode.getResourceRequest();
                for (int k = 0; k < numResources; k++) {
                    resourceRequests.add(Integer.valueOf(lineScanner.nextInt()));
                }
                modes.add(mode);
            }
        }

        // Skips non useful lines
        for (int i = 0; i < 3; i++) {
            fileScanner.nextLine();
        }

        // Parse resources (availabilities)
        lineScanner = new Scanner(fileScanner.nextLine());
        List<Resource> resources = project.getResource();
        for (int k = 0; k < project.getNumRenewableResources(); k++) {
            Resource resource = new Resource();
            resource.setId(k);
            resource.setRenewable(true);
            resource.setName("R" + (k + 1));
            resource.setCapacity(lineScanner.nextInt());
            resources.add(resource);
        }
        for (int k = 0; k < project.getNumNonRenewableResources(); k++) {
            Resource resource = new Resource();
            resource.setId(k + project.getNumRenewableResources());
            resource.setRenewable(false);
            resource.setName("N" + (k + 1));
            resource.setCapacity(lineScanner.nextInt());
            resources.add(resource);
        }

        //		doubly constrained resources can be modeled with a combination of both renewable and non-renewable resources
        //		so we can ignore them 
        //		for (int k = 0; k < project.getNumDoublyConstrainedResources(); k++) {
        //		Resource resource = new Resource();
        //		resource.setId(k + project.getNumRenewableResources()
        //		+ project.getNumNonRenewableResources());
        //		resource.setType("D");
        //		resource.setName("D" + (k + 1));
        //		resource.setCapacity(lineScanner.nextInt());
        //		resources.add(resource);
        //		}

        lineScanner.close();
        fileScanner.close();

        //		System.out.println("Project successfully parsed from file: " + fileName);
        return project;
    }


    /* (non-Javadoc)
     * @see cbrcps.wrappers.DatasetWrapper#exportProject(rcpsp.Project, java.lang.String, java.lang.String)
     */
    @Override
    public void exportProject(Project project, String datasetName, String problemName) {
        // TODO Auto-generated method stub
    }
}
