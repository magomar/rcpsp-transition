/**
 *
 */
package rcpsp.data;

import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class ProjectDescription {

    private final String name;
    private final int numJobs;
    private final List<Integer>[] predecessors;
    private final List<Integer>[] successors;
    private final List<Mode>[] modes;
    private final Resource[] resource;
    private final int timeHorizon;
    private final int numRenewableResources;
    private final int numNonRenewableResources;

    /**
     * @param project
     */
    public ProjectDescription(Project project) {
        name = project.getName();
        numJobs = project.getNumJobs();
        timeHorizon = project.getTimeHorizon();
        predecessors = new ArrayList[numJobs];
        successors = new ArrayList[numJobs];
        modes = new ArrayList[numJobs];
        resource = new Resource[project.getResource().size()];
        project.getResource().toArray(resource);

//		Pre-processing
        for (Job job : project.getJob()) {
            int j = job.getId();
            predecessors[j] = new ArrayList<>();
            successors[j] = (ArrayList<Integer>) job.getSuccessor();
            modes[j] = (ArrayList<Mode>) job.getMode();
        }
        for (int j = 0; j < numJobs; j++) {
            for (Integer successor : successors[j]) {
                predecessors[successor.intValue()].add(j);
            }
        }
        int nRR = 0;
        int nNRR = 0;
        for (Resource res : project.getResource()) {
            if (res.isRenewable()) {
                nRR++;
            } else {
                nNRR++;
            }
        }
        numRenewableResources = nRR;
        numNonRenewableResources = nNRR;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public List<Mode>[] getModes() {
        return modes;
    }

    /**
     * @return
     */
    public int getNumJobs() {
        return numJobs;
    }

    /**
     * @return
     */
    public List<Integer>[] getPredecessors() {
        return predecessors;
    }

    /**
     * @return
     */
    public Resource[] getResource() {
        return resource;
    }

    /**
     * @return
     */
    public List<Integer>[] getSuccessors() {
        return successors;
    }

    /**
     * @return
     */
    public int getTimeHorizon() {
        return timeHorizon;
    }

    /**
     * Computes the earliest and latest start and finish times of jobs considering only precedence constrainsts (and
     * ignoring resource constraints), and assuming all jobs are executed using the mode with minimum duration. These
     * values are returned in an array of type {@link TimeBounds} (one element per job).
     *
     * @param pd
     * @return the time bounds
     */
    public static TimeBounds[] performCriticalPathAnalysis(ProjectDescription pd) {

        TimeBounds[] timeBounds = new TimeBounds[pd.numJobs];
        int[] minDuration = new int[pd.numJobs];

        List<Integer> jobPredecessors;
        // Forward pass (earliest start (ES) and earliest finish (EF) times)
        for (int j = 0; j < pd.numJobs; j++) {
            timeBounds[j] = new TimeBounds();
            minDuration[j] = pd.modes[j].get(0).getDuration();
            jobPredecessors = pd.predecessors[j];
            if (jobPredecessors.isEmpty()) {
                timeBounds[j].setEarliestStartTime(0);
                timeBounds[j].setEarliestFinishTime(minDuration[j]);
            } else {
                int maxPredecessorEF = 0;
                for (Integer predecessor : jobPredecessors) {
                    int predecessorEF = timeBounds[predecessor.intValue()].getEarliestFinishTime();
                    if (predecessorEF > maxPredecessorEF) {
                        maxPredecessorEF = predecessorEF;
                    }
                }
                timeBounds[j].setEarliestStartTime(maxPredecessorEF);
                timeBounds[j].setEarliestFinishTime(maxPredecessorEF + minDuration[j]);
            }
        }

        // Backward pass (latest start (LS) and latest finish (LF) times)
        for (int j = pd.numJobs - 1; j > -1; j--) {
            List<Integer> jobSuccessors = pd.successors[j];
            if (jobSuccessors.isEmpty()) {
                timeBounds[j].setLatestFinishTime(pd.timeHorizon);
                timeBounds[j].setLatestStartTime(pd.timeHorizon - minDuration[j]);
            } else {
                int minSuccessorLS = pd.timeHorizon;
                for (Integer successor : jobSuccessors) {
                    int successorLS = timeBounds[successor.intValue()].getLatestStartTime();
                    if (successorLS < minSuccessorLS) {
                        minSuccessorLS = successorLS;
                    }
                }
                timeBounds[j].setLatestFinishTime(minSuccessorLS);
                timeBounds[j].setLatestStartTime(minSuccessorLS - minDuration[j]);
            }
        }
        return timeBounds;
    }

    /**
     * Computes and returns the network complexity (NC) of a project. This value measures the average number of
     * non-redundant precedence relations per job, including dummy start and end jobs.
     *
     * @return the network complexity
     */
    public static double computeNetworkComplexity(ProjectDescription pd) {
        float nc = 0;
        for (int i = 0; i < pd.numJobs; i++) {
            nc += pd.successors[i].size();
        }
        nc /= pd.numJobs;
        return nc;
    }

    /**
     * Computes and returns the resource factor (RF) of a project for renewable resources. This value measures the
     * average portion of renewable resources used per job, considering all jobs but the dummy start and end jobs. It is
     * a real number in the interval [0, 1]
     *
     * @param pd
     * @return
     */
    public static double computeRenewableResourceFactor(ProjectDescription pd) {
        if (pd.numRenewableResources > 0) {
            int numJobs = pd.modes.length;
            double rf = 0;
            double jobRF;
            for (int j = 1; j < numJobs - 1; j++) {
                jobRF = 0;
                for (Mode mode : pd.modes[j]) {
                    for (int k = 0; k < pd.numRenewableResources; k++) {
                        if (mode.getResourceRequest().get(k) > 0) {
                            jobRF += 1;
                        }
                    }
                }
                jobRF /= (double) pd.modes[j].size() * pd.numRenewableResources;
                rf += jobRF;
            }
            rf /= numJobs - 2;
            return rf;
        } else {
            return 0;
        }
    }

    /**
     * Returns the resource strenght of a project for renewable resources. This value measures the scarcity of
     * resources. It is a real number in the interval [0, 1]
     *
     * @param pd
     * @return the resource strenght of renewable resources
     */
    public static double computeRenewableResourceStrenght(ProjectDescription pd) {
        if (pd.numRenewableResources > 0) {
            int numJobs = pd.modes.length;
            double rsMaxLB = 0;
            double rsMinUB = 1;
            double[] rsLB = new double[pd.numRenewableResources];
            double[] rsUB = new double[pd.numRenewableResources];
            int[] minDemand = new int[pd.numRenewableResources];
            int minJobDemand;
            int[][] maxJobDemand = new int[pd.numRenewableResources][numJobs];
            int[][] modeMaxJobDemand = new int[pd.numRenewableResources][numJobs];
            int[] maxDemand = new int[pd.numRenewableResources]; // peak period demand
            int maxPeriodDemand;
            int capacity;
            for (int k = 0; k < pd.numRenewableResources; k++) {
                minDemand[k] = 0;
                capacity = pd.resource[k].getCapacity();
                for (int j = 1; j < numJobs - 1; j++) {
                    minJobDemand = Integer.MAX_VALUE;
                    maxJobDemand[k][j] = 0;
                    for (Mode mode : pd.modes[j]) {
                        minJobDemand = Math.min(minJobDemand, mode.getResourceRequest().get(k));
                        maxJobDemand[k][j] = Math.max(maxJobDemand[k][j], mode.getResourceRequest().get(k));
                    }
                    for (Mode mode : pd.modes[j]) {
                        if (mode.getResourceRequest().get(k) == maxJobDemand[k][j]) {
                            modeMaxJobDemand[k][j] = mode.getId();
                            break;
                        }
                    }
                    minDemand[k] = Math.max(minDemand[k], minJobDemand);
                }

                int[] finishTime = new int[numJobs];
                int[] startTime = new int[numJobs];
                int[] duration = new int[numJobs];
                List<Integer> jobPredecessors;

                // Forward pass to compute earliest start and earliest finish  times
                for (int j = 0; j < numJobs; j++) {
                    duration[j] = pd.modes[j].get(modeMaxJobDemand[k][j]).getDuration();
                    jobPredecessors = pd.predecessors[j];
                    if (jobPredecessors.isEmpty()) {
                        startTime[j] = 0;
                        finishTime[j] = duration[j];
                    } else {
                        int maxPredecessorEF = 0;
                        for (Integer predecessor : jobPredecessors) {
                            int predecessorEF = finishTime[predecessor.intValue()];
                            if (predecessorEF > maxPredecessorEF) {
                                maxPredecessorEF = predecessorEF;
                            }
                        }
                        startTime[j] = maxPredecessorEF;
                        finishTime[j] = maxPredecessorEF + duration[j];
                    }
                }

                HashSet<Integer> candidateTimes = new HashSet<>(numJobs); // different start times
                for (int j = 0; j < numJobs; j++) {
                    candidateTimes.add(startTime[j]);
                }

                maxDemand[k] = minDemand[k];
                for (Integer integer : candidateTimes) {
                    int time = integer.intValue();
                    maxPeriodDemand = 0;
                    for (int j = 1; j < numJobs - 1; j++) {
                        if (startTime[j] + 1 <= time && time <= finishTime[j]) {
                            maxPeriodDemand += pd.modes[j].get(modeMaxJobDemand[k][j]).getResourceRequest().get(k);
                        }
                    }
                    maxDemand[k] = Math.max(maxDemand[k], maxPeriodDemand);
                }
                double denom = Math.max(1, maxDemand[k] - minDemand[k]);
                rsLB[k] = (double) (capacity - minDemand[k] - 0.5) / denom;
                rsMaxLB = Math.max(rsMaxLB, rsLB[k]);
                rsUB[k] = (double) (capacity - minDemand[k] + 0.5) / denom;
                rsMinUB = Math.min(rsMinUB, rsUB[k]);
            }
            return Math.min(rsMaxLB, rsMinUB);
        } else {
            return 0;
        }
    }

    /**
     * Returns the resource factor (RF) of a project for non renewable resources. This value measures the average
     * portion of non renewable resources used per job, considering all jobs but the dummy start and end jobs. It is a
     * real number in the interval [0, 1]
     *
     * @return the resource factor of non-renewable resources
     */
    public static double computeNonRenewableResourceFactor(ProjectDescription pd) {
        if (pd.numNonRenewableResources > 0) {
            int numJobs = pd.modes.length;
            double rf = 0;
            double jobRF;
            for (int j = 1; j < numJobs - 1; j++) {
                jobRF = 0;
                for (Mode mode : pd.modes[j]) {
                    for (int k = pd.numRenewableResources; k < pd.numRenewableResources + pd.numNonRenewableResources; k++) {
                        if (mode.getResourceRequest().get(k) > 0) {
                            jobRF += 1;
                        }
                    }
                }
                jobRF /= (double) pd.modes[j].size() * pd.numNonRenewableResources;
                rf += jobRF;
            }
            rf /= numJobs - 2;
            return rf;
        } else {
            return 0;
        }
    }

    /**
     * Returns the resource strength of a project for non-renewable resources. This value measures the scarcity of
     * resources. It is a real number in the interval [0, 1]
     *
     * @param pd
     * @return the resource strength of non-renewable resources
     */
    public static double computeNonRenewableResourceStrenght(ProjectDescription pd) {
        if (pd.numNonRenewableResources > 0) {
            int numJobs = pd.modes.length;
            double[] rs = new double[2];
            rs[0] = 0;
            rs[1] = 1;
            double[] rsLB = new double[pd.numNonRenewableResources];
            double[] rsUB = new double[pd.numNonRenewableResources];
            int[] minDemand = new int[pd.numNonRenewableResources];
            int minJobDemand;
            int[][] maxJobDemand = new int[pd.numNonRenewableResources][numJobs];
            int[][] modeMaxJobDemand = new int[pd.numNonRenewableResources][numJobs];
            int[] maxDemand = new int[pd.numNonRenewableResources]; // peak period demand
            int maxPeriodDemand;
            int capacity;
            int numResources = pd.numRenewableResources + pd.numNonRenewableResources;
            for (int k = pd.numRenewableResources; k < numResources; k++) {
                minDemand[k] = 0;
                capacity = pd.resource[k].getCapacity();
                for (int j = 1; j < numJobs - 1; j++) {
                    minJobDemand = Integer.MAX_VALUE;
                    maxJobDemand[k][j] = 0;
                    for (Mode mode : pd.modes[j]) {
                        minJobDemand = Math.min(minJobDemand, mode.getResourceRequest().get(k));
                        maxJobDemand[k][j] = Math.max(maxJobDemand[k][j], mode.getResourceRequest().get(k));
                    }
                    for (Mode mode : pd.modes[j]) {
                        if (mode.getResourceRequest().get(k) == maxJobDemand[k][j]) {
                            modeMaxJobDemand[k][j] = mode.getId();
                            break;
                        }
                    }
                    minDemand[k] = Math.max(minDemand[k], minJobDemand);
                }

                int[] finishTime = new int[numJobs];
                int[] startTime = new int[numJobs];
                int[] duration = new int[numJobs];
                List<Integer> jobPredecessors;

                // Forward pass to compute earliest start and earliest finish  times
                for (int j = 0; j < numJobs; j++) {
                    duration[j] = pd.modes[j].get(modeMaxJobDemand[k][j]).getDuration();
                    jobPredecessors = pd.predecessors[j];
                    if (jobPredecessors.isEmpty()) {
                        startTime[j] = 0;
                        finishTime[j] = duration[j];
                    } else {
                        int maxPredecessorEF = 0;
                        for (Integer predecessor : jobPredecessors) {
                            int predecessorEF = finishTime[predecessor.intValue()];
                            if (predecessorEF > maxPredecessorEF) {
                                maxPredecessorEF = predecessorEF;
                            }
                        }
                        startTime[j] = maxPredecessorEF;
                        finishTime[j] = maxPredecessorEF + duration[j];
                    }
                }

                HashSet<Integer> candidateTimes = new HashSet<>(numJobs); // different start times
                for (int j = 0; j < numJobs; j++) {
                    candidateTimes.add(startTime[j]);
                }

                maxDemand[k] = minDemand[k];
                for (Integer integer : candidateTimes) {
                    int time = integer.intValue();
                    maxPeriodDemand = 0;
                    for (int j = 1; j < numJobs - 1; j++) {
                        if (startTime[j] + 1 <= time && time <= finishTime[j]) {
                            maxPeriodDemand += pd.modes[j].get(modeMaxJobDemand[k][j]).getResourceRequest().get(k);
                        }
                    }
                    maxDemand[k] = Math.max(maxDemand[k], maxPeriodDemand);
                }
                double denom = Math.max(1, maxDemand[k] - minDemand[k]);
                rsLB[k] = (double) (capacity - minDemand[k] - 0.5) / denom;
                rs[0] = Math.max(rs[0], rsLB[k]);
                rsUB[k] = (double) (capacity - minDemand[k] + 0.5) / denom;
                rs[1] = Math.min(rs[1], rsUB[k]);
            }
            return Math.min(rs[0], rs[1]);
        } else {
            return 0;
        }
    }
}
