
package rcpsp.data.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProjectSchedule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProjectSchedule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="algorithm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="makespan" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="JobSchedule" type="{rcpsp}JobSchedule" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProjectSchedule", namespace = "rcpsp", propOrder = {
    "algorithm",
    "makespan",
    "jobSchedule"
})
public class ProjectSchedule {

    @XmlElement(namespace = "rcpsp", required = true)
    protected String algorithm;
    @XmlElement(namespace = "rcpsp")
    protected int makespan;
    @XmlElement(name = "JobSchedule", namespace = "rcpsp", required = true)
    protected List<JobSchedule> jobSchedule;

    /**
     * Gets the value of the algorithm property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     * Sets the value of the algorithm property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlgorithm(String value) {
        this.algorithm = value;
    }

    /**
     * Gets the value of the makespan property.
     * 
     */
    public int getMakespan() {
        return makespan;
    }

    /**
     * Sets the value of the makespan property.
     * 
     */
    public void setMakespan(int value) {
        this.makespan = value;
    }

    /**
     * Gets the value of the jobSchedule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the jobSchedule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJobSchedule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JobSchedule }
     * 
     * 
     */
    public List<JobSchedule> getJobSchedule() {
        if (jobSchedule == null) {
            jobSchedule = new ArrayList<JobSchedule>();
        }
        return this.jobSchedule;
    }

}
