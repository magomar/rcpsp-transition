
package rcpsp.data.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Dataset" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Type" type="{rcpsp}BenchmarkType"/>
 *         &lt;element name="BenchmarkSolution" type="{rcpsp}BenchmarkSolution" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="author" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataset",
    "type",
    "benchmarkSolution"
})
@XmlRootElement(name = "BenchmarkResults", namespace = "rcpsp")
public class BenchmarkResults {

    @XmlElement(name = "Dataset", namespace = "rcpsp", required = true)
    protected String dataset;
    @XmlElement(name = "Type", namespace = "rcpsp", required = true)
    protected BenchmarkType type;
    @XmlElement(name = "BenchmarkSolution", namespace = "rcpsp", required = true)
    protected List<BenchmarkSolution> benchmarkSolution;
    @XmlAttribute(name = "author")
    protected String author;

    /**
     * Gets the value of the dataset property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataset() {
        return dataset;
    }

    /**
     * Sets the value of the dataset property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataset(String value) {
        this.dataset = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link BenchmarkType }
     *     
     */
    public BenchmarkType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link BenchmarkType }
     *     
     */
    public void setType(BenchmarkType value) {
        this.type = value;
    }

    /**
     * Gets the value of the benchmarkSolution property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the benchmarkSolution property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBenchmarkSolution().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BenchmarkSolution }
     * 
     * 
     */
    public List<BenchmarkSolution> getBenchmarkSolution() {
        if (benchmarkSolution == null) {
            benchmarkSolution = new ArrayList<BenchmarkSolution>();
        }
        return this.benchmarkSolution;
    }

    /**
     * Gets the value of the author property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthor() {
        return author;
    }

    /**
     * Sets the value of the author property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthor(String value) {
        this.author = value;
    }

}
