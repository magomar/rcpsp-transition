
package rcpsp.data.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Project complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Project">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Job" type="{rcpsp}Job" maxOccurs="unbounded"/>
 *         &lt;element name="Resource" type="{rcpsp}Resource" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *       &lt;attribute name="id" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="numJobs" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="numRenewableResources" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="numNonRenewableResources" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="numDoublyConstrainedResources" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="timeHorizon" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="relDate" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="dueDate" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="tardCost" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="MPMTime" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Project", namespace = "rcpsp", propOrder = {
    "job",
    "resource"
})
public class Project {

    @XmlElement(name = "Job", namespace = "rcpsp", required = true)
    protected List<Job> job;
    @XmlElement(name = "Resource", namespace = "rcpsp", required = true)
    protected List<Resource> resource;
    @XmlAttribute(name = "id", required = true)
    protected int id;
    @XmlAttribute(name = "name")
    protected String name;
    @XmlAttribute(name = "numJobs")
    protected Integer numJobs;
    @XmlAttribute(name = "numRenewableResources")
    protected Integer numRenewableResources;
    @XmlAttribute(name = "numNonRenewableResources")
    protected Integer numNonRenewableResources;
    @XmlAttribute(name = "numDoublyConstrainedResources")
    protected Integer numDoublyConstrainedResources;
    @XmlAttribute(name = "timeHorizon")
    protected Integer timeHorizon;
    @XmlAttribute(name = "relDate")
    protected Integer relDate;
    @XmlAttribute(name = "dueDate")
    protected Integer dueDate;
    @XmlAttribute(name = "tardCost")
    protected Integer tardCost;
    @XmlAttribute(name = "MPMTime")
    protected Integer mpmTime;

    /**
     * Gets the value of the job property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the job property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getJob().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Job }
     * 
     * 
     */
    public List<Job> getJob() {
        if (job == null) {
            job = new ArrayList<Job>();
        }
        return this.job;
    }

    /**
     * Gets the value of the resource property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resource property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResource().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Resource }
     * 
     * 
     */
    public List<Resource> getResource() {
        if (resource == null) {
            resource = new ArrayList<Resource>();
        }
        return this.resource;
    }

    /**
     * Gets the value of the id property.
     * 
     */
    public int getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setId(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the numJobs property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumJobs() {
        return numJobs;
    }

    /**
     * Sets the value of the numJobs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumJobs(Integer value) {
        this.numJobs = value;
    }

    /**
     * Gets the value of the numRenewableResources property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumRenewableResources() {
        return numRenewableResources;
    }

    /**
     * Sets the value of the numRenewableResources property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumRenewableResources(Integer value) {
        this.numRenewableResources = value;
    }

    /**
     * Gets the value of the numNonRenewableResources property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumNonRenewableResources() {
        return numNonRenewableResources;
    }

    /**
     * Sets the value of the numNonRenewableResources property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumNonRenewableResources(Integer value) {
        this.numNonRenewableResources = value;
    }

    /**
     * Gets the value of the numDoublyConstrainedResources property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumDoublyConstrainedResources() {
        return numDoublyConstrainedResources;
    }

    /**
     * Sets the value of the numDoublyConstrainedResources property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumDoublyConstrainedResources(Integer value) {
        this.numDoublyConstrainedResources = value;
    }

    /**
     * Gets the value of the timeHorizon property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTimeHorizon() {
        return timeHorizon;
    }

    /**
     * Sets the value of the timeHorizon property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTimeHorizon(Integer value) {
        this.timeHorizon = value;
    }

    /**
     * Gets the value of the relDate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRelDate() {
        return relDate;
    }

    /**
     * Sets the value of the relDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRelDate(Integer value) {
        this.relDate = value;
    }

    /**
     * Gets the value of the dueDate property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDueDate() {
        return dueDate;
    }

    /**
     * Sets the value of the dueDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDueDate(Integer value) {
        this.dueDate = value;
    }

    /**
     * Gets the value of the tardCost property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTardCost() {
        return tardCost;
    }

    /**
     * Sets the value of the tardCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTardCost(Integer value) {
        this.tardCost = value;
    }

    /**
     * Gets the value of the mpmTime property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMPMTime() {
        return mpmTime;
    }

    /**
     * Sets the value of the mpmTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMPMTime(Integer value) {
        this.mpmTime = value;
    }

}
