
package rcpsp.data.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenchmarkSolution complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BenchmarkSolution">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Problem" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LowerBound" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UpperBound" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="OptimalSolution" type="{rcpsp}ProjectSchedule"/>
 *         &lt;element name="HeuristicSolution" type="{rcpsp}ProjectSchedule"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BenchmarkSolution", namespace = "rcpsp", propOrder = {
    "problem",
    "lowerBound",
    "upperBound",
    "optimalSolution",
    "heuristicSolution"
})
public class BenchmarkSolution {

    @XmlElement(name = "Problem", namespace = "rcpsp", required = true)
    protected String problem;
    @XmlElement(name = "LowerBound", namespace = "rcpsp")
    protected int lowerBound;
    @XmlElement(name = "UpperBound", namespace = "rcpsp")
    protected int upperBound;
    @XmlElement(name = "OptimalSolution", namespace = "rcpsp", required = true)
    protected ProjectSchedule optimalSolution;
    @XmlElement(name = "HeuristicSolution", namespace = "rcpsp", required = true)
    protected ProjectSchedule heuristicSolution;

    /**
     * Gets the value of the problem property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProblem() {
        return problem;
    }

    /**
     * Sets the value of the problem property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProblem(String value) {
        this.problem = value;
    }

    /**
     * Gets the value of the lowerBound property.
     * 
     */
    public int getLowerBound() {
        return lowerBound;
    }

    /**
     * Sets the value of the lowerBound property.
     * 
     */
    public void setLowerBound(int value) {
        this.lowerBound = value;
    }

    /**
     * Gets the value of the upperBound property.
     * 
     */
    public int getUpperBound() {
        return upperBound;
    }

    /**
     * Sets the value of the upperBound property.
     * 
     */
    public void setUpperBound(int value) {
        this.upperBound = value;
    }

    /**
     * Gets the value of the optimalSolution property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectSchedule }
     *     
     */
    public ProjectSchedule getOptimalSolution() {
        return optimalSolution;
    }

    /**
     * Sets the value of the optimalSolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectSchedule }
     *     
     */
    public void setOptimalSolution(ProjectSchedule value) {
        this.optimalSolution = value;
    }

    /**
     * Gets the value of the heuristicSolution property.
     * 
     * @return
     *     possible object is
     *     {@link ProjectSchedule }
     *     
     */
    public ProjectSchedule getHeuristicSolution() {
        return heuristicSolution;
    }

    /**
     * Sets the value of the heuristicSolution property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProjectSchedule }
     *     
     */
    public void setHeuristicSolution(ProjectSchedule value) {
        this.heuristicSolution = value;
    }

}
