
package rcpsp.data.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenchmarkType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BenchmarkType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OPT"/>
 *     &lt;enumeration value="HRS"/>
 *     &lt;enumeration value="LB"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BenchmarkType", namespace = "rcpsp")
@XmlEnum
public enum BenchmarkType {

    OPT,
    HRS,
    LB;

    public String value() {
        return name();
    }

    public static BenchmarkType fromValue(String v) {
        return valueOf(v);
    }

}
