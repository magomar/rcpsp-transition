package rcpsp.data;

/**
 * @author Mario Gomez
 *
 */
public final class TimeBounds {
	private int earliestStartTime;
	private int earliestFinishTime;
	private int latestStartTime;
	private int latestFinishTime;

	/**
	 * 
	 */
	public TimeBounds() {
		earliestStartTime = -1;
		earliestFinishTime = -1;
		latestStartTime = -1;
		latestFinishTime = -1;
	}

	/**
	 * @return
	 */
	public int getEarliestFinishTime() {
		return earliestFinishTime;
	}

	/**
	 * @param earliestFinishTime
	 */
	public void setEarliestFinishTime(int earliestFinishTime) {
		this.earliestFinishTime = earliestFinishTime;
	}

	/**
	 * @return
	 */
	public int getEarliestStartTime() {
		return earliestStartTime;
	}

	/**
	 * @param earliestStartTime
	 */
	public void setEarliestStartTime(int earliestStartTime) {
		this.earliestStartTime = earliestStartTime;
	}

	/**
	 * @return
	 */
	public int getLatestFinishTime() {
		return latestFinishTime;
	}

	/**
	 * @param latestFinishTime
	 */
	public void setLatestFinishTime(int latestFinishTime) {
		this.latestFinishTime = latestFinishTime;
	}

	/**
	 * @return
	 */
	public int getLatestStartTime() {
		return latestStartTime;
	}

	/**
	 * @param latestStartTime
	 */
	public void setLatestStartTime(int latestStartTime) {
		this.latestStartTime = latestStartTime;
	}

}
