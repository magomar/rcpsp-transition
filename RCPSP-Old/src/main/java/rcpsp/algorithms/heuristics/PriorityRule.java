package rcpsp.algorithms.heuristics;

import java.util.Comparator;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public interface PriorityRule extends Comparator<Integer>  {

	/**
	 * This method returns an array of priority values for a list of jobs, 
	 * where the lowest priority value is the highest scheduling priority
	 * @param integerList
	 * @return the priority values
	 */
	public double[] getPriorityValues(List<Integer> integerList);
	
	/**
	 * Defensive copy of the object, equivalent to clone()
	 * @return a new instance of the scheduler
	 */
	public PriorityRule newInstance();
	
}
