/**
 * 
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.algorithms.cbr.CBRCPS;
import rcpsp.algorithms.matcher.ProjectMatcher;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 */
public interface CaseBasedPriorityRule {

	/**
	 * @param project
	 * @param jobPriorityRule
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public void build(Project project, JobPriorityRule jobPriorityRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher);
}
