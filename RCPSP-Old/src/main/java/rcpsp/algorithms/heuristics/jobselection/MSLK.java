/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.List;


/**
 * @author Mario Gomez
 *
 */
public final class MSLK extends AbstractJobPriorityRule {
	private int[] mslk;


	@Override
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
		super.setData(timeBounds, successors, modes, resource);
		mslk = new int[successors.length];
		for (int j = 0; j < successors.length; j++) {
			mslk[j] = timeBounds[j].getLatestStartTime() - timeBounds[j].getEarliestStartTime();
		}
	}


	@Override
	public int compare(Integer o1, Integer o2) {
		int i1 = o1.intValue();
		int i2 = o2.intValue();
		return  mslk[i1] - mslk[i2];
	}


	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			int j = jobList.get(i).intValue();
			priorityValue[i] = mslk[j];
		}
		return priorityValue;
	}
}
