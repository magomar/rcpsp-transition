/**
 * 
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.List;


/**
 * @author Mario Gomez
 *
 */
public final class GRPW extends AbstractJobPriorityRule {
	private int[] rpw;


	@Override
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
		super.setData(timeBounds, successors, modes, resource);
		rpw = new int[successors.length];
		for (int j = 0; j < successors.length; j++) {
			rpw[j] = modes[j].get(0).getDuration();
			for (Integer successor : successors[j]) {
				rpw[j] += modes[successor.intValue()].get(0).getDuration();
			}
		}
	}
	

	@Override
	public int compare(Integer o1, Integer o2) {
		int i1 = o1.intValue();
		int i2 = o2.intValue();
		return rpw[i2] - rpw[i1];
	}
	
	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			int j = jobList.get(i).intValue();
			priorityValue[i] =rpw[jobList.get(0).intValue()] - rpw[j];  // assuming jobList is sorted
		}
		return priorityValue;
	}

}
