/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import java.util.List;

/**
 * @author Mario Gomez This class implements the Longest Processing Time (LPT) Job Priority Rule
 * heuristic This heuristic gives greater priority the job with the longest processing time
 */
public final class LPT extends AbstractJobPriorityRule {

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        return modes[i2].get(0).getDuration() - modes[i1].get(0).getDuration();
    }

    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i).intValue();
            priorityValue[i] = modes[jobList.get(0)].get(0).getDuration() - modes[j].get(0).getDuration(); // assuming jobList is sorted
        }
        return priorityValue;
    }
}
