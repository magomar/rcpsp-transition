/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class TRD extends AbstractJobPriorityRule {

    private List<Integer>[] trd;

    @Override
    public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
        super.setData(timeBounds, successors, modes, resource);
        trd = new List[modes.length]; // length = num jobs
        for (int j = 0; j < modes.length; j++) {
            trd[j] = new ArrayList<>();
            for (int m = 0; m < modes[j].size(); m++) {
                int sumResourceUsage = 0;
                for (Integer request : modes[j].get(m).getResourceRequest()) {
                    sumResourceUsage += request.intValue();
                }
                trd[j].add(sumResourceUsage);
            }
        }
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        return trd[i1].get(0).intValue() - trd[i2].get(0).intValue();
    }

    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i).intValue();
            priorityValue[i] = trd[j].get(0).intValue();
        }
        return priorityValue;
    }
}
