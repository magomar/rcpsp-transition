/**
 * 
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.algorithms.heuristics.PriorityRule;
import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public interface JobPriorityRule extends PriorityRule {
	
	/**
	 * @param timeBounds
	 * @param successors
	 * @param modes
	 * @param resource
	 */
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource);
	
	
}
