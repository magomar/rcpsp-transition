/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class NIS extends AbstractJobPriorityRule {

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        return successors[i2].size() - successors[i1].size();
    }

    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i).intValue();
            priorityValue[i] = successors[jobList.get(0).intValue()].size() - successors[j].size();
        }
        return priorityValue;
    }
}
