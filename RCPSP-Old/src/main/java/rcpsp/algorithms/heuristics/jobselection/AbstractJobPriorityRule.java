/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.algorithms.heuristics.PriorityRule;
import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractJobPriorityRule implements JobPriorityRule {

    TimeBounds[] timeBounds;
    List<Integer>[] successors;
    List<Mode>[] modes;
    Resource[] resource;

    /**
     * @param timeBounds
     * @param successors
     * @param modes
     * @param resource
     */
    @Override
    public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
        this.timeBounds = timeBounds;
        this.successors = successors;
        this.modes = modes;
        this.resource = resource;
    }

    /*
     * (non-Javadoc) @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }

    /*
     * (non-Javadoc) @see rcpsp.PriorityRule#newInstance()
     */
    @Override
    public PriorityRule newInstance() {
        PriorityRule newInstance = null;
        try {
            newInstance = this.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
        }
        return newInstance;
    }
}
