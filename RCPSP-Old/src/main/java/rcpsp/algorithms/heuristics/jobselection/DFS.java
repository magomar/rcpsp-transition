/**
 * 
 */
package rcpsp.algorithms.heuristics.jobselection;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class DFS extends AbstractJobPriorityRule {


	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[] priorityValue = new double[jobList.size()];
		for (int i = 0; i < priorityValue.length; i++) {
			priorityValue[i] = i;
		}
		return priorityValue;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Integer o1, Integer o2) {
		return 0;
	}

}
