package rcpsp.algorithms.heuristics.jobselection;

import rcpsp.algorithms.heuristics.PriorityRule;
import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class CompositePriorityRule extends AbstractJobPriorityRule {
	final JobPriorityRule[] priorityRule;

	/**
	 * @param listOfRules
	 */
	public CompositePriorityRule(List<JobPriorityRule> listOfRules) {
		priorityRule = new JobPriorityRule[listOfRules.size()];
		listOfRules.toArray(this.priorityRule);
	}

	/* (non-Javadoc)
	 * @see rcpsp.JobPriorityRule#setData(rcpsp.data.TimeBounds[], java.util.List<java.lang.Integer>[], java.util.List<Mode>[], rcpsp.Resource[])
	 */
	@Override
	public void setData(TimeBounds[] timeBounds, List<Integer>[] successors, List<Mode>[] modes, Resource[] resource) {
		for (int i = 0; i < priorityRule.length; i++) {
			priorityRule[i].setData(timeBounds, successors, modes, resource);
		}
	}



	@Override
	public double[] getPriorityValues(List<Integer> jobList) {
		double[][] singlePriority = new double[priorityRule.length][jobList.size()];
		for (int i = 0; i < priorityRule.length; i++) {
			singlePriority[i] = priorityRule[i].getPriorityValues(jobList);
		}
		double[] compositePriority = new double[jobList.size()];
		for (int j = 0; j < jobList.size(); j++) {
			compositePriority[j] = 0;
			for (int i = 0; i < priorityRule.length; i++) {
				compositePriority[j] += singlePriority[i][j];
			}

		}
		return compositePriority;
	}


	@Override
	public int compare(Integer o1, Integer o2) {
		List<Integer> integerList = new ArrayList<Integer>();
		integerList.add(o1);
		integerList.add(o2);
		double[] pv = getPriorityValues(integerList);
		double diff = pv[0] - pv[1];
		//		return ( diff != 0 ? (int) diff : o1.intValue() - o2.intValue());
		return  (int) diff;
	}


	@Override
	public String toString() {
		return Arrays.asList(priorityRule).toString();
	}


	@Override
	public PriorityRule newInstance() {
		List<JobPriorityRule> newPriorityRule = new ArrayList<JobPriorityRule>();
		for (int i = 0; i < priorityRule.length; i++) {
			newPriorityRule.add((JobPriorityRule) priorityRule[i].newInstance());
		}
		return new CompositePriorityRule(newPriorityRule);
	}

}
