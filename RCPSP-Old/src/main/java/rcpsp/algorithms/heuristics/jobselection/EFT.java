/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class EFT extends AbstractJobPriorityRule {

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        return timeBounds[i1].getEarliestFinishTime() - timeBounds[i2].getEarliestFinishTime();
    }

    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i).intValue();
            priorityValue[i] = timeBounds[j].getEarliestFinishTime();
        }
        return priorityValue;
    }
}
