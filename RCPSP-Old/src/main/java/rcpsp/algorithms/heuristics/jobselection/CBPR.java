/**
 *
 */
package rcpsp.algorithms.heuristics.jobselection;

import jcolibri.cbrcore.CBRCase;
import jcolibri.cbrcore.CBRQuery;
import jcolibri.exception.ExecutionException;
import jcolibri.method.retrieve.RetrievalResult;
import rcpsp.algorithms.cbr.CBRCPS;
import rcpsp.algorithms.cbr.CaseDescription;
import rcpsp.algorithms.cbr.CaseSolution;
import rcpsp.algorithms.cbr.HybridCBPRComparator;
import rcpsp.algorithms.matcher.ProjectMatcher;
import rcpsp.data.ProjectDescription;
import rcpsp.data.ProjectMatching;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Project;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 */
public final class CBPR extends AbstractJobPriorityRule implements CaseBasedPriorityRule {

    int[] jobRanking;
    String secondRule;

    /**
     * @param jobPriorityRule
     */
    public CBPR(JobPriorityRule jobPriorityRule) {
        secondRule = jobPriorityRule.toString();
    }

    /* (non-Javadoc)
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare(Integer o1, Integer o2) {
        return (jobRanking[o1] - jobRanking[o2]);
    }

    /* (non-Javadoc)
     * @see rcpsp.algorithms.heuristics.JobComparatorInterface#getProbabilityValues()
     */
    @Override
    public double[] getPriorityValues(List<Integer> jobList) {
        double[] priorityValue = new double[jobList.size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int j = jobList.get(i);
            priorityValue[i] = jobRanking[j];
        }
        return priorityValue;
    }

    /* (non-Javadoc)
     * @see rcpsp.CaseBasedPriorityRule#build(Project, rcpsp.JobPriorityRule, cbrcps.CBRCPS, cbrcps.wrappers.DatasetWrapper, ProjectMatcher)
     */
    @Override
    public void build(Project project, JobPriorityRule jobPriorityRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher) {
        secondRule = jobPriorityRule.toString();

        //		RETRIEVE CASES
        //		Compute network parameters for the problem being solved
        ProjectDescription pd = new ProjectDescription(project);
        double nc = ProjectDescription.computeNetworkComplexity(pd);
        double rrf = ProjectDescription.computeRenewableResourceFactor(pd);
        double nrf = ProjectDescription.computeNonRenewableResourceFactor(pd);
        double rrs = ProjectDescription.computeRenewableResourceStrenght(pd);
        double nrs = ProjectDescription.computeNonRenewableResourceStrenght(pd);

        //		Create a new query to retrieve similar problems solved in the past
        CBRQuery query = new CBRQuery();
        CaseDescription caseDescription = new CaseDescription();
        caseDescription.setNumJobs(project.getNumJobs());
        caseDescription.setNumRenewableResources(project.getNumRenewableResources());
        caseDescription.setNumNonRenewableResources(project.getNumNonRenewableResources());
        caseDescription.setNetworkComplexity(nc);
        caseDescription.setRenewableResourceFactor(rrf);
        caseDescription.setRenewableResourceStrenght(rrs);
        caseDescription.setNonRenewableResourceFactor(nrf);
        caseDescription.setNonRenewableResourceStrenght(nrs);
        query.setDescription(caseDescription);
        try {
            // Execute the query and get the retrieved cases

                cbrcps.cycle(query);
        } catch (ExecutionException ex) {
            Logger.getLogger(CBPR.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        ArrayList<RetrievalResult> retrievedCases = cbrcps.getRetrievedCases();

        // Print query and retrieved cases
//		System.out.println("Retrieved cases for query: " + query);
//		for (RetrievalResult retrievalResult : retrievedCases) {
//		System.out.println(retrievalResult);
//		}

        // Compare project against retrieved projects using graph similarity to decide which one is the most similar
        // and obtain a mapping of jobs between each other 
        ProjectMatching bestMatching = null;
        CBRCase bestCase = null;
        for (RetrievalResult result : retrievedCases) {
            CBRCase retrievedCase = result.get_case();
            CaseDescription retrievedCaseDescription = (CaseDescription) retrievedCase.getDescription();
            Project pastProject = cbWrapper.parseProblem(retrievedCaseDescription.getDataset(),
                    retrievedCaseDescription.getCaseId());
            ProjectMatching matching = matcher.matchProjects(pastProject, project);
            if (bestMatching == null || matching.getSimilarity() > bestMatching.getSimilarity()) {
                bestCase = retrievedCase;
                bestMatching = matching;
            }
        }

        CaseDescription bestCaseDescription = (CaseDescription) bestCase.getDescription();
        CaseSolution bestCaseSolution = (CaseSolution) bestCase.getSolution();
        System.out.println("Most similar case: " + bestCaseDescription.getCaseId());
        System.out.println("Similarity: " + bestMatching.getSimilarity());
        System.out.println("Mapping: " + bestMatching.getMap());

        // Adapt solution from the best retrieved case to solve current problem

        // First, read the activity list from the best case retrieved
        StringTokenizer st = new StringTokenizer(bestCaseSolution.getActivityList());
        ArrayList<Integer> activityList = new ArrayList<>();
        while (st.hasMoreTokens()) {
            activityList.add(Integer.valueOf(st.nextToken()));
        }
        System.out.println("Retrieved solution: " + activityList);

        // Create a new activity list for the problem being solved
        List<Integer> partialActivityList = new ArrayList<>();
        for (Integer caseJob : activityList) {
            if (bestMatching.getMap().containsKey(caseJob)) {
                partialActivityList.add(bestMatching.getMap().get(caseJob));
            }
        }

        System.out.println("Adapted solution: " + partialActivityList);
        System.out.println();

        // Finally, set the job rankings, to be used by the compare and getPriorityValues methods
        int numJobs = project.getNumJobs();
        jobRanking = new int[numJobs];
        for (int j = 0; j < jobRanking.length; j++) {
            if (partialActivityList.contains(j)) {
                jobRanking[j] = partialActivityList.indexOf(j);
            } else {
                jobRanking[j] = -1;
            }
        }
        List<Integer> rank = new ArrayList<>();
        for (int r : jobRanking) {
            rank.add(r);
        }
//		System.out.println("Partial job rankings: " + rank);
        Comparator hybridCBRComparator = new HybridCBPRComparator(jobRanking, jobPriorityRule);
        jobPriorityRule.setData(timeBounds, successors, modes, resource);
        List<Job> newActivityList = (List<Job>) ((ArrayList<Job>) project.getJob()).clone();
        Collections.sort(newActivityList, hybridCBRComparator);
        int index = 0;
        for (Job job : newActivityList) {
            jobRanking[job.getId()] = index;
            index++;
        }
        rank = new ArrayList<>();
        for (int r : jobRanking) {
            rank.add(r);
        }
//		System.out.println("Final job rankings" + rank);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ":" + secondRule;
    }
}
