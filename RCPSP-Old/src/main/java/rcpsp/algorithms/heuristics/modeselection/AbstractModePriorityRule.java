/**
 *
 */
package rcpsp.algorithms.heuristics.modeselection;

import rcpsp.algorithms.heuristics.PriorityRule;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Resource;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public abstract class AbstractModePriorityRule implements ModePriorityRule {

    List<Mode>[] modes;
    Resource[] resource;
    int selectedJob;

    @Override
    public void setData(List<Mode>[] modes, Resource[] resource) {
        this.modes = modes;
        this.resource = resource;
    }

    @Override
    public void setSelectedJob(int selectedJob) {
        this.selectedJob = selectedJob;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    /*
     * (non-Javadoc) @see rcpsp.PriorityRule#newInstance()
     */
    @Override
    public PriorityRule newInstance() {
        PriorityRule newInstance = null;
        try {
            newInstance = this.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
        }
        return newInstance;
    }
}
