/**
 *
 */
package rcpsp.algorithms.heuristics.modeselection;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class SFM extends AbstractModePriorityRule {

    @Override
    public int compare(Integer o1, Integer o2) {
        int i1 = o1.intValue();
        int i2 = o2.intValue();
        int compare = modes[selectedJob].get(i1).getDuration() - modes[selectedJob].get(i2).getDuration();
//		return (compare != 0 ? 
//				compare : 
//					i1 - i2);
        return compare;
    }

    @Override
    public double[] getPriorityValues(List<Integer> modeList) {
        double[] priorityValue = new double[modes[selectedJob].size()];
        for (int i = 0; i < priorityValue.length; i++) {
            int m = modeList.get(i).intValue();
            priorityValue[i] = modes[selectedJob].get(m).getDuration();  // assuming jobList is sorted
        }
        return priorityValue;
    }
}
