/**
 * 
 */
package rcpsp.algorithms.heuristics;

import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.jobselection.MultiPriorityRule;
import rcpsp.util.CombinationsGenerator;
import rcpsp.util.PermutationsGenerator;

/**
 * @author Mario Gomez
 *
 */
public final class MultiRuleGenerator {

	/**
	 * @param jobPriorityRule
	 * @param ruleWeight
	 * @param useWeightPermutations
	 * @return
	 */
	public static JobPriorityRule[] getMultiRules(JobPriorityRule[] jobPriorityRule, double[] ruleWeight, boolean useWeightPermutations) {
		PermutationsGenerator permutator = new PermutationsGenerator(ruleWeight.length);
		double[][] weightPermutations;
		double[] weightPermutation;
		int[] elementIndex;
		int groupIndex = 0;
		if (useWeightPermutations) { // Generate permutations of the weights
			weightPermutations = new double[permutator.getTotal().intValue()][ruleWeight.length];
			while (permutator.hasMore()) {
				weightPermutation = new double[ruleWeight.length]; 
				elementIndex = permutator.getNext();
				for (int i = 0; i < elementIndex.length; i++) {
					weightPermutation[i] = ruleWeight[elementIndex[i]];
				}
				weightPermutations[groupIndex++] = weightPermutation;
			}
		} else { // no permutations, use weights as provided in ruleWeight
			weightPermutations = new double[1][ruleWeight.length];
		}
		// Generate combinations of the priority rules, 
		// and for each combination generate one multi-rules per weight permutation
		CombinationsGenerator ruleCombinator = new CombinationsGenerator(jobPriorityRule.length, ruleWeight.length);
		MultiPriorityRule[] multiRule = new MultiPriorityRule[ruleCombinator.getTotal().intValue() * weightPermutations.length];
		JobPriorityRule[] rulesToCombine;
		groupIndex = 0;
		while (ruleCombinator.hasMore ()) {
			rulesToCombine = new JobPriorityRule[ruleWeight.length];
			elementIndex = ruleCombinator.getNext ();
			for (int i = 0; i < elementIndex.length; i++) {
				rulesToCombine[i] = jobPriorityRule[elementIndex[i]];
			}
			// for each combination, generate all permutations
			for (int i = 0; i < weightPermutations.length; i++) {
				multiRule[groupIndex++] = new MultiPriorityRule(rulesToCombine, weightPermutations[i]);
			}
		}
		return multiRule;
	}
	
	/**
	 * @param jobPriorityRule
	 * @param ruleWeight
	 * @param useWeightPermutations
	 * @param multiRuleListSize
	 * @return
	 */
	public static JobPriorityRule[][] getMultiRuleLists(JobPriorityRule[] jobPriorityRule, double[] ruleWeight, boolean useWeightPermutations, int multiRuleListSize) {
		JobPriorityRule[] multiRule = getMultiRules(jobPriorityRule, ruleWeight, useWeightPermutations);
		 // Now generate combinations of the generated multirules to feed the multiheuristic algorithm
		 // with different sets of rules
		 CombinationsGenerator multiRuleCombinator = new CombinationsGenerator(multiRule.length, multiRuleListSize);;
		 JobPriorityRule[][] multiRuleCombination = new JobPriorityRule[multiRuleCombinator.getTotal().intValue()][multiRuleListSize];
		 int[] elementIndex;
		 int groupIndex = 0;
		 JobPriorityRule[] rulesToCombine;
		 while (multiRuleCombinator.hasMore ()) {
			 rulesToCombine = new JobPriorityRule[multiRuleListSize];
			 elementIndex = multiRuleCombinator.getNext ();
			 for (int i = 0; i < elementIndex.length; i++) {
				 rulesToCombine[i] = jobPriorityRule[elementIndex[i]];
			 }
			 multiRuleCombination[groupIndex++] = rulesToCombine;
		 }	 
		return multiRuleCombination;
	}
}
