/**
 * 
 */
package rcpsp.algorithms.sampler;


import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public class RandomSampler extends AbstractSampler {


	@Override
	public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule) {
		double[] prob = new double[integerList.size()];
		for (int i = 0; i < prob.length; i++) {
			prob[i] = 1.0 / (double) prob.length;			
		}
		return prob;
	}


	@Override
	public String toString() {
		return "RS";
	}
}
