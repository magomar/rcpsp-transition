/**
 * 
 */
package rcpsp.algorithms.sampler;


import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class DeterministicSampler extends AbstractSampler {

	@Override
	public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule) {
		double[] prob = new double[integerList.size()];
		prob[0] = 1.0;
		for (int i = 1; i < integerList.size(); i++) {
			prob[i] = 0.0;			
		}
		return prob;
	}

	@Override
	public String toString() {
		return "";
	}


}
