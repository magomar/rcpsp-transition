/**
 * 
 */
package rcpsp.algorithms.sampler;


import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class ModifiedRBRS extends AbstractSampler {
	private double epsilon;
	private final double alpha;
	private final double lambda;

	/**
	 * @param alpha
	 * @param lambda
	 */
	public ModifiedRBRS(double alpha, double lambda) {
		this.alpha = alpha;
		this.lambda = lambda;
	}

	@Override
	public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule) {
		double[] priority = priorityRule.getPriorityValues(integerList);
		double[] regret = new double[priority.length];
		double[] prob = new double[priority.length];
		double maxPriority = Double.NEGATIVE_INFINITY;
		double minPositivePriority = Double.POSITIVE_INFINITY;
		boolean positivePriority = false;

		for (int i = 0; i < priority.length; i++) {
			maxPriority = Math.max(priority[i], maxPriority);
		}

		for (int i = 0; i < priority.length; i++) {
			regret[i] = maxPriority - priority[i];
			if (regret[i] > 0) {
				positivePriority = true;
				minPositivePriority = Math.min(regret[i], minPositivePriority);
			}
		}

		if (positivePriority) epsilon = minPositivePriority / lambda;
		else epsilon = 1;

		double sumRegret = 0;
		for (int i = 0; i < priority.length; i++) {
			regret[i] = Math.pow(regret[i] + epsilon, alpha);
			sumRegret += regret[i];
		}
		for (int i = 0; i < priority.length; i++) {
			prob[i] = regret[i] / sumRegret;			
		}
		return prob;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MRBRS(" + alpha + "," + lambda + ")";
	}
}