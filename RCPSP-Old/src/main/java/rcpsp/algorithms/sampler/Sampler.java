/**
 *
 */
package rcpsp.algorithms.sampler;

import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public interface Sampler {

	/**
	 * @param integerList
	 * @param priorityRule
	 * @return
	 */
	public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule);
	/**
	 * @param alternatives
	 * @param priorityRule
	 * @return
	 */
	public List<Integer> sampleOne(List<Integer> alternatives, PriorityRule priorityRule);
	/**
	 * @param alternatives
	 * @param priorityRule
	 * @param sampleSize
	 * @return
	 */
	public List<Integer> sample(List<Integer> alternatives, PriorityRule priorityRule, int sampleSize);
	
	/**
	 * @return
	 */
	public String toString();
	
	/**
	 * @return defensive copy of the object
	 */
	public Sampler newInstance();
}
