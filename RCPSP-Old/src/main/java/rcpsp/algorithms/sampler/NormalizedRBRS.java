/**
 * 
 */
package rcpsp.algorithms.sampler;


import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class NormalizedRBRS extends AbstractSampler {
	private double epsilon;
	private final double alpha;
	

	/**
	 * @param alpha
	 */
	public NormalizedRBRS(double alpha) {
		this.alpha = alpha;
	}

	@Override
	public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule) {
		double[] priority = priorityRule.getPriorityValues(integerList);
		double[] regret = new double[priority.length];
		double[] prob = new double[priority.length];
		double maxPriority = Double.NEGATIVE_INFINITY;
		double minPriority = Double.POSITIVE_INFINITY;
		double minPositivePriority = Double.POSITIVE_INFINITY;
		boolean zeroPriority = false;
		boolean positivePriority = false;
		
		for (int i = 0; i < priority.length; i++) {
			maxPriority = Math.max(priority[i], maxPriority);
			minPriority = Math.min(priority[i], minPriority);
		}
		
		for (int i = 0; i < priority.length; i++) {
			regret[i] = maxPriority - priority[i] + minPriority;
			if (regret[i] == 0) zeroPriority = true;
			else if (regret[i] > 0) {
				positivePriority = true;
				minPositivePriority = Math.min(regret[i], minPositivePriority);
			}
		}
		
		if (positivePriority) epsilon = minPositivePriority / 10.0;
		else epsilon = 1;
		
		double sumRegret = 0;
		for (int i = 0; i < priority.length; i++) {
			if (zeroPriority) 
				regret[i] = Math.pow(regret[i] + epsilon, alpha);
			else regret[i] = Math.pow(regret[i], alpha);
			sumRegret += regret[i];
		}
		for (int i = 0; i < priority.length; i++) {
			prob[i] = regret[i] / sumRegret;			
		}
		return prob;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NRBRS(" + alpha + ")";
	}
}