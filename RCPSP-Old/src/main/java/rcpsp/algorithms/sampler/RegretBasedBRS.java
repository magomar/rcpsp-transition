/**
 *
 */
package rcpsp.algorithms.sampler;

import rcpsp.algorithms.heuristics.PriorityRule;

import java.util.List;

/**
 * @author Mario Gomez
 *
 */
public final class RegretBasedBRS extends AbstractSampler {

    private final double epsilon;
    private final double alpha;

    /**
     * @param epsilon
     * @param alpha
     */
    public RegretBasedBRS(double epsilon, double alpha) {
        this.epsilon = epsilon;
        this.alpha = alpha;
    }

    @Override
    public double[] getProbabilityValues(List<Integer> integerList, PriorityRule priorityRule) {
        double[] priority = priorityRule.getPriorityValues(integerList);
        double[] regret = new double[priority.length];
        double[] prob = new double[priority.length];
        double maxPriority = Double.NEGATIVE_INFINITY;
        double sumRegret = 0;
        for (int i = 0; i < priority.length; i++) {
            maxPriority = Math.max(priority[i], maxPriority);
        }
        for (int i = 0; i < priority.length; i++) {
            regret[i] = Math.pow(maxPriority - priority[i] + epsilon, alpha);
            sumRegret += regret[i];
        }
        for (int i = 0; i < priority.length; i++) {
            prob[i] = regret[i] / sumRegret;
        }
        return prob;
    }

    @Override
    public String toString() {
        return "RBRS(" + epsilon + "," + alpha + ")";
    }
}
