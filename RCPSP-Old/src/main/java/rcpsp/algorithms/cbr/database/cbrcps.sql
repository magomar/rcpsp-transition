create database cbrcps;
use cbrcps;
drop table cbrcps;
create table cbrcps(caseId VARCHAR(16),dataset VARCHAR(16),numJobs INTEGER,numRenewableResources INTEGER,numNonRenewableResources INTEGER,networkComplexity FLOAT,renewableResourceFactor FLOAT,nonRenewableResourceFactor FLOAT,renewableResourceStrenght FLOAT,nonRenewableResourceStrenght FLOAT,activityList VARCHAR(256),priorityRule VARCHAR(128));
insert into cbrcps values('example_casebase1','example_casebase',7,1,0,1,1,0,0,0,'0 1 2 4 3 5 6 ','LFT');
insert into cbrcps values('example_casebase2','example_casebase',7,1,0,1,1,0,0,0,'0 1 2 4 3 5 6 ','LFT');
insert into cbrcps values('example_casebase3','example_casebase',7,1,0,1,1,0,0,0,'0 1 2 3 4 5 6 ','LFT');
insert into cbrcps values('example_casebase4','example_casebase',8,1,0,1,1,0,0,0,'0 2 1 4 3 6 5 7 ','LFT');
