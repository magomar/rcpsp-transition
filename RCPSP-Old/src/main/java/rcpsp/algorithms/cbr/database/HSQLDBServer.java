package rcpsp.algorithms.cbr.database;

import jcolibri.util.FileIO;
import org.apache.commons.logging.LogFactory;
import org.hsqldb.Server;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;

/**
 * Creates a data base server with the tables for the CBRCPS application using the HSQLDB library.
 */

public class HSQLDBServer
{
	static boolean initialized = false;

	private static Server server;

	/**
	 * Initialize the server
	 */
	public static void init()
	{
		if (initialized)
			return;
		LogFactory.getLog(HSQLDBServer.class).info("Creating data base ...");

		server = new Server();
		server.setDatabaseName(0, "cbrcps");
		server.setDatabasePath(0, "mem:cbrcps;sql.enforce_strict_size=true");

		server.setLogWriter(null);
		server.setErrWriter(null);
		server.setSilent(true);
		server.start();

		initialized = true;
		try
		{
			Class.forName("org.hsqldb.jdbcDriver");

			PrintStream out = new PrintStream(new ByteArrayOutputStream());
			Connection conn = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/cbrcps", "sa", "");
			SqlFile file = new SqlFile(
					new	File(FileIO.findFile("cbrcps/algorithms.cbr/database/cbrcps.sql").getFile()),false,new HashMap());
			file.execute(conn,out,out, true);

			LogFactory.getLog(HSQLDBServer.class).info("Data base generation finished");

		} catch (Exception e)
		{
			LogFactory.getLog(HSQLDBServer.class).error(e);
		}

	}

	/**
	 * Shutdown the server
	 */
	public static void shutDown()
	{

		if (initialized)
		{
			server.stop();
			initialized = false;
		}
	}

}
