/**
 * 
 */
package rcpsp.algorithms.cbr;

import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.data.jaxb.Job;

import java.util.Comparator;

/**
 * @author Mario Gomez
 */
public final class HybridCBPRComparator implements Comparator<Job> {
	final int[] jobRanking;
	final JobPriorityRule jobPriorityRule;
	
	/**
	 * @param jobRanking
	 */
	public HybridCBPRComparator(int[] jobRanking, JobPriorityRule jobPriorityRule) {
		this.jobRanking = jobRanking; 
		this.jobPriorityRule = jobPriorityRule;
	}
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Job o1, Job o2) {
//		int numJobs = jobRanking.length;
		int i1 = o1.getId();
		int i2 = o2.getId();
		return (jobRanking[i1] == -1 || jobRanking[i2] == -1 ?
				jobPriorityRule.compare(i1, i2) :
					jobRanking[i1] - jobRanking[i2]);
	}

}
