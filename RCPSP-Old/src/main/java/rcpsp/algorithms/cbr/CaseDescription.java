/**
 *
 */
package rcpsp.algorithms.cbr;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

/**
 * @author Mario Gomez
 *
 */
public final class CaseDescription implements CaseComponent {

	private String caseId;
	private String dataset;
	private Integer numJobs;
	private Integer numRenewableResources;
	private Integer numNonRenewableResources;
	private Double networkComplexity;
	private Double renewableResourceStrenght;
	private Double nonRenewableResourceStrenght;
	private Double renewableResourceFactor;
	private Double nonRenewableResourceFactor;

	/**
	 * @return the caseId
	 */
	public String getCaseId() {
		return caseId;
	}

	/**
	 * @param caseId the caseId to set
	 */
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	/**
	 * @return the dataset
	 */
	public String getDataset() {
		return dataset;
	}

	/**
	 * @param dataset the dataset to set
	 */
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	/**
	 * @return the networkComplexity
	 */
	public Double getNetworkComplexity() {
		return networkComplexity;
	}

	/**
	 * @param networkComplexity the networkComplexity to set
	 */
	public void setNetworkComplexity(Double networkComplexity) {
		this.networkComplexity = networkComplexity;
	}

	/**
	 * @return the nonRenewableResourceFactor
	 */
	public Double getNonRenewableResourceFactor() {
		return nonRenewableResourceFactor;
	}

	/**
	 * @param nonRenewableResourceFactor the nonRenewableResourceFactor to set
	 */
	public void setNonRenewableResourceFactor(Double nonRenewableResourceFactor) {
		this.nonRenewableResourceFactor = nonRenewableResourceFactor;
	}

	/**
	 * @return the nonRenewableResourceStrenght
	 */
	public Double getNonRenewableResourceStrenght() {
		return nonRenewableResourceStrenght;
	}

	/**
	 * @param nonRenewableResourceStrenght the nonRenewableResourceStrenght to set
	 */
	public void setNonRenewableResourceStrenght(Double nonRenewableResourceStrenght) {
		this.nonRenewableResourceStrenght = nonRenewableResourceStrenght;
	}

	/**
	 * @return the numJobs
	 */
	public Integer getNumJobs() {
		return numJobs;
	}

	/**
	 * @param numJobs the numJobs to set
	 */
	public void setNumJobs(Integer numJobs) {
		this.numJobs = numJobs;
	}

	/**
	 * @return the numNonRenewableResources
	 */
	public Integer getNumNonRenewableResources() {
		return numNonRenewableResources;
	}

	/**
	 * @param numNonRenewableResources the numNonRenewableResources to set
	 */
	public void setNumNonRenewableResources(Integer numNonRenewableResources) {
		this.numNonRenewableResources = numNonRenewableResources;
	}

	/**
	 * @return the numRenewableResources
	 */
	public Integer getNumRenewableResources() {
		return numRenewableResources;
	}

	/**
	 * @param numRenewableResources the numRenewableResources to set
	 */
	public void setNumRenewableResources(Integer numRenewableResources) {
		this.numRenewableResources = numRenewableResources;
	}

	/**
	 * @return the renewableResourceFactor
	 */
	public Double getRenewableResourceFactor() {
		return renewableResourceFactor;
	}

	/**
	 * @param renewableResourceFactor the renewableResourceFactor to set
	 */
	public void setRenewableResourceFactor(Double renewableResourceFactor) {
		this.renewableResourceFactor = renewableResourceFactor;
	}

	/**
	 * @return the renewableResourceStrenght
	 */
	public Double getRenewableResourceStrenght() {
		return renewableResourceStrenght;
	}

	/**
	 * @param renewableResourceStrenght the renewableResourceStrenght to set
	 */
	public void setRenewableResourceStrenght(Double renewableResourceStrenght) {
		this.renewableResourceStrenght = renewableResourceStrenght;
	}

	/* (non-Javadoc)
	 * @see jcolibri.cbrcore.CaseComponent#getIdAttribute()
	 */
	public Attribute getIdAttribute() {
		return new Attribute("caseId", this.getClass());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + caseId + ";" + dataset + ";"
		+ numJobs + ";" + numRenewableResources + ";" + numNonRenewableResources + ";" 
		+ networkComplexity + ";"
		+ renewableResourceStrenght +";" + nonRenewableResourceStrenght +";"
		+ renewableResourceFactor + ";" + nonRenewableResourceFactor +
		")";
	}
}
