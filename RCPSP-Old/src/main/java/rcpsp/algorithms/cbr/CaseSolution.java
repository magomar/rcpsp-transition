/**
 * 
 */
package rcpsp.algorithms.cbr;

import jcolibri.cbrcore.Attribute;
import jcolibri.cbrcore.CaseComponent;

/**
 * @author Mario Gomez
 *
 */
public final class CaseSolution implements CaseComponent {
	
	/** The identifier of the case */
	private String id;
	/** An activity list encoding a solution */
	private String activityList;
	/** A priority rule */
	private String priorityRule;
	
	/**
	 * @return the identifier of the case
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the activityList
	 */
	public String getActivityList() {
		return activityList;
	}

	/**
	 * @param activityList the activityList to set
	 */
	public void setActivityList(String activityList) {
		this.activityList = activityList;
	}

	/**
	 * @return the priorityRule
	 */
	public String getPriorityRule() {
		return priorityRule;
	}

	/**
	 * @param priorityRule the priorityRule to set
	 */
	public void setPriorityRule(String priorityRule) {
		this.priorityRule = priorityRule;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see jcolibri.cbrcore.CaseComponent#getIdAttribute()
	 */
	@Override
	public Attribute getIdAttribute() {
		return new Attribute("id", this.getClass());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + id + ";" + activityList + ";" + priorityRule + ")";
	}
}
