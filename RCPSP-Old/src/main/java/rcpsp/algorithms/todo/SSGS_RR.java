/**
 *
 */
package rcpsp.algorithms.todo;

import rcpsp.algorithms.Constant;
import rcpsp.algorithms.heuristics.jobselection.AbstractJobPriorityRule;
import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.modeselection.AbstractModePriorityRule;
import rcpsp.algorithms.heuristics.modeselection.ModePriorityRule;
import rcpsp.algorithms.sampler.AbstractSampler;
import rcpsp.algorithms.sampler.DeterministicSampler;
import rcpsp.algorithms.sampler.Sampler;
import rcpsp.algorithms.scheduler.AbstractScheduler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.ResourcesArray;
import rcpsp.data.Solution;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//TODO Many things, not finished yet !!

/**
 * @author Mario Gomez
 *
 * Implementation of the Serial Scheduling Generation Scheme (SSGS) with random rule (RR) extension The RR extension
 * uses a probabilistic function to select the priority rule to apply in each step of the scheduling This class provides
 * the functionality required to execute heuristic algorithms for the RCPSP and MRCPSP problems It supports a)
 * Deterministic scheduling (one iteration) heuristic algorithms, including both job selection heuristics and mode
 * selection heuristics b) Probabilistic scheduling (several iterations) - Probabilistic selection of next job to
 * schedule (eg. Biased Random Sampling) - Probabilistic selection of the heuristic used in every step of an iteration
 * c) Multimode, with one heuristic used to sort the modes
 *
 */
public final class SSGS_RR extends AbstractScheduler {

    private static final Logger LOG = Logger.getLogger(SSGS_RR.class.getName());
    // Algorithm fields
    // arrays indexed by level
//	protected List<Integer>[] eligibleJobs;
//	protected List<Integer>[] remainingEligibleJobs;
//	protected int[] selectedJob;
    // arrays indexed by job;
    protected List<Integer>[] remainingModes;
    protected int iterations;
//	protected int level;
//	protected Solution bestSolution;
    protected JobPriorityRule[] jobPriorityRule;
    protected ModePriorityRule modePriorityRule;
    protected Sampler sampler;
    // Auxiliar fields
    protected Random random = new Random(System.currentTimeMillis());
    protected int numSolutionsFound = 0;

    /**
     * @param jobPriorityRule
     * @param modePriorityRule
     * @param sampler
     * @param iterations
     */
    public SSGS_RR(JobPriorityRule[] jobPriorityRule, ModePriorityRule modePriorityRule,
            AbstractSampler sampler, int iterations) {
        super();
        this.jobPriorityRule = jobPriorityRule;
        this.modePriorityRule = modePriorityRule;
        this.sampler = sampler;
        this.iterations = iterations;
    }

    /**
     * This constructor is used for deterministic serial scheduling
     *
     * @param jobPriorityRule
     * @param modePriorityRule
     */
    public SSGS_RR(AbstractJobPriorityRule[] jobPriorityRule, AbstractModePriorityRule modePriorityRule) {
        this(jobPriorityRule, modePriorityRule, new DeterministicSampler(), 1);
    }

    /**
     * This constructor is used for deterministic serial scheduling with default heuristics
     */
    public SSGS_RR() {
        this(Constant.bestRule, Constant.bestMode[0], new DeterministicSampler(), 1);
    }

    @Override
    public void init(Project project) {
        super.init(project);
        remainingModes = new ArrayList[numJobs];
        for (int j = 0; j < numJobs; j++) {
            remainingModes[j] = new ArrayList<>();
        }
        for (int i = 0; i < jobPriorityRule.length; i++) {
            this.jobPriorityRule[i].setData(timeBounds, successors, modes, resource);
        }
        modePriorityRule.setData(modes, resource);
    }

    @Override
    public Solution solve(Project project) {

        int iterationsDone = 0;
        while (iterationsDone < iterations) {
            // Initialization [Step 1]
            //level 0
            level = 0;
            selectedJob[level] = 0;

            //level 1;
            level = 1;
            eligibleJobs[level].clear();
            remainingEligibleJobs[level].clear();
            //schedule dummy source 0
            eligibleJobs[level].add(0);
            remainingEligibleJobs[level].add(0);
            remainingModes[0].clear();
            remainingModes[0].add(modes[0].get(0).getId());
            selectedJob[level] = 0;
            scheduledJobs.clear();
            scheduledJobs.add(0);
            startTime[0] = 0;
            finishTime[0] = 0;
            mode[0] = 0;

            // level 2
            level = 2;
            eligibleJobs[level].clear();
            remainingEligibleJobs[level].clear();
            // generate eligible jobs and modes
            for (Integer successor : successors[0]) {
                eligibleJobs[level].add(successor);
                remainingEligibleJobs[level].add(successor);
                remainingModes[successor.intValue()].clear();
            }

//			Heuristic: sort the set of (remaining) eligible jobs
            JobPriorityRule selectedRule = samplePriorityRules();
            Collections.sort(remainingEligibleJobs[level], selectedRule);
//			Use sampling method to select one of the eligible jobs
            remainingEligibleJobs[level] = sampler.sampleOne(remainingEligibleJobs[level], selectedRule);
            selectedJob[level] = remainingEligibleJobs[level].get(0).intValue();

            // Heuristic: sort the available modes
            modePriorityRule.setSelectedJob(selectedJob[level]);
            for (Mode m : modes[selectedJob[level]]) {
                remainingModes[selectedJob[level]].add(m.getId());
            }
            Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
            mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
            // Select next untested mode or descendant
            while (true) {
                if (remainingModes[selectedJob[level]].size() > 0) {
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                    remainingModes[selectedJob[level]].remove(0);
                    findFeasibleStartTime();
                } else if (remainingEligibleJobs[level].size() > 1) {
                    remainingEligibleJobs[level].remove(0);
                    selectedJob[level] = remainingEligibleJobs[level].get(0);
//					// Heuristic: sort the available modes
                    modePriorityRule.setSelectedJob(selectedJob[level]);
                    for (Mode m : modes[selectedJob[level]]) {
                        remainingModes[selectedJob[level]].add(m.getId());
                    }
                    Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                    findFeasibleStartTime();
                } else { // One level backtracking [Step 3]
                    level--;
                    if (level == 0) { // Finish !
                        break;
                    } else { // unschedule previously scheduled job
                        int jobToUnSchedule = selectedJob[level];
                        scheduledJobs.remove(scheduledJobs.size() - 1);
                        Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                        int resourceID = 0;
                        for (Integer request : jobMode.getResourceRequest()) {
                            for (int k = 1; k <= level; k++) {
                                if (resource[resourceID].isRenewable()) {
                                    if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                            && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                    }
                                } else {
                                    if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                    }
                                }
                            }
                            resourceID++;
                        }
                    }
                }
            }
            iterationsDone++;
        }
        if (!bestSolution.isComplete()) {
            LOG.log(Level.INFO, "{0}: No solution found", toString());
        }
        System.out.println("Solutions found " + numSolutionsFound);
        return bestSolution;
    }

    /**
     * This method tries to schedule the next job in the set of eligible jobs for the current level If a
     * precedence-feasible and resource-feasible start time is found then the job is scheduled and a new set of eligible
     * jobs is computed for the next level If no feasible start time is found, then the method returns without any
     * change
     */
    private void findFeasibleStartTime() {
        boolean isPrecedenceFeasible = true;
        int jobToSchedule = selectedJob[level];
        Mode jobMode = modes[jobToSchedule].get(mode[jobToSchedule]);
        int duration = jobMode.getDuration();
        // First, compute time bounds (lower and upper bound) for start time
        // First condition: start time >= start time of latest job scheduled
        int startTimeLowerBound = startTime[selectedJob[level - 1]];
        // Second condition start time <= latest finish time (LF) - duration
        int startTimeUpperBound = timeBounds[jobToSchedule].getLatestFinishTime() - duration;

        if (startTimeLowerBound <= startTimeUpperBound) {
            // Third condition: start time >= finish time of its predecessors
            for (Integer predecessor : predecessors[jobToSchedule]) {
                int predecessorID = predecessor.intValue();
                if (finishTime[predecessorID] <= startTimeUpperBound) {
                    startTimeLowerBound = Math.max(startTimeLowerBound, finishTime[predecessorID]);
                } else {
                    isPrecedenceFeasible = false;
                    break;
                }
            }
        } else {
            isPrecedenceFeasible = false;
        }

        // Now find a start time in [startTimeLowerBound,startTimeUpperBound] which is resource feasible
        // Note that start time would be equal to the finish time of some job already scheduled
        // since renewable resources are liberated when a job finishes
        if (isPrecedenceFeasible) {
            int minFeasibleStartTime = startTimeUpperBound + 1;
            for (Integer scheduledJob : scheduledJobs) {
                int jobID = scheduledJob.intValue();
                int candidateStartTime = finishTime[jobID];
                if (candidateStartTime >= startTimeLowerBound
                        && candidateStartTime < minFeasibleStartTime
                        && remainingResources[jobID].isResourceFeasible(jobMode.getResourceRequest())) {
                    minFeasibleStartTime = candidateStartTime;
                }
            }
            if (minFeasibleStartTime <= startTimeUpperBound) { // if it is resource && precedence feasible
                // feasible start time found, so schedule job and update resources arrays
                startTime[jobToSchedule] = minFeasibleStartTime;
                finishTime[jobToSchedule] = minFeasibleStartTime + duration;
                // Update resources arrays
                remainingResources[jobToSchedule].reset();
                int resourceID = 0;
                for (Integer request : modes[jobToSchedule].get(mode[jobToSchedule]).getResourceRequest()) {
                    if (!resource[resourceID].isRenewable()) {
                        remainingResources[jobToSchedule].useResource(resourceID,
                                request.intValue());
                    }
                    for (Integer scheduledJob : scheduledJobs) {
                        int jobIndex = scheduledJob.intValue();
                        if (resource[resourceID].isRenewable()) { //Renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]
                                    && finishTime[jobIndex] < finishTime[jobToSchedule]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            } else if (startTime[jobIndex] <= finishTime[jobToSchedule]
                                    && finishTime[jobToSchedule] < finishTime[jobIndex]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        } else { // Non renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            }
                            if (startTime[jobIndex] <= finishTime[jobToSchedule]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        }
                    }
                    resourceID++;
                }

                scheduledJobs.add(Integer.valueOf(jobToSchedule)); // add current job to list of scheduled jobs

                if (level == numJobs) {
                    updateBestSolution(); // new solution found [Step 5]
                } else { // Update the eligible set [Step 6]
                    level++;
                    eligibleJobs[level].clear();
                    remainingEligibleJobs[level].clear();
                    for (Integer eligibleJob : eligibleJobs[level - 1]) {
                        eligibleJobs[level].add(eligibleJob);
                        remainingEligibleJobs[level].add(eligibleJob);
                    }
                    eligibleJobs[level].remove(Integer.valueOf(jobToSchedule));
                    remainingEligibleJobs[level].remove(Integer.valueOf(jobToSchedule));
                    for (Integer candidateJob : successors[jobToSchedule]) {
                        if (!eligibleJobs[level].contains(candidateJob)) {
                            boolean eligible = true;
                            for (Integer predecessor : predecessors[candidateJob.intValue()]) {
                                if (!scheduledJobs.contains(predecessor)) {
                                    eligible = false;
                                    break;
                                }
                            }
                            if (eligible) {
                                eligibleJobs[level].add(candidateJob);
                                remainingEligibleJobs[level].add(candidateJob);
                            }
                        }
                    }
//					Heuristic: sort the set of (remaining) eligible jobs
                    JobPriorityRule selectedRule = samplePriorityRules();
                    Collections.sort(remainingEligibleJobs[level], selectedRule);
//					Use sampling method to select one of the eligible jobs
                    remainingEligibleJobs[level] = sampler.sampleOne(remainingEligibleJobs[level], selectedRule);
                    selectedJob[level] = remainingEligibleJobs[level].get(0).intValue();

//					// Heuristic: sort the available modes
                    modePriorityRule.setSelectedJob(selectedJob[level]);
                    for (Mode m : modes[selectedJob[level]]) {
                        remainingModes[selectedJob[level]].add(Integer.valueOf(m.getId()));
                    }
                    Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                }
            }
//			else // if it is not (precedence & resource) feasible in any mode, do not continue exploring this level
//				if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1)
//					remainingEligibleJobs[level].clear() ; // this will provoke a backtrack
        }
//		else  // if it is not (precedence) feasible in any mode, do not continue exploring this level
//			if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1)
//				remainingEligibleJobs[level].clear() ; // this will provoke a backtrack
    }

    /**
     *
     */
    private void updateBestSolution() {
        numSolutionsFound++;

        // Store solution and adjust time bounds [Step 7]
        ResourcesArray cloneRemainingResources[] = new ResourcesArray[remainingResources.length];
        for (int i = 0; i < cloneRemainingResources.length; i++) {
            cloneRemainingResources[i] = ResourcesArray.newInstance(remainingResources[i]);
        }
        bestSolution = new Solution(selectedJob.clone(), startTime.clone(), finishTime.clone(),
                mode.clone(), finishTime[selectedJob[level]], toString(), cloneRemainingResources);

//		System.out.println("Found new solution: "+ bestSolution.toString());

        int boundDecrement = timeBounds[numJobs - 1].getLatestFinishTime() - bestSolution.getMakespan() + 1;
        for (int j = 0; j < numJobs; j++) {
            timeBounds[j].setLatestStartTime(timeBounds[j].getLatestStartTime()
                    - boundDecrement);
            timeBounds[j].setLatestFinishTime(timeBounds[j].getLatestFinishTime()
                    - boundDecrement);
        }

        // Calculate lowest indexed level violating the time window [Step 8]
        scheduledJobs.clear();
        level = 1;
    }

    private JobPriorityRule samplePriorityRules() {
        JobPriorityRule selectedRule = null;
        double[] prob = new double[jobPriorityRule.length];
        for (int i = 0; i < prob.length; i++) {
            prob[i] = 1.0 / (double) prob.length;
        }
        double probAcum = 0;
        double randomEvent = random.nextDouble();
        for (int index = 0; index < prob.length; index++) {
            probAcum += prob[index];
            if (randomEvent <= probAcum) {
                selectedRule = jobPriorityRule[index];
                break;
            }
        }
        return selectedRule;
    }

    /*
     * (non-Javadoc) @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SSGS-RR_" + sampler.toString() + "_" + iterations + ":" + Arrays.asList(jobPriorityRule);
    }

    @Override
    public void close() {
        super.close();

    }

    @Override
    public Scheduler newInstance() {
        // TODO Auto-generated method stub
        return null;
    }
}
