/**
 * 
 */
package rcpsp.algorithms.matcher;


/**
 * @author Mario Gomez
 */
public abstract class AbstractProjectMatcher implements ProjectMatcher {

	/* (non-Javadoc)
	 * @see ProjectMatcher#newInstance()
	 */
	public ProjectMatcher newInstance() {
		ProjectMatcher newInstance = null;
		try {
			newInstance = this.getClass().newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newInstance;
	}
}
