/**
 * 
 */
package rcpsp.algorithms.matcher;

import absurdist.Absurdist;
import absurdist.ConceptSystem;
import rcpsp.algorithms.ProjectToGraphConverter;
import rcpsp.data.ProjectMatching;
import rcpsp.data.jaxb.Project;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeSet;

/**
 * @author Mario Gomez
 */
public final class AbsurdistMatcher extends AbstractProjectMatcher {

	/* (non-Javadoc)
	 * @see ProjectMatcher#match(Project, Project)
	 */
	public ProjectMatching matchProjects(Project source, Project target) {
		// Ensure that Absurdist will be created without 
		// expensive history arrays
		Absurdist.turnOffHistory();

		// learning rate (use default if none is supplied)
		String flag =  "L" ;
		if (System.getProperty(flag)!=null) {
			Absurdist.lRate =Double.parseDouble(System.getProperty(flag));
		}
		System.out.println( "Learning rate: -DL=" +Absurdist.lRate);

		// step count (use default if none is supplied)
		flag =  "maxLoop" ;
		if (System.getProperty(flag)!=null) {
			Absurdist.maxLoop =Integer.parseInt(System.getProperty(flag));
		}
		System.out.println( "maxLoop=" +Absurdist.maxLoop);


		ConceptSystem systemO = ProjectToGraphConverter.convertToAbsurdist(source);
		ConceptSystem systemN = ProjectToGraphConverter.convertToAbsurdist(target);

		Absurdist absurdist = new Absurdist( systemO, systemN );

		// resetChi: Default is true
		final String flag2 =  "coefMode" ;
		System.out.println( "-D"+flag2+"=" +absurdist.coefMode);

		// exp: Default is false
		absurdist.useExponential = Boolean.getBoolean( "exp" );
		System.out.println( "-Dexp=" + absurdist.useExponential);

		absurdist.mapSystem( );
		try {
			absurdist.print( );
		} catch (Exception e) {
			e.printStackTrace();
		}
		// TODO create the map, see output to know absurdit format 
		TreeSet<String> cliqueList = null;
		// TODO obtain similarity
		double similarity = 0.0;
		
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (String clique : cliqueList) {
			StringTokenizer st = new StringTokenizer(clique, ":, ");
			while (st.hasMoreTokens()) {
				Integer caseJob = Integer.valueOf(st.nextToken());
				Integer problemJob = Integer.valueOf(st.nextToken());
				if (!map.containsKey(caseJob) && !map.containsValue(problemJob))
					map.put(caseJob, problemJob);
			}
		}
		return new ProjectMatching(similarity, map);
	}

}
