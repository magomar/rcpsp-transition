/**
 * 
 */
package rcpsp.algorithms.matcher;

import rcpsp.data.ProjectMatching;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 */
public interface ProjectMatcher {
	

	/**
	 * Compares a query project against a collection of projects, and returns a matching between the query project
	 * and the project in the collection that is the most similar to the former. 
	 * @param source a single project
	 * @param target a projects to match the query project with
	 * @return the matching between project and the most similar project in projects
	 */
	public ProjectMatching matchProjects(Project source, Project target);
//	/**
//	 * @return a new instance of the object 
//	 */
//	public ProjectMatcher newInstance();
}
