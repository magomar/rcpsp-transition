/**
 * 
 */
package rcpsp.algorithms.matcher;

import rcpsp.algorithms.ProjectToGraphConverter;
import rcpsp.data.ProjectMatching;
import rcpsp.data.jaxb.Project;
import simpack.api.IGraphAccessor;
import simpack.measure.graph.SubgraphIsomorphism;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeSet;

/**
 * @author Mario Gomez
 */
public final class SimPackMatcher extends AbstractProjectMatcher {

	/* (non-Javadoc)
	 * @see ProjectMatcher#match(Project, Project)
	 */
	public ProjectMatching matchProjects(Project source, Project target) {
//		 Compare problem structure using graph similarity to decide which is the best case
		IGraphAccessor g1 = ProjectToGraphConverter.convertToSimPack(source);
		IGraphAccessor g2 = ProjectToGraphConverter.convertToSimPack(target);
		SubgraphIsomorphism sgi = new SubgraphIsomorphism(g1,g2);
		sgi.calculate();
		double similarity = sgi.getSimilarity();
		TreeSet<String> cliqueList = sgi.getCliqueList();
		HashMap<Integer,Integer> map = new HashMap<Integer, Integer>();
		for (String clique : cliqueList) {
			StringTokenizer st = new StringTokenizer(clique, ":, ");
			while (st.hasMoreTokens()) {
				Integer caseJob = Integer.valueOf(st.nextToken());
				Integer problemJob = Integer.valueOf(st.nextToken());
				if (!map.containsKey(caseJob) && !map.containsValue(problemJob))
					map.put(caseJob, problemJob);
			}
		}

		return new ProjectMatching(similarity, map);
	}

}
