package rcpsp.algorithms;

import absurdist.Concept;
import absurdist.ConceptSystem;
import absurdist.Relation;
import absurdist.RelationModel;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.DefaultGraph;
import org.jgrapht.DirectedGraph;
import org.jgrapht.graph.DefaultDirectedGraph;
import rcpsp.data.jaxb.Job;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.jgrapht.Link;
import rcpsp.jgrapht.Node;
import simpack.accessor.graph.SimpleGraphAccessor;
import simpack.api.IGraphAccessor;
import simpack.util.graph.GraphNode;

import java.util.Hashtable;
import java.util.Vector;

public final class ProjectToGraphConverter {

	/**
	 * Converts a project into a graph using JGraphT model
	 * @param project
	 * @return a a DirectedGraph
	 */
	public static DirectedGraph<Node,Link> convertToJGraphT(Project project) {
		DirectedGraph<Node, Link> graph = new DefaultDirectedGraph<Node, Link>(Link.class);
		Node[] node = new Node[project.getNumJobs()];
		for (Job job : project.getJob()) {
			node[job.getId()] = new Node(job.getId());
			graph.addVertex(node[job.getId()]);
		}

		for (Job job : project.getJob()) {
			for (Integer successor : job.getSuccessor()) {
				Link link = new Link(successor.intValue(), 0);
				graph.addEdge(node[job.getId()], node[successor.intValue()], link);
			}
		}

		return graph;
	}

	/**
	 * Converts a project into a graph using GraphStream model
	 * @param project
	 * @return a Graph
	 */
	public static Graph convertToGraphStream(Project project) {
		Graph g = new DefaultGraph(project.getName());
		for (Job job : project.getJob()) {
			String nodeID = Integer.toString(job.getId());
			g.addNode("J" + nodeID);
			org.graphstream.graph.Node node = g.getNode("J" + nodeID);
			Mode mode = job.getMode().get(0);
			node.addAttribute("D", mode.getDuration());
			node.addAttribute("R", mode.getResourceRequest().toString());
			node.addAttribute("label", "J" + nodeID);
		}
		int edgeCounter = 0;
		for (Job job : project.getJob()) {
			for (Integer successor : job.getSuccessor()) {
				g.addEdge("E"+ Integer.toString(edgeCounter++), "J"+ Integer.toString(job.getId()), "J"+successor.toString(), true);
			}
		}
		return g;
	}
	
	/**
	 * Converts a project into a graph using SimPack model
	 * @param project
	 * @return an IGraphAccessor
	 */
	public static IGraphAccessor convertToSimPack(Project project) {
//		System.out.println(project.getName());
		IGraphAccessor graph = new SimpleGraphAccessor();
		GraphNode[] node = new GraphNode[project.getNumJobs()];
		for (Job job : project.getJob()) {
			int jobID = job.getId();
			node[jobID] = new GraphNode(jobID);
//			StringBuilder s = new StringBuilder();
//			s.append(jobID);
//			ArrayList<Integer> resourceRequests = job.getModes().get(0).getResourceRequests();
//			for (Integer request : resourceRequests) {
//				s.append(request);
//			}
//			node[jobID].setUserObject(s.toString());
//			System.out.println("Node: " + node[jobID].getLabel() + ", " + node[jobID].getUserObject());
			graph.addNode(node[jobID]);
		}

		for (Job job : project.getJob()) {
			for (Integer successor : job.getSuccessor()) {
				graph.setEdge(node[job.getId()], node[successor.intValue()]);
			}
		}
		return graph;
	}
	
	public static ConceptSystem convertToAbsurdist(Project project) {
	    String name = project.getName();
	    Vector categories = new Vector();
	    Vector models = new Vector();
	    Vector concepts = new Vector();
	    Hashtable relations = new Hashtable(); 
		
	    String defaultCategory = "Job";
	    RelationModel defaultRelationModel = new RelationModel("Precedes", true); 
	    
	    categories.add(defaultCategory);
		models.add(defaultRelationModel);
		for (Job job : project.getJob()) {
			concepts.add(new Concept(job.getId(), defaultCategory));
		}
		ConceptSystem graph = new ConceptSystem(name, categories, models, concepts, relations);
		
		for (Job job : project.getJob()) {
			for (Integer successor : job.getSuccessor()) {
				int i = job.getId();
				int j = successor.intValue();
				Relation relation =  new Relation(defaultRelationModel, (Concept) concepts.get(i), (Concept)concepts.get(j));
				graph.putRelation(i, j, relation);
			}
		}
		
		return graph;
	}

}
