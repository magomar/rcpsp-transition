package rcpsp.algorithms;

import rcpsp.algorithms.heuristics.jobselection.*;
import rcpsp.algorithms.heuristics.modeselection.ModePriorityRule;
import rcpsp.algorithms.heuristics.modeselection.SFM;
import rcpsp.algorithms.sampler.AbstractSampler;
import rcpsp.algorithms.sampler.ModifiedRBRS;

/**
 * @author Mario Gomez
 *
 */

public final class Constant {
	public final static String datasetsPath = "datasets/";
	public final static String resultsPath = "results/";
	public final static String sqlPath = "src/cbrcps/algorithms.cbr/database/";

	private final static JobPriorityRule[][] mr = {
		{new EST(), new MSLK()},
		{new EST(), new LFT()},
		{new EST(), new LST()},
		{new EST(), new LFT(), new MSLK()},
		{new EST(), new JN()},
		{new EST(), new MTS()},
		{new EST(), new NIS()},
		{new EST(), new WRUP()}

	};
	private final static  double[][] wmr = {
		{0.80, 0.20},
		{0.80, 0.20},
		{0.80, 0.20},
		{0.9, 0.09, 0.01},
		{0.9, 0.10},
		{0.99, 0.01},
		{0.80, 0.20},
		{0.80, 0.20},
	};

	public static JobPriorityRule[] bestRule = {
		new MultiPriorityRule(mr[0], wmr[0]),
		new MultiPriorityRule(mr[1], wmr[1]),
		new MultiPriorityRule(mr[2], wmr[2]),
		new MultiPriorityRule(mr[3], wmr[3]),
		new MultiPriorityRule(mr[4], wmr[4]),
		new MultiPriorityRule(mr[5], wmr[5]),
		new MultiPriorityRule(mr[6], wmr[6]),
//		new EST(),
		new LST(),
//		new DFS(),
//		new TRS(),
		new LFT(),
//		new WRUP(),
		new MTS(),
	};

	public final static JobPriorityRule[] singleRule = {
		new LST(),
		new LFT(),
		new EST(),
		new EFT(),
		new WRUP(),
		new MTS(),
		new MSLK(),
		new TRS(),
		new NIS(),
		new JN(),
		new GRPW(),
		new LPT(),
		new SPT(),
		new TRD(),
		new DFS(),
	};

	public final static ModePriorityRule[] bestMode = {
		new SFM(),
	};

	public final static int[] iterations = {
		500, 
		1000,
		5000
		};
	public final static double epsilon = 1;
	public final static double[] alpha = {
//		12, 13, 14,
		15
		};
	public final static double[] lambda = {10};

	public final static AbstractSampler bestSampler = new ModifiedRBRS(10, 10);

	/**
	 * @param ruleName the name of a priority rule
	 * @return the priority rule with the given name
	 */
	public static JobPriorityRule getPriorityRuleByName(String ruleName) {
		JobPriorityRule result = null;
		for (JobPriorityRule rule : singleRule) {
			if (rule.toString().equals(ruleName)) {
				result = rule;
				break;
			}
		}

		return result;
	}
}
