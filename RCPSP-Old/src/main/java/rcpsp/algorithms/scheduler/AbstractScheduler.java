package rcpsp.algorithms.scheduler;

import rcpsp.data.ProjectDescription;
import rcpsp.data.ResourcesArray;
import rcpsp.data.Solution;
import rcpsp.data.TimeBounds;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;
import rcpsp.data.jaxb.Resource;
import rcpsp.util.SingleThreadStopwatch;
import rcpsp.util.Stopwatch;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 * Skeleton implementation of a Scheduler algorithm. This class can be extended by concrete scheduling algorithms
 */
public abstract class AbstractScheduler implements Scheduler, Cloneable {

    private static final Logger LOG = Logger.getLogger(AbstractScheduler.class.getName());
    //	Problem fields
    protected int numJobs;
    protected List<Integer>[] predecessors;
    protected List<Integer>[] successors;
    protected List<Mode>[] modes;
    protected Resource[] resource;
    protected int timeHorizon;
    protected TimeBounds[] timeBounds; // lower and upper bounds for start/finish times of jobs
    // Solution fields
    protected List<Integer> scheduledJobs;
    //	arrays indexed by job index
    protected ResourcesArray[] remainingResources;
    protected int[] startTime;
    protected int[] finishTime;
    protected int[] mode;
    // Algorithm fields
    // arrays indexed by level
    protected List<Integer>[] eligibleJobs;
    protected List<Integer>[] remainingEligibleJobs;
    protected int[] selectedJob;
    protected int level;
    protected Solution bestSolution;
    //	 Auxiliary fields
    protected final Stopwatch stopwatch = new Stopwatch();
    /**
     * project description is used to compute project descriptors: network complexity, resource strenght, resource
     * factor
     */
    protected ProjectDescription projectDescription;

    /**
     * @param project
     */
    protected void init(Project project) {
        System.out.println("? Solving " + project.getName() + " with algorithm " + getAlgorithm());
        projectDescription = new ProjectDescription(project);
        numJobs = projectDescription.getNumJobs();
        predecessors = projectDescription.getPredecessors();
        successors = projectDescription.getSuccessors();
        modes = projectDescription.getModes();
        resource = projectDescription.getResource();
        timeHorizon = projectDescription.getTimeHorizon();

        scheduledJobs = new ArrayList<>(numJobs);
        remainingResources = new ResourcesArray[numJobs];
        startTime = new int[numJobs];
        finishTime = new int[numJobs];
        mode = new int[numJobs];
        for (int j = 0; j < numJobs; j++) {
            remainingResources[j] = new ResourcesArray(resource);
        }

        selectedJob = new int[numJobs + 1];
        eligibleJobs = new ArrayList[numJobs + 1];
        remainingEligibleJobs = new ArrayList[numJobs + 1];
        for (int i = 0; i <= numJobs; i++) {
            eligibleJobs[i] = new ArrayList<>();
            remainingEligibleJobs[i] = new ArrayList<>();
        }

        //		Initialize solution 
        bestSolution = new Solution(numJobs, timeHorizon);

        //		(Pre)Computation of time bounds using Critical Path Analysis
        timeBounds = ProjectDescription.performCriticalPathAnalysis(projectDescription);
        stopwatch.start();
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#getAlgorithm()
     */
    @Override
    public String getAlgorithm() {
        return this.toString();
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#close()
     */
    @Override
    public void close() {
        long cpuTime = stopwatch.stop();
        System.out.println("* Final solution for " + projectDescription.getName() + ": " + bestSolution.toString() + SingleThreadStopwatch.timeToString(cpuTime));
        System.out.println();
    }
}
