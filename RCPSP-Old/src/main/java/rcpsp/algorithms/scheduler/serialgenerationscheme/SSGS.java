/**
 *
 */
package rcpsp.algorithms.scheduler.serialgenerationscheme;

import rcpsp.algorithms.Constant;
import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.modeselection.ModePriorityRule;
import rcpsp.algorithms.sampler.DeterministicSampler;
import rcpsp.algorithms.sampler.Sampler;
import rcpsp.algorithms.scheduler.AbstractScheduler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.ResourcesArray;
import rcpsp.data.Solution;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 * Implementation of the Serial Scheduling Generation Scheme (SSGS) This class provides the functionality required to
 * execute heuristic algorithms for the RCPSP and MRCPSP problems It supports a) Deterministic scheduling (one
 * iteration) heuristic algorithms, including both job selection heuristics and mode selection heuristics b)
 * Probabilistic scheduling (several iterations): usually referred to as sampling - Probabilistic selection of next job
 * to schedule based on priority values computed by Priority Rules (LFT, MTS, etc.). The problem is solved multiple
 * times (iterations) choosing different jobs to schedule at each stage, depending on the probabilities. Different
 * probability functions can be used by passing a sampling method to the constructor. (eg. Biased Random Sampling,
 * sRegret Based RBS, etc.). c) Multimode, with one heuristic used to sort the modes
 *
 */
public class SSGS extends AbstractScheduler {

    private static final Logger LOG = Logger.getLogger(SSGS.class.getName());
    protected final JobPriorityRule jobPriorityRule;
    protected final ModePriorityRule modePriorityRule;
    protected final Sampler sampler;
    protected final int iterations;
    protected ArrayList<Integer>[] remainingModes;

    /**
     * This constructor is used for probabilistic serial scheduling, usually referred to as sampling. The probability
     * function is defined by the sampler.
     *
     * @param jobPriorityRule is the heuristic used to decide the next job to schedule
     * @param modePriorityRule is the heuristic used to decide the execution mode of a job
     * @param sampler is the probability function to sample the solution space for several iterations
     * @param iterations number of times to sample the solution space (each sample generates a new schedule using the
     * probability function defined by the sampler
     */
    public SSGS(JobPriorityRule jobPriorityRule, ModePriorityRule modePriorityRule, Sampler sampler, int iterations) {
        this.jobPriorityRule = jobPriorityRule;
        this.modePriorityRule = modePriorityRule;
        this.sampler = sampler;
        this.iterations = iterations;
    }

    /**
     * This constructor is used for deterministic serial scheduling
     *
     * @param jobPriorityRule
     * @param modePriorityRule
     */
    public SSGS(JobPriorityRule jobPriorityRule, ModePriorityRule modePriorityRule) {
        this(jobPriorityRule, modePriorityRule, new DeterministicSampler(), 1);
    }

    /**
     * This constructor is used for deterministic serial scheduling, with default mode selection heuristic
     *
     * @param jobPriorityRule
     */
    public SSGS(JobPriorityRule jobPriorityRule) {
        this(jobPriorityRule, Constant.bestMode[0], new DeterministicSampler(), 1);
    }

    /**
     * This constructor is used for deterministic serial scheduling, with default heuristics
     *
     */
    public SSGS() {
        this(Constant.singleRule[0], Constant.bestMode[0], new DeterministicSampler(), 1);
    }

    /* (non-Javadoc)
     * @see rcpsp.AbstractScheduler#init(Project)
     */
    @Override
    public void init(Project project) {
        super.init(project);
        remainingModes = new ArrayList[numJobs];
        for (int j = 0; j < numJobs; j++) {
            remainingModes[j] = new ArrayList<>();
        }
        jobPriorityRule.setData(timeBounds, successors, modes, resource);
        modePriorityRule.setData(modes, resource);
    }

    /* (non-Javadoc)
     * @see algorithms.Solver#solve()
     */
    @Override
    public Solution solve(Project project) {
        init(project);

        int iterationsDone = 0;
        while (iterationsDone < iterations) {
            // Initialization [Step 1]
            //level 0
            level = 0;
            selectedJob[level] = 0;

            //level 1;
            level = 1;
            eligibleJobs[level].clear();
            remainingEligibleJobs[level].clear();
            //schedule dummy source 0
            eligibleJobs[level].add(0);
            remainingEligibleJobs[level].add(0);
            remainingModes[0].clear();
            remainingModes[0].add(modes[0].get(0).getId());
            selectedJob[level] = 0;
            scheduledJobs.clear();
            scheduledJobs.add(Integer.valueOf(0));
            startTime[0] = 0;
            finishTime[0] = 0;
            mode[0] = 0;

            // level 2
            level = 2;
            eligibleJobs[level].clear();
            remainingEligibleJobs[level].clear();
            // generate eligible jobs and modes
            for (Integer successor : successors[0]) {
                eligibleJobs[level].add(successor);
                remainingEligibleJobs[level].add(successor);
                remainingModes[successor].clear();
            }

            //			Heuristic: sort the set of (remaining) eligible jobs
            Collections.sort(remainingEligibleJobs[level], jobPriorityRule);
            //			Use sampling method to select one of the eligible jobs
            remainingEligibleJobs[level] = sampler.sampleOne(remainingEligibleJobs[level], jobPriorityRule);
            selectedJob[level] = remainingEligibleJobs[level].get(0);

            // Heuristic: sort the available modes
            modePriorityRule.setSelectedJob(selectedJob[level]);
            for (Mode mod : modes[selectedJob[level]]) {
                remainingModes[selectedJob[level]].add(mod.getId());
            }
            Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
            mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
            // Select next untested mode or descendant
            while (true) {
                if (remainingModes[selectedJob[level]].size() > 0) {
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                    remainingModes[selectedJob[level]].remove(Integer.valueOf(0));
                    findFeasibleStartTime();
                } else if (remainingEligibleJobs[level].size() > 1) {
                    remainingEligibleJobs[level].remove(Integer.valueOf(0));
                    selectedJob[level] = remainingEligibleJobs[level].get(0);
                    //					// Heuristic: sort the available modes
                    modePriorityRule.setSelectedJob(selectedJob[level]);
                    for (Mode mod : modes[selectedJob[level]]) {
                        remainingModes[selectedJob[level]].add(mod.getId());
                    }
                    Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                    findFeasibleStartTime();
                } else { // One level backtracking [Step 3]
                    level--;
                    if (level == 0) { // Finish !
                        break;
                    } else { // unschedule previously scheduled job
                        int jobToUnSchedule = selectedJob[level];
                        scheduledJobs.remove(Integer.valueOf(scheduledJobs.size() - 1));
                        Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                        int resourceID = 0;
                        for (Integer request : jobMode.getResourceRequest()) {
                            for (int k = 1; k <= level; k++) {
                                if (resource[resourceID].isRenewable()) {
                                    if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                            && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[selectedJob[k]].liberateResource(resourceID, request);
                                    }
                                } else {
                                    if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[selectedJob[k]].liberateResource(resourceID, request);
                                    }
                                }
                            }
                            resourceID++;
                        }
                    }
                }
            }
            iterationsDone++;
        }
        if (!bestSolution.isComplete()) {
            LOG.log(Level.INFO, "{0}: No solution found", toString());
        }
        return bestSolution;
    }

    /**
     * This method tries to schedule the next job in the set of eligible jobs for the current level If a
     * precedence-feasible and resource-feasible start time is found then the job is scheduled and a new set of eligible
     * jobs is computed for the next level If no feasible start time is found, then the method returns without any
     * change
     */
    protected void findFeasibleStartTime() {
        boolean isPrecedenceFeasible = true;
        int jobToSchedule = selectedJob[level];
        Mode jobMode = modes[jobToSchedule].get(mode[jobToSchedule]);
        int duration = jobMode.getDuration();
        // First, compute time bounds (lower and upper bound) for start time
        // First condition: start time >= start time of latest job scheduled
        int startTimeLowerBound = startTime[selectedJob[level - 1]];
        // Second condition start time <= latest finish time (LF) - duration
        int startTimeUpperBound = timeBounds[jobToSchedule].getLatestFinishTime() - duration;

        if (startTimeLowerBound <= startTimeUpperBound) {
            // Third condition: start time >= finish time of its predecessors
            for (Integer predecessor : predecessors[jobToSchedule]) {
                int predecessorID = predecessor.intValue();
                if (finishTime[predecessorID] <= startTimeUpperBound) {
                    startTimeLowerBound = Math.max(startTimeLowerBound, finishTime[predecessorID]);
                } else {
                    isPrecedenceFeasible = false;
                    break;
                }
            }
        } else {
            isPrecedenceFeasible = false;
        }

        // Now find a start time in [startTimeLowerBound,startTimeUpperBound] which is resource feasible
        // Note that start time would be equal to the finish time of some job already scheduled
        // since renewable resources are liberated when a job finishes
        if (isPrecedenceFeasible) {
            int minFeasibleStartTime = startTimeUpperBound + 1;
            for (Integer scheduledJob : scheduledJobs) {
                int jobID = scheduledJob.intValue();
                int candidateStartTime = finishTime[jobID];
                if (candidateStartTime >= startTimeLowerBound
                        && candidateStartTime < minFeasibleStartTime
                        && remainingResources[jobID].isResourceFeasible(jobMode.getResourceRequest())) {
                    minFeasibleStartTime = candidateStartTime;
                }
            }
            if (minFeasibleStartTime <= startTimeUpperBound) { // if it is resource && precedence feasible
                // feasible start time found, so schedule job and update resources arrays
                startTime[jobToSchedule] = minFeasibleStartTime;
                finishTime[jobToSchedule] = minFeasibleStartTime + duration;
                // Update resources arrays
                remainingResources[jobToSchedule].reset();
                int resourceID = 0;
                for (Integer request : modes[jobToSchedule].get(mode[jobToSchedule]).getResourceRequest()) {
                    if (!resource[resourceID].isRenewable()) {
                        remainingResources[jobToSchedule].useResource(resourceID,
                                request.intValue());
                    }
                    for (Integer scheduledJob : scheduledJobs) {
                        int jobIndex = scheduledJob.intValue();
                        if (resource[resourceID].isRenewable()) { //Renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]
                                    && finishTime[jobIndex] < finishTime[jobToSchedule]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            } else if (startTime[jobIndex] <= finishTime[jobToSchedule]
                                    && finishTime[jobToSchedule] < finishTime[jobIndex]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        } else { // Non renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            }
                            if (startTime[jobIndex] <= finishTime[jobToSchedule]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        }
                    }
                    resourceID++;
                }

                scheduledJobs.add(jobToSchedule); // add current job to list of scheduled jobs

                if (level == numJobs) {
                    updateBestSolution(); // new solution found [Step 5]
                } else { // Update the eligible set [Step 6]
                    level++;
                    eligibleJobs[level].clear();
                    remainingEligibleJobs[level].clear();
                    for (Integer eligibleJob : eligibleJobs[level - 1]) {
                        eligibleJobs[level].add(eligibleJob);
                        remainingEligibleJobs[level].add(eligibleJob);
                    }
                    eligibleJobs[level].remove(Integer.valueOf(jobToSchedule));
                    remainingEligibleJobs[level].remove(Integer.valueOf(jobToSchedule));
                    for (Integer candidateJob : successors[jobToSchedule]) {
                        if (!eligibleJobs[level].contains(candidateJob)) {
                            boolean eligible = true;
                            for (Integer predecessor : predecessors[candidateJob]) {
                                if (!scheduledJobs.contains(predecessor)) {
                                    eligible = false;
                                    break;
                                }
                            }
                            if (eligible) {
                                eligibleJobs[level].add(candidateJob);
                                remainingEligibleJobs[level].add(candidateJob);
                            }
                        }
                    }
                    //					Heuristic: sort the set of remaining eligible jobs => choses the evaluation order)
                    Collections.sort(remainingEligibleJobs[level], jobPriorityRule);
                    remainingEligibleJobs[level] = sampler.sampleOne(remainingEligibleJobs[level], jobPriorityRule);
                    selectedJob[level] = remainingEligibleJobs[level].get(0).intValue();
                    //					// Heuristic: sort the available modes
                    modePriorityRule.setSelectedJob(selectedJob[level]);
                    for (Mode mod : modes[selectedJob[level]]) {
                        remainingModes[selectedJob[level]].add(Integer.valueOf(mod.getId()));
                    }
                    Collections.sort(remainingModes[selectedJob[level]], modePriorityRule);
                    mode[selectedJob[level]] = remainingModes[selectedJob[level]].get(0);
                }
            } else // if it is not (precedence & resource) feasible in any mode, do not continue exploring this level
            if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
                remainingEligibleJobs[level].clear(); // this will provoke a backtrack
            }
        } else // if it is not (precedence) feasible in any mode, do not continue exploring this level
        if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
            remainingEligibleJobs[level].clear(); // this will provoke a backtrack
        }
    }

    /**
     *
     */
    protected void updateBestSolution() {
        // Store solution and adjust time bounds [Step 7]
        ResourcesArray cloneRemainingResources[] = new ResourcesArray[remainingResources.length];
        for (int i = 0; i < cloneRemainingResources.length; i++) {
            cloneRemainingResources[i] = ResourcesArray.newInstance(remainingResources[i]);
        }
        bestSolution = new Solution(selectedJob.clone(), startTime.clone(), finishTime.clone(),
                mode.clone(), finishTime[selectedJob[level]], toString(), cloneRemainingResources);

        //		System.out.println("Found new solution: "+ bestSolution.toString());

        int boundDecrement = timeBounds[numJobs - 1].getLatestFinishTime() - bestSolution.getMakespan() + 1;
        for (int j = 0; j < numJobs; j++) {
            timeBounds[j].setLatestStartTime(timeBounds[j].getLatestStartTime()
                    - boundDecrement);
            timeBounds[j].setLatestFinishTime(timeBounds[j].getLatestFinishTime()
                    - boundDecrement);
        }

        // Calculate lowest indexed level violating the time window [Step 8]
        scheduledJobs.clear();
        level = 1;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return (!"".equals(sampler.toString())
                ? "SSGS_" + sampler.toString() + "_" + iterations + ":" + jobPriorityRule.toString()
                : "SSGS:" + jobPriorityRule.toString());
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#newInstance()
     */
    @Override
    public Scheduler newInstance() {
        JobPriorityRule newJobPriorityRule = (JobPriorityRule) jobPriorityRule.newInstance();
        ModePriorityRule newModePriorityRule = (ModePriorityRule) modePriorityRule.newInstance();
        Sampler newSampler = sampler.newInstance();
        return new SSGS(newJobPriorityRule, newModePriorityRule, newSampler, iterations);
    }
}