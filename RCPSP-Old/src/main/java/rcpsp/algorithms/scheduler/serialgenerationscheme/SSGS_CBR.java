package rcpsp.algorithms.scheduler.serialgenerationscheme; /**
 * 
 */

import rcpsp.algorithms.Constant;
import rcpsp.algorithms.cbr.CBRCPS;
import rcpsp.algorithms.heuristics.jobselection.CaseBasedPriorityRule;
import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.modeselection.ModePriorityRule;
import rcpsp.algorithms.matcher.ProjectMatcher;
import rcpsp.algorithms.sampler.DeterministicSampler;
import rcpsp.algorithms.sampler.Sampler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.io.DatasetWrapper;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 *
 */

public class SSGS_CBR extends SSGS {
	protected final CBRCPS cbrcps;
	protected final DatasetWrapper cbWrapper;
	protected final ProjectMatcher matcher;
	protected final JobPriorityRule secondRule;
	
	/**
	 * @param caseBasedRule
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 * @param sampler
	 * @param iterations
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public SSGS_CBR(CaseBasedPriorityRule caseBasedRule, JobPriorityRule jobPriorityRule, ModePriorityRule modePriorityRule, Sampler sampler, int iterations, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher) {
		super((JobPriorityRule) caseBasedRule, modePriorityRule, sampler, iterations);
		this.secondRule = jobPriorityRule;
		this.cbrcps = cbrcps;
		this.cbWrapper = cbWrapper;
		this.matcher = matcher;
	}


	/**
	 * @param caseBasedRule
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public SSGS_CBR(CaseBasedPriorityRule caseBasedRule, JobPriorityRule jobPriorityRule, ModePriorityRule modePriorityRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher) {
		this(caseBasedRule, jobPriorityRule, modePriorityRule, new DeterministicSampler(), 1, cbrcps, cbWrapper, matcher);
	}


	/**
	 * @param caseBasedRule
	 * @param jobPriorityRule
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public SSGS_CBR(CaseBasedPriorityRule caseBasedRule, JobPriorityRule jobPriorityRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher) {
		this(caseBasedRule, jobPriorityRule , Constant.bestMode[0], new DeterministicSampler(), 1, cbrcps, cbWrapper, matcher);
	}


	/**
	 * @param caseBasedRule
	 * @param cbrcps
	 * @param cbWrapper
	 * @param matcher
	 */
	public SSGS_CBR(CaseBasedPriorityRule caseBasedRule, CBRCPS cbrcps, DatasetWrapper cbWrapper, ProjectMatcher matcher) {
		this(caseBasedRule, Constant.singleRule[0] , Constant.bestMode[0], new DeterministicSampler(), 1, cbrcps, cbWrapper, matcher);
	}

//	/* (non-Javadoc)
//	 * @see rcpsp.SSGS#init(Project)
//	 */
	@Override
	public void init(Project project) {
		super.init(project);
		secondRule.setData(timeBounds, successors, modes, resource);
		((CaseBasedPriorityRule) jobPriorityRule).build(project, secondRule, cbrcps, cbWrapper, matcher);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (sampler.toString() != "" ? 
				"SSGS_CBR_" + sampler.toString() + "_" + iterations + ":" + "k" + ":" + jobPriorityRule 
				: "SSGS_CBR:" + cbrcps.getK() + ":" + jobPriorityRule);
	}

	/* (non-Javadoc)
	 * @see rcpsp.Scheduler#newInstance()
	 */
	@Override
	public Scheduler newInstance() {
		CaseBasedPriorityRule newJobPriorityRule = (CaseBasedPriorityRule) jobPriorityRule.newInstance();
		ModePriorityRule newModePriorityRule = (ModePriorityRule) modePriorityRule.newInstance();
		JobPriorityRule newSecondRule = (JobPriorityRule) secondRule.newInstance();
		Sampler newSampler = sampler.newInstance();
		return new SSGS_CBR(newJobPriorityRule, newSecondRule, newModePriorityRule, newSampler, iterations, cbrcps, cbWrapper, matcher);
	}
	
}

