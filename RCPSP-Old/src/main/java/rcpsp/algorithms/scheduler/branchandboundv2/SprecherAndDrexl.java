package rcpsp.algorithms.scheduler.branchandboundv2;

import rcpsp.data.ResourcesArray;
import rcpsp.data.Solution;
import rcpsp.algorithms.scheduler.AbstractSchedulerV2;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;

import java.util.BitSet;
import java.util.logging.Logger;

/**
 * @author Mario Gomez Java implementation of the Sprecher & Drexl algorithm for solving the RCPSP This algorithm uses
 * Depth-First Bran & Bound search. To remark that this algorithm uses time bounds that are updated dynamically as new
 * solutions are found, which speeds up the search by discarding in advance partial solutions that are unable to improve
 * the best solution found so far. In addition, search can be set to be either exhaustive, or limited to finding one
 * solution
 */
public class SprecherAndDrexl extends AbstractSchedulerV2 {

    private static final Logger LOG = Logger.getLogger(SprecherAndDrexl.class.getName());
    protected int[] selectedJobIndex;
//    protected int counter;

    /* (non-Javadoc)
     * @see rcpsp.AbstractScheduler#init(Project)
     */
    @Override
    public void init(Project project) {
        super.init(project);
        selectedJobIndex = new int[numJobs + 1];
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#solve()
     */
    @Override
    public Solution solve(Project project) {
        init(project);

        // Initialization [Step 1]

        //level 0
        selectedJob[0] = 0;

        //level 1;
        eligibleJobs[1].set(0);
        selectedJobIndex[1] = 0;
        selectedJob[1] = 0;

        scheduledJobs.set(0);
        startTime[0] = 0;
        finishTime[0] = 0;
        mode[0] = 0;

        level = 2;
        BitSet eJobs = eligibleJobs[level];
        for (int s = successors[0].nextSetBit(0); s >= 0; s = successors[0].nextSetBit(s + 1)) {
            eJobs.set(s);
        }
        selectedJobIndex[level] = eJobs.nextSetBit(0);
        selectedJob[level] = selectedJobIndex[level];
        mode[selectedJob[level]] = -1;

        // Select next untested mode or descendant
        while (true) {
            if (mode[selectedJob[level]] < modes[selectedJob[level]].size() - 1) {
                mode[selectedJob[level]]++;
                findFeasibleStartTime();
            } else {
                selectedJobIndex[level] = eJobs.nextSetBit(selectedJobIndex[level] + 1);
                if (selectedJobIndex[level] >= 0) {
                    selectedJob[level] = selectedJobIndex[level];
                    mode[selectedJob[level]] = 0;
                    findFeasibleStartTime();
                } else { // selectedJobIndex[level] = -1, no more eligible jobs )
                    // One level backtracking [Step 3] (
                    level--;
                    eJobs = eligibleJobs[level];
                    if (level == 0) { // Finish !
                        break;
                    } else { // unschedule previously scheduled job
                        int jobToUnSchedule = selectedJob[level];
                        scheduledJobs.set(jobToUnSchedule, false);
                        Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                        int resourceID = 0;
                        for (Integer request : jobMode.getResourceRequest()) {
                            for (int k = 1; k <= level; k++) {
                                int sjk = selectedJob[k];
                                if (resource[resourceID].isRenewable()) { // Renewable resource
                                    if (startTime[jobToUnSchedule] <= finishTime[sjk]
                                            && finishTime[sjk] < finishTime[jobToUnSchedule]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[sjk].liberateResource(resourceID, request.intValue());
                                    }
                                } else { // Non renewable resource
                                    if (startTime[jobToUnSchedule] <= finishTime[sjk]) {
                                        // update resource arrays of a job already scheduled
                                        remainingResources[sjk].liberateResource(resourceID, request.intValue());
                                    }
                                }
                            }
                            resourceID++;
                        }
                    }
                }
            }
        }
        if (!bestSolution.isComplete()) {
            LOG.info("No solution found");
        }
        return bestSolution;
    }

    /**
     * This method tries to schedule the next job in the set of eligible jobs for the current level If a
     * precedence-feasible and resource-feasible start time is found then the job is scheduled and a new set of eligible
     * jobs is computed for the next level If no feasible start time is found, then the method ends without any change
     */
    protected void findFeasibleStartTime() {
//        if (counter % 1000 == 0) {
//            System.out.println(counter + "  l=" + level + "  job=" + selectedJob[level]);
//        }
//        counter++;
        boolean isPrecedenceFeasible = true;
        int jobToSchedule = selectedJob[level];
        Mode jobMode = modes[jobToSchedule].get(mode[jobToSchedule]);
        int duration = jobMode.getDuration();
        // First, compute time bounds (lower and upper bound) for start time
        // First condition: start time >= start time of latest job scheduled
        int startTimeLowerBound = startTime[selectedJob[level - 1]];
        // Second condition start time <= latest finish time (LF) - duration
        int startTimeUpperBound = timeBounds[jobToSchedule].getLatestFinishTime() - duration;

        if (startTimeLowerBound <= startTimeUpperBound) {
            // Third condition: start time >= finish time of its predecessors
            for (int p = predecessors[jobToSchedule].nextSetBit(0); p >= 0; p = predecessors[jobToSchedule].nextSetBit(p + 1)) {
                if (finishTime[p] <= startTimeUpperBound) {
                    startTimeLowerBound = Math.max(startTimeLowerBound,
                            finishTime[p]);
                } else {
                    isPrecedenceFeasible = false;
                    break;
                }
            }
        } else {
            isPrecedenceFeasible = false;
        }

        // Now find a start time in [startTimeLowerBound,startTimeUpperBound] which is resource feasible
        // Note that start time would be equal to the finish time of some job already scheduled
        // since renewable resources are liberated when a job finishes
        if (isPrecedenceFeasible) {
            int minFeasibleStartTime = startTimeUpperBound + 1;
            for (int j = scheduledJobs.nextSetBit(0); j >= 0; j = scheduledJobs.nextSetBit(j + 1)) {
                int candidateStartTime = finishTime[j];
                if (candidateStartTime >= startTimeLowerBound
                        && candidateStartTime < minFeasibleStartTime
                        && remainingResources[j].isResourceFeasible(jobMode.getResourceRequest())) {
                    minFeasibleStartTime = candidateStartTime;
                }
            }
            if (minFeasibleStartTime <= startTimeUpperBound) { // if it is resource && precedence feasible
                // feasible start time found, so schedule job and update resources arrays
                startTime[jobToSchedule] = minFeasibleStartTime;
                finishTime[jobToSchedule] = minFeasibleStartTime + duration;
                remainingResources[jobToSchedule].reset();
                int resourceID = 0;
                for (Integer request : modes[jobToSchedule].get(mode[jobToSchedule]).getResourceRequest()) {
                    if (!resource[resourceID].isRenewable()) {
                        remainingResources[jobToSchedule].useResource(resourceID,
                                request.intValue());
                    }
                    for (int j = scheduledJobs.nextSetBit(0); j >= 0; j = scheduledJobs.nextSetBit(j + 1)) {
                        if (resource[resourceID].isRenewable()) { //Renewable resource
                            if (startTime[jobToSchedule] <= finishTime[j]
                                    && finishTime[j] < finishTime[jobToSchedule]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[j].useResource(resourceID, request.intValue());
                            } else if (startTime[j] <= finishTime[jobToSchedule]
                                    && finishTime[jobToSchedule] < finishTime[j]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[j].get(mode[j]).getResourceRequest().get(resourceID));
                            }
                        } else { // Non renewable resource
                            if (startTime[jobToSchedule] <= finishTime[j]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[j].useResource(resourceID, request.intValue());
                            }
                            if (startTime[j] <= finishTime[jobToSchedule]) {
                                // update resources arrays of the job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[j].get(mode[j]).getResourceRequest().get(resourceID));
                            }
                        }
                    }

                    resourceID++;
                }
                scheduledJobs.set(jobToSchedule); // add current job to list of scheduled jobs

                if (level == numJobs) {
                    updateBestSolution(); // new solution found [Step 5]
                } else { // Update the eligible set [Step 6]
                    level++;
                    BitSet eJobs = eligibleJobs[level];
                    eJobs.clear();
                    for (int e = eligibleJobs[level - 1].nextSetBit(0); e >= 0; e = eligibleJobs[level - 1].nextSetBit(e + 1)) {
                        eJobs.set(e);
                    }
                    eJobs.set(jobToSchedule, false);
                    for (int candidateJob = successors[jobToSchedule].nextSetBit(0); candidateJob >= 0; candidateJob = successors[jobToSchedule].nextSetBit(candidateJob + 1)) {
                        if (!eJobs.get(candidateJob)) {
                            boolean eligible = true;
                            for (int p = predecessors[candidateJob].nextSetBit(0); p >= 0; p = predecessors[candidateJob].nextSetBit(p + 1)) {
                                if (!scheduledJobs.get(p)) {
                                    eligible = false;
                                    break;
                                }
                            }
                            if (eligible) {
                                eJobs.set(candidateJob);
                            }
                        }
                    }
                    selectedJobIndex[level] = eJobs.nextSetBit(0);
                    selectedJob[level] = selectedJobIndex[level];
                    mode[selectedJob[level]] = -1;
                }
            } else // if it is not (precedence & resource) feasible in any mode, do not continue exploring this level
            if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
                selectedJobIndex[level] = numJobs; // this will provoke a backtrack
            }
        } else // if it is not (precedence) feasible in any mode, do not continue exploring this level
        if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
            selectedJobIndex[level] = numJobs; // this will provoke a backtrack
        }
    }

    /**
     *
     */
    protected void updateBestSolution() {
        // Store solution and adjust time bounds [Step 7]
        ResourcesArray cloneRemainingResources[] = new ResourcesArray[remainingResources.length];
        for (int i = 0; i < cloneRemainingResources.length; i++) {
            cloneRemainingResources[i] = ResourcesArray.newInstance(remainingResources[i]);
        }

        bestSolution = new Solution(selectedJob.clone(), startTime.clone(), finishTime.clone(),
                mode.clone(), finishTime[selectedJob[level]], toString(), cloneRemainingResources);

//		stopwatch.stop();
//		System.out.println("Found new solution: "+ bestSolution.toString() + stopwatch.toString());

        int boundDecrement = timeBounds[numJobs - 1].getLatestFinishTime() - bestSolution.getMakespan() + 1;
        for (int j = 0; j < numJobs; j++) {
            timeBounds[j].setLatestStartTime(timeBounds[j].getLatestStartTime()
                    - boundDecrement);
            timeBounds[j].setLatestFinishTime(timeBounds[j].getLatestFinishTime()
                    - boundDecrement);
        }

        // Calculate lowest indexed level violating the time window [Step 8]
        int levelBacktrack = 1;
        for (int k = 1; k < level; k++) {
            if (finishTime[selectedJob[k]] > timeBounds[selectedJob[k]].getLatestFinishTime()) {
                levelBacktrack = k;
                break;
            }
        }

        // Variable-level backtracking [Step 9]
        for (int l = level; l >= levelBacktrack; l--) {
            int resourceID = 0;
            int jobToUnSchedule = selectedJob[l];
            scheduledJobs.set(jobToUnSchedule, false);
            Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
            for (Integer request : jobMode.getResourceRequest()) {
                for (int k = 1; k < l; k++) {
                    int sjk = selectedJob[k];
                    if (resource[resourceID].isRenewable()) {
                        if (startTime[jobToUnSchedule] <= finishTime[sjk]
                                && finishTime[sjk] < finishTime[jobToUnSchedule]) {
                            // update resource arrays of a job already scheduled
                            remainingResources[sjk].liberateResource(resourceID, request.intValue());
                        }
                    } else {
                        if (startTime[jobToUnSchedule] <= finishTime[sjk]) {
                            // update resource arrays of a job already scheduled
                            remainingResources[sjk].liberateResource(resourceID, request.intValue());
                        }
                    }
                }
                resourceID++;
            }
        }
        level = levelBacktrack;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SD";
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#newInstance()
     */
    @Override
    public Scheduler newInstance() {
        return new SprecherAndDrexl();
    }
}
