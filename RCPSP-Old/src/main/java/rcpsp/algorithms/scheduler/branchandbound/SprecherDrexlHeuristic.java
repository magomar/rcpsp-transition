package rcpsp.algorithms.scheduler.branchandbound;

import rcpsp.algorithms.Constant;
import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.Solution;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;

import java.util.Collections;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 * Heuristically guided version of the Sprecher & Drexl algorithm for solving the RCPSP While the original S&D algorithm
 * uses Depth-First Bran&Bound search, this algorithm uses heuristics to guide the search. The S&D algorithm modifies
 * time bounds dynamically as new solutions are found to speed up the search, therefore search could be accelerated by
 * finding good solutions earlier. The algorithm implemented by this class uses heuristics to do that: during the
 * schedule generation process, the next job to schedule is selected from the set of eligible jobs taking using certain
 * priority rules. These priorities are based on characteristics of the eligible jobs that are supposed to influence the
 * goodness of the final solution, so that the most promising solutions are explored first. In addition, search can be
 * set to be either exhaustive, or limited to finding one solution Finally, note that the heuristics are used only for
 * the selection of a job, but not for the selection of the execution mode; instead, all modes are explored.
 */
public class SprecherDrexlHeuristic extends SprecherDrexl {

    private static final Logger LOG = Logger.getLogger(SprecherDrexlHeuristic.class.getName());
    protected final JobPriorityRule jobPriorityRule;

    /**
     * @param jobPriorityRule
     */
    public SprecherDrexlHeuristic(JobPriorityRule jobPriorityRule) {
        super();
        this.jobPriorityRule = jobPriorityRule;
    }

    /**
     *
     */
    public SprecherDrexlHeuristic() {
        this(Constant.bestRule[0]);
    }

    /* (non-Javadoc)
     * @see rcpsp.SprecherDrexl#init(Project)
     */
    @Override
    public void init(Project project) {
        super.init(project);
        this.jobPriorityRule.setData(timeBounds, successors, modes, resource);
    }

    /* (non-Javadoc)
     * @see rcpsp.SprecherDrexl#solve()
     */
    @Override
    public Solution solve(Project project) {
        init(project);

        //level 0
        selectedJob[0] = 0;

        //level 1;
        eligibleJobs[1].add(0);
        selectedJobIndex[1] = 0;
        selectedJob[1] = 0;

        scheduledJobs.add(0);
        startTime[0] = 0;
        finishTime[0] = 0;
        mode[0] = 0;

        level = 2;
        for (Integer successor : successors[0]) {
            eligibleJobs[level].add(successor);
        }

        //		Heuristic: sorts the set of (remaining) eligible jobs => determines the evaluation order)
        Collections.sort(eligibleJobs[level], jobPriorityRule);

        selectedJobIndex[level] = 0;
        selectedJob[level] = eligibleJobs[level].get(0).intValue();
        mode[selectedJob[level]] = -1;

        // Select next untested mode or descendant
        while (true) {
            if (mode[selectedJob[level]] < modes[selectedJob[level]].size() - 1) {
                mode[selectedJob[level]]++;
                findFeasibleStartTime();
            } else if (selectedJobIndex[level] < eligibleJobs[level].size() - 1) {
                selectedJobIndex[level]++;
                selectedJob[level] = eligibleJobs[level].get(selectedJobIndex[level]).intValue();
                mode[selectedJob[level]] = 0;
                findFeasibleStartTime();
            } else { // One level backtracking [Step 3]
                level--;
                if (level == 0) { // Finish !
                    break;
                } else { // unschedule previously scheduled job
                    int jobToUnSchedule = selectedJob[level];
                    scheduledJobs.remove(scheduledJobs.size() - 1);
                    Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                    int resourceID = 0;
                    for (Integer request : jobMode.getResourceRequest()) {
                        for (int k = 1; k <= level; k++) {
                            if (resource[resourceID].isRenewable()) {
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                        && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            } else {
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            }
                        }
                        resourceID++;
                    }
                }
            }
        }

        if (!bestSolution.isComplete()) {
            LOG.info("No solution found");
        } else {
//			stopwatch.stop();
//			System.out.println(toString() +" > Solution found " + bestSolution.toString());
//			System.out.println(stopwatch.toString() + "   Nodes visited : " + nodesVisited + 
//			"   Nodes explored: " + nodesExplored +  "   Nodes scheduled: " + nodesScheduled + "   Time per node: " + 
//			(float) stopwatch.getTotalTime()/nodesVisited);
//			System.out.println();
        }
        return bestSolution;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SDH: " + jobPriorityRule.toString();
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#newInstance()
     */
    @Override
    public Scheduler newInstance() {
        JobPriorityRule newJobPriorityRule = (JobPriorityRule) jobPriorityRule.newInstance();
        return new SprecherDrexlHeuristic(newJobPriorityRule);
    }
}
