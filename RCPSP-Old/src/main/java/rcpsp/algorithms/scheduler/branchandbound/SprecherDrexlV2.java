/**
 *
 */
package rcpsp.algorithms.scheduler.branchandbound;

import rcpsp.data.ResourcesArray;
import rcpsp.data.Solution;
import rcpsp.algorithms.scheduler.AbstractScheduler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.data.jaxb.Mode;
import rcpsp.data.jaxb.Project;

import java.util.logging.Logger;

/**
 * @author Mario Gomez
 */
public class SprecherDrexlV2 extends AbstractScheduler {

    private static final Logger LOG = Logger.getLogger(SprecherDrexlV2.class.getName());
    protected int[] selectedJobIndex;

    /* (non-Javadoc)
     * @see rcpsp.AbstractScheduler#init(Project)
     */
    @Override
    public void init(Project project) {
        super.init(project);
        selectedJobIndex = new int[numJobs + 1];
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#solve()
     */
    @Override
    public Solution solve(Project project) {
        init(project);

        // Initialization [Step 1]

        //level 0
        selectedJob[0] = 0;

        //level 1;
        eligibleJobs[1].add(0);
        selectedJobIndex[1] = 0;
        selectedJob[1] = 0;

        scheduledJobs.add(0);
        startTime[0] = 0;
        finishTime[0] = 0;
        mode[0] = 0;

        level = 2;
        for (Integer successor : successors[0]) {
            eligibleJobs[level].add(successor);
        }

        selectedJobIndex[level] = 0;
        selectedJob[level] = eligibleJobs[level].get(0).intValue();
        mode[selectedJob[level]] = -1;

        // Select next untested mode or descendant
        while (true) {
            if (mode[selectedJob[level]] < modes[selectedJob[level]].size() - 1) {
                mode[selectedJob[level]]++;
                findFeasibleStartTime();
            } else if (selectedJobIndex[level] < eligibleJobs[level].size() - 1) {
                selectedJobIndex[level]++;
                selectedJob[level] = eligibleJobs[level].get(selectedJobIndex[level]).intValue();
                mode[selectedJob[level]] = 0;
                findFeasibleStartTime();
            } else { // One level backtracking [Step 3]
                level--;
                if (level == 0) { // Finish !
                    break;
                } else { // unschedule previously scheduled job
                    int jobToUnSchedule = selectedJob[level];
                    scheduledJobs.remove(scheduledJobs.size() - 1);
                    Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
                    int resourceID = 0;
                    for (Integer request : jobMode.getResourceRequest()) {
                        for (int k = 1; k <= level; k++) {
                            if (resource[resourceID].isRenewable()) { // Renewable resource
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                        && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            } else { // Non renewable resource
                                if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                                    // update resource arrays of a job already scheduled
                                    remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                                }
                            }
                        }
                        resourceID++;
                    }
                }
            }
        }
        if (!bestSolution.isComplete()) {
            LOG.info("No solution found");
        }
        return bestSolution;
    }

    /**
     * This method tries to schedule the next job in the set of eligible jobs for the current level If a
     * precedence-feasible and resource-feasible start time is found then the job is scheduled and a new set of eligible
     * jobs is computed for the next level If no feasible start time is found, then the method ends without any change
     */
    protected void findFeasibleStartTime() {
        boolean isPrecedenceFeasible = true;
        int jobToSchedule = selectedJob[level];
        Mode jobMode = modes[jobToSchedule].get(mode[jobToSchedule]);
        int duration = jobMode.getDuration();
        // First, compute time bounds (lower and upper bound) for start time
        // First condition: start time >= start time of latest job scheduled
        int startTimeLowerBound = startTime[selectedJob[level - 1]];
        // Second condition start time <= latest finish time (LF) - duration
        int startTimeUpperBound = timeBounds[jobToSchedule].getLatestFinishTime() - duration;

        if (startTimeLowerBound <= startTimeUpperBound) {
            // Third condition: start time >= finish time of its predecessors
            for (Integer predecessor : predecessors[jobToSchedule]) {
                int predecessorID = predecessor.intValue();
                if (finishTime[predecessorID] <= startTimeUpperBound) {
                    startTimeLowerBound = Math.max(startTimeLowerBound,
                            finishTime[predecessorID]);
                } else {
                    isPrecedenceFeasible = false;
                    break;
                }
            }
        } else {
            isPrecedenceFeasible = false;
        }

        // Now find a start time in [startTimeLowerBound,startTimeUpperBound] which is resource feasible
        // Note that start time would be equal to the finish time of some job already scheduled
        // since renewable resources are liberated when a job finishes
        if (isPrecedenceFeasible) {
            int minFeasibleStartTime = startTimeUpperBound + 1;
            for (Integer scheduledJob : scheduledJobs) {
                int jobID = scheduledJob.intValue();
                int candidateStartTime = finishTime[jobID];
                if (candidateStartTime >= startTimeLowerBound
                        && candidateStartTime < minFeasibleStartTime
                        && remainingResources[jobID].isResourceFeasible(jobMode.getResourceRequest())) {
                    minFeasibleStartTime = candidateStartTime;
                }
            }
            if (minFeasibleStartTime <= startTimeUpperBound) { // if it is resource && precedence feasible
                // feasible start time found, so schedule job and update resources arrays
                startTime[jobToSchedule] = minFeasibleStartTime;
                finishTime[jobToSchedule] = minFeasibleStartTime + duration;
                remainingResources[jobToSchedule].reset();
                int resourceID = 0;
                for (Integer request : modes[jobToSchedule].get(mode[jobToSchedule]).getResourceRequest()) {
                    if (!resource[resourceID].isRenewable()) {
                        remainingResources[jobToSchedule].useResource(resourceID,
                                request.intValue());
                    }
                    for (Integer scheduledJob : scheduledJobs) {
                        int jobIndex = scheduledJob.intValue();
                        if (resource[resourceID].isRenewable()) { //Renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]
                                    && finishTime[jobIndex] < finishTime[jobToSchedule]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            } else if (startTime[jobIndex] <= finishTime[jobToSchedule]
                                    && finishTime[jobToSchedule] < finishTime[jobIndex]) {
                                // update resources arrays of job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        } else { // Non renewable resource
                            if (startTime[jobToSchedule] <= finishTime[jobIndex]) {
                                // update resource arrays of a job already scheduled
                                remainingResources[jobIndex].useResource(resourceID, request.intValue());
                            }
                            if (startTime[jobIndex] <= finishTime[jobToSchedule]) {
                                // update resources arrays of the job being scheduled
                                remainingResources[jobToSchedule].useResource(resourceID,
                                        modes[jobIndex].get(mode[jobIndex]).getResourceRequest().get(resourceID));
                            }
                        }
                    }

                    resourceID++;
                }
                scheduledJobs.add(Integer.valueOf(jobToSchedule)); // add current job to list of scheduled jobs

                if (level == numJobs) {
                    updateBestSolution(); // new solution found [Step 5]
                } else { // Update the eligible set [Step 6]
                    level++;
                    eligibleJobs[level].clear();
                    for (Integer eligibleJob : eligibleJobs[level - 1]) {
                        eligibleJobs[level].add(eligibleJob);
                    }
                    eligibleJobs[level].remove(Integer.valueOf(jobToSchedule));
                    for (Integer candidateJob : successors[jobToSchedule]) {
                        if (!eligibleJobs[level].contains(candidateJob)) {
                            boolean eligible = true;
                            for (Integer predecessor : predecessors[candidateJob.intValue()]) {
                                if (!scheduledJobs.contains(predecessor)) {
                                    eligible = false;
                                    break;
                                }
                            }
                            if (eligible) {
                                eligibleJobs[level].add(candidateJob);
                            }
                        }
                    }
                    selectedJobIndex[level] = 0;
                    selectedJob[level] = eligibleJobs[level].get(0).intValue();
                    mode[selectedJob[level]] = -1;
                }
            } else // if it is not (precedence & resource) feasible in any mode, do not continue exploring this level
            if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
                selectedJobIndex[level] = eligibleJobs[level].size(); // this will provoke a backtrack
            }
        } else // if it is not (precedence) feasible in any mode, do not continue exploring this level
        if (mode[selectedJob[level]] == modes[selectedJob[level]].size() - 1) {
            selectedJobIndex[level] = eligibleJobs[level].size(); // this will provoke a backtrack
        }
    }

    /**
     *
     */
    protected void updateBestSolution() {
        // Store solution and adjust time bounds [Step 7]
        ResourcesArray cloneRemainingResources[] = new ResourcesArray[remainingResources.length];
        for (int i = 0; i < cloneRemainingResources.length; i++) {
            cloneRemainingResources[i] = ResourcesArray.newInstance(remainingResources[i]);
        }

        bestSolution = new Solution(selectedJob.clone(), startTime.clone(), finishTime.clone(),
                mode.clone(), finishTime[selectedJob[level]], toString(), cloneRemainingResources);

//		stopwatch.stop();
//		System.out.println("Found new solution: "+ bestSolution.toString() + stopwatch.toString());

        int boundDecrement = timeBounds[numJobs - 1].getLatestFinishTime() - bestSolution.getMakespan() + 1;
        for (int j = 0; j < numJobs; j++) {
            timeBounds[j].setLatestStartTime(timeBounds[j].getLatestStartTime()
                    - boundDecrement);
            timeBounds[j].setLatestFinishTime(timeBounds[j].getLatestFinishTime()
                    - boundDecrement);
        }

        // Calculate lowest indexed level violating the time window [Step 8]
        int levelBacktrack = 1;
        for (int k = 1; k < level; k++) {
            if (finishTime[selectedJob[k]] > timeBounds[selectedJob[k]].getLatestFinishTime()) {
                levelBacktrack = k;
                break;
            }
        }

        // Variable-level backtracking [Step 9]
        for (int i = level; i >= levelBacktrack; i--) {
            int resourceID = 0;
            int jobToUnSchedule = selectedJob[i];
            scheduledJobs.remove(scheduledJobs.size() - 1);
            Mode jobMode = modes[jobToUnSchedule].get(mode[jobToUnSchedule]);
            for (Integer request : jobMode.getResourceRequest()) {
                for (int k = 1; k < i; k++) {
                    if (resource[resourceID].isRenewable()) {
                        if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]
                                && finishTime[selectedJob[k]] < finishTime[jobToUnSchedule]) {
                            // update resource arrays of a job already scheduled
                            remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                        }
                    } else {
                        if (startTime[jobToUnSchedule] <= finishTime[selectedJob[k]]) {
                            // update resource arrays of a job already scheduled
                            remainingResources[selectedJob[k]].liberateResource(resourceID, request.intValue());
                        }
                    }
                }
                resourceID++;
            }
        }
        level = levelBacktrack;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SDv2";
    }

    /* (non-Javadoc)
     * @see rcpsp.Scheduler#newInstance()
     */
    @Override
    public Scheduler newInstance() {
        return new SprecherDrexl();
    }
}
