/**
 *
 */
package rcpsp.algorithms.scheduler.multiheuristic;

import rcpsp.algorithms.Constant;
import rcpsp.algorithms.heuristics.jobselection.JobPriorityRule;
import rcpsp.algorithms.heuristics.modeselection.ModePriorityRule;
import rcpsp.algorithms.sampler.DeterministicSampler;
import rcpsp.algorithms.sampler.Sampler;
import rcpsp.algorithms.scheduler.Scheduler;
import rcpsp.algorithms.scheduler.serialgenerationscheme.SSGS;
import rcpsp.data.Solution;
import rcpsp.util.Stopwatch;
import rcpsp.data.jaxb.Project;

import java.util.Arrays;
import java.util.logging.Logger;

/**
 * @author Mario Gomez
 *
 */
public class MultiHeuristicSSGS implements Scheduler {
//	 Algorithm fields
	protected JobPriorityRule[] jobPriorityRule;
	protected ModePriorityRule[] modePriorityRule ;
	protected Sampler sampler;
	protected int iterations;
	protected Solution bestSolution;
	protected Project project;

	//	 Auxiliary fields
	protected final Logger logger = Logger.getLogger(this.getClass().getName());
	protected final Stopwatch stopwatch = new Stopwatch();

	/**
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 * @param sampler
	 * @param iterations
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule, ModePriorityRule[] modePriorityRule, Sampler sampler, int iterations) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = modePriorityRule;
		this.sampler = sampler;
		this.iterations = iterations;
		
	}
	
	/**
	 * @param jobPriorityRule
	 * @param modePriorityRule
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule, ModePriorityRule[] modePriorityRule) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = modePriorityRule;
		this.sampler = new DeterministicSampler();
		this.iterations = 1;
	}
	
	/**
	 * @param jobPriorityRule
	 */
	public MultiHeuristicSSGS(JobPriorityRule[] jobPriorityRule) {
		this.jobPriorityRule = jobPriorityRule;
		this.modePriorityRule = Constant.bestMode;
		this.sampler = new DeterministicSampler();
		this.iterations = 1;
	}
	
	
	/* (non-Javadoc)
	 * @see rcpsp.Scheduler#solve(Project)
	 */
	@Override
	public Solution solve(Project project) {
		stopwatch.start();
		this.project = project;
		bestSolution = new Solution(project.getNumJobs(), project.getTimeHorizon());
		for (int j = 0; j < jobPriorityRule.length; j++)
			for (int m = 0; m < modePriorityRule.length; m++) {
				SSGS solver = new SSGS(jobPriorityRule[j], modePriorityRule[m], sampler, iterations);
				Solution newSolution = solver.solve(project);
				if (newSolution.getMakespan() < bestSolution.getMakespan()) {
					bestSolution = newSolution;
				}
			}
		if (!bestSolution.isComplete()) logger.info(toString() + ": No solution found");
		return bestSolution;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "MHS_" + sampler.toString() + Arrays.asList(jobPriorityRule).toString();
	}
	
	/* (non-Javadoc)
	 * @see rcpsp.Scheduler#newInstance()
	 */
	@Override
	public Scheduler newInstance() {
		JobPriorityRule[] newJobPriorityRule = new JobPriorityRule[jobPriorityRule.length];
		for (int i = 0; i < newJobPriorityRule.length; i++) {
			newJobPriorityRule[i] = (JobPriorityRule) jobPriorityRule[i].newInstance();
		}
		
		ModePriorityRule[] newModePriorityRule = new ModePriorityRule[modePriorityRule.length];
		for (int i = 0; i < newModePriorityRule.length; i++) {
			newModePriorityRule[i] = (ModePriorityRule) modePriorityRule[i].newInstance();
		}
		Sampler newSampler = sampler.newInstance();
		
		return new MultiHeuristicSSGS(newJobPriorityRule, newModePriorityRule, newSampler, iterations);
	}
	
	/* (non-Javadoc)
	 * @see rcpsp.Scheduler#getAlgorithm()
	 */
	@Override
	public String getAlgorithm() {
		return this.toString();
	}

	/* (non-Javadoc)
	 * @see rcpsp.Scheduler#close()
	 */
	public void close() {
		stopwatch.stop();
		System.out.println("* Final solution for " + project.getName() + ":" + bestSolution.toString() + stopwatch.toString());
	}
}

