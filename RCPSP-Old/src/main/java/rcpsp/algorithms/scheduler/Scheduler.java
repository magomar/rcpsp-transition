package rcpsp.algorithms.scheduler;

import rcpsp.data.Solution;
import rcpsp.data.jaxb.Project;

/**
 * @author Mario Gomez
 * This interface defines the basic functionality to be provided by any scheduling algorithm
 * @see AbstractScheduler
 */
public interface Scheduler {


	/**
	 * @return the solution to an scheduling problem
	 */
	public Solution solve(Project project);
	
	/**
	 *  Closes the scheduler
	 */
	public void close();
	
	
	/**
	 * Get the name of the scheduling algorithm 
	 * @return the name of the scheduling algorithm
	 */
	public String getAlgorithm();
	
	/**
	 * Defensive copy of the object, equivalent to clone().
	 * This is method is needed to solve multiple problems concurrently
	 * @return a new instance of the scheduler
	 */
	public Scheduler newInstance();
	

}
